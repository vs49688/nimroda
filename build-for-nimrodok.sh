#!/bin/bash
set -e

function build_nimrod {
	SYSTEM=$1
	ARCH=$2

	echo "Building Nimrod/A for Nimrod/OK with configuration $SYSTEM-$ARCH"
	LDFLAGS="-static-libstdc++"
	CFLAGS=""
	CXXFLAGS=""
	
	CMAKEFLAGS="-DNIMRODA_BUILD_STATIC=Off"
	BUILDDIR="build-$SYSTEM-$ARCH"

	if [ x"$SYSTEM" == x"linux" ]; then
		BINARY="libnimroda.so"
		if [ x"$ARCH" == x"x86" ]; then
			CFLAGS="$CFLAGS -m32"
			CXXFLAGS="$CXXFLAGS -m32"
		fi
	elif [ x"$SYSTEM" == x"windows" ]; then
		BINARY="libnimroda.dll"
		LDFLAGS="$LDFLAGS -static-libgcc"
		if [ x"$ARCH" == x"x86" ]; then
			CMAKEFLAGS="$CMAKEFLAGS -DCMAKE_TOOLCHAIN_FILE=../mingw-w64-toolchain-x86.cmake"
		elif [ x"$ARCH" == x"x86_64" ]; then
			CMAKEFLAGS="$CMAKEFLAGS -DCMAKE_TOOLCHAIN_FILE=../mingw-w64-toolchain-x86_64.cmake"
		fi
	fi

	mkdir -p -- "$BUILDDIR"
	cd "$BUILDDIR"

	export LDFLAGS=$LDFLAGS
	export CFLAGS=$CFLAGS
	export CXXFLAGS=$CXXFLAGS

	cmake $CMAKEFLAGS ..
	make nimroda
	strip -s "$BINARY"

	cd ..
}

function archive_for_nimrodok {
	mkdir -p build-final/{linux-x86,linux-x86-64,win32-x86,win32-x86-64}
	cp build-linux-x86/libnimroda.so build-final/linux-x86/libnimroda.so
	cp build-linux-x86_64/libnimroda.so build-final/linux-x86-64/libnimroda.so
	cp build-windows-x86/libnimroda.dll build-final/win32-x86/nimroda.dll
	cp build-windows-x86_64/libnimroda.dll build-final/win32-x86-64/nimroda.dll

	cd build-final
	tar -cJvf ../nimroda-binaries.tar.xz linux-x86 linux-x86-64 win32-x86 win32-x86-64
	cd ..
}

build_nimrod linux x86
build_nimrod linux x86_64
build_nimrod windows x86
build_nimrod windows x86_64

echo "Compressing..."
archive_for_nimrodok

doxygen
