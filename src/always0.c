/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "nimroda/nimroda_ip.h"

#define EVAL_LIMIT 10

typedef struct
{
	size_t num_evals;
	nimroda_point_t *point;

	nimroda_point_t _point;
} always0_data_t;

nimroda_instance_t *always0Init(nimroda_instance_t *instance, const nimroda_custom_config_t *config)
{
	always0_data_t *data = malloc(sizeof(always0_data_t));
	if(data == NULL)
	{
		nimroda_printf(instance, "Failed to allocate %u bytes.\n", (uint32_t)sizeof(always0_data_t));
		return NULL;
	}

	memset(data, 0, sizeof(always0_data_t));

	instance->optim.__private = data;
	data->num_evals = 0;

	if(!nimroda_point_alloc_inplace(instance, data->point))
	{
		nimroda_printf(instance, "Failed to allocate %u bytes.\n", (uint32_t)sizeof(data->point));
		return NULL;
	}

	return instance;
}

void always0DeInit(nimroda_instance_t *instance)
{
	always0_data_t *data = (always0_data_t*)instance->optim.__private;

	nimroda_point_release_inplace(data->point);
	free(instance->optim.__private);
}

void always0Optimise(nimroda_instance_t *instance)
{
	always0_data_t *data = (always0_data_t*)instance->optim.__private;

	nimroda_optim_state_t state = instance->optim.state;
	switch(state)
	{
		case STATE_STOPPED:
		case STATE_WAITING_FOR_BATCH:
			if(data->num_evals++ < EVAL_LIMIT)
			{
				instance->optim.current_batch.num_points = 1;
				instance->optim.current_batch.points = &data->point;
				instance->optim.current_batch.points[0] = &data->_point;
				instance->optim.state = STATE_WAITING_FOR_BATCH;
			}
			else
			{
				instance->optim.state = STATE_FINISHED;

				if(!nimroda_point_alloc_inplace(instance, *instance->optim.status.results))
				{
					instance->optim.status.code = 1;
					strncpy(instance->optim.status.message, "Failed to allocate results", LONGEST_ERROR_STRING);
					instance->optim.status.result_count = 0;
					break;
				}

				instance->optim.status.code = 0;
				strncpy(instance->optim.status.message, "Zero returned!", LONGEST_ERROR_STRING);

				for(size_t i = 0; i < instance->num_dims; ++i)
					instance->optim.status.results[0]->coord[i] = 0;

				instance->optim.status.result_count = 1;
			}
			break;
		case STATE_REFIRE:
		case STATE_FINISHED:
			break;
	}
}
