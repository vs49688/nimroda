/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <cstring>
#include <climits>
#include <cassert>
#include <algorithm>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <sstream>
#include "nimroda/mots2/NimrodAdapter.hpp"
#include "nimroda/mots2/TabuSearch.hpp"

using namespace NimrodA;
/* Can't using the entire namespace here as some names conflict. */
using TabuSearch = NimrodA::MOTS2::TabuSearch;
using ConfigurationSettings = NimrodA::MOTS2::ConfigurationSettings;

nimroda_instance_t *mots2Init(nimroda_instance_t *instance, const nimroda_custom_config_t *config)
{
	assert(instance->optim.__private == nullptr);

	NimrodA::Nimrod& nimrod = *reinterpret_cast<NimrodA::Nimrod*>(instance->__cpp);

	/* Allocate the memory from the application, then placement-new it.
	 * Use the C++ allocator for this, as it throws. */
	TabuSearch *ts = new TabuSearch(nimrod);
	instance->optim.__private = ts;
	return instance;
}

void mots2Deinit(nimroda_instance_t *instance)
{
	assert(instance->optim.__private != nullptr);

	delete reinterpret_cast<TabuSearch*>(instance->optim.__private);
}

void mots2Search(nimroda_instance_t *instance)
{
	assert(instance->optim.__private != nullptr);
	TabuSearch *ts = reinterpret_cast<TabuSearch*>(instance->optim.__private);
	ts->fire();
}

int mots2Save(nimroda_instance_t *instance, cio_context_t *ctx)
{
	assert(instance->optim.__private != nullptr);
	TabuSearch *ts = reinterpret_cast<TabuSearch*>(instance->optim.__private);

	CIOContext t(ctx, true);
	ts->save(t);

	/* The above will throw if anything bad happens. */
	return 0;
}

int mots2Restore(nimroda_instance_t *instance, cio_context_t *ctx)
{
	assert(instance->optim.__private != nullptr);
	TabuSearch *ts = reinterpret_cast<TabuSearch*>(instance->optim.__private);
	CIOContext t(ctx, true);
	ts->restore(t);

	/* The above will throw if anything bad happens. */
	return 0;
}

nimroda_algorithm_t g_MOTS2Definition = {"MOTS2", "Tabu Search", INT_MAX, &mots2Init, &mots2Search, &mots2Deinit, &mots2Save, &mots2Restore};