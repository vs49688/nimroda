/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdexcept>
#include "nimroda/mots2/ConfigurationSettings.hpp"
#include "nimroda/cxx/Nimrod.hpp"
#include "nimroda/cxx/CIOContext.hpp"

using namespace NimrodA;
using namespace NimrodA::MOTS2;

template<typename T>
static T parseOrDefault(const char *s, const char *fmt, T _default)
{
	T val;

	int ret = sscanf(s, fmt, &val);
	if(ret != 1)
		return _default;

	return val;
}

ConfigurationSettings::ConfigurationSettings(Nimrod& nimrod)
{
	diversify = parseOrDefault<uint32_t>(nimrod.getConfig("mots2.diversify"), "%lu", 25);
	intensify = parseOrDefault<uint32_t>(nimrod.getConfig("mots2.intensify"), "%lu", 15);
	reduce = parseOrDefault<uint32_t>(nimrod.getConfig("mots2.reduce"), "%lu", 50);

	/* Start-step: Try to read this as a point. If that fails, read it as a double. */
	auto ss = std::stringstream(nimrod.getConfig("mots2.start_step"));
	stepSize = DoubleVector::readVector(ss);
	if(stepSize.getLength() == 0)
		stepSize = DoubleVector(nimrod.dimensionality, parseOrDefault<double>(nimrod.getConfig("mots2.start_step"), "%lf", 0));

	if(stepSize.getLength() != nimrod.dimensionality)
		throw std::runtime_error("Invalid length for mots2.start_step");

	SSRF = parseOrDefault<double>(nimrod.getConfig("mots2.ssrf"), "%lf", 0.0f);

	numSamples = parseOrDefault<uint32_t>(nimrod.getConfig("mots2.n_sample"), "%lu", 0);
	loopLimit = parseOrDefault<uint32_t>(nimrod.getConfig("mots2.loop_limit"), "%lu", 0);
	evalLimit = parseOrDefault<uint32_t>(nimrod.getConfig("mots2.eval_limit"), "%lu", 0);
	improveLimit = parseOrDefault<uint32_t>(nimrod.getConfig("mots2.improvement_limit"), "%lu", 1000);
	numRegions = parseOrDefault<uint32_t>(nimrod.getConfig("mots2.n_regions"), "%lu", 0);
	stmSize = parseOrDefault<uint32_t>(nimrod.getConfig("mots2.stm_size"), "%lu", 0);
}


void ConfigurationSettings::save(CIOContext& ctx)
{
	ctx.write_uint64((uint64_t)diversify, "diversify");
	ctx.write_uint64((uint64_t)intensify, "intensify");
	ctx.write_uint64((uint64_t)reduce, "reduce");

	ctx.write_coord_vector(stepSize, "step_size");

	ctx.write_float64(SSRF, "ssrf");
	ctx.write_uint64((uint64_t)numSamples, "num_samples");
	ctx.write_uint64((uint64_t)improveLimit, "improve_limit");
	ctx.write_uint64((uint64_t)numRegions, "num_regions");
	ctx.write_uint64((uint64_t)stmSize, "stm_size");
}

void ConfigurationSettings::restore(CIOContext& ctx)
{
	diversify = (size_t)ctx.read_uint64("diversify");
	intensify = (size_t)ctx.read_uint64("intensify");
	reduce = (size_t)ctx.read_uint64("reduce");

	stepSize = ctx.read_coord_vector("step_size");

	SSRF = ctx.read_float64("ssrf");
	numSamples = (size_t)ctx.read_uint64("num_samples");
	improveLimit = (size_t)ctx.read_uint64("improve_limit");
	numRegions = (size_t)ctx.read_uint64("num_regions");
	stmSize = (size_t)ctx.read_uint64("stm_size");

}