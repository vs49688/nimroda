/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <algorithm>
#include <cassert>
#include "nimroda/mots2/LTMContainer.hpp"
#include "nimroda/cxx/Nimrod.hpp"
#include "nimroda/cxx/CIOContext.hpp"

using namespace NimrodA;
using namespace NimrodA::MOTS2;

LTMContainer::LTMContainer(Nimrod& nimrod, size_t numRegions) :
	LTMContainer(nimrod, numRegions, RegionVector(nimrod.dimensionality, CountVector(numRegions, 0)))
{
}

LTMContainer::LTMContainer(Nimrod& nimrod, size_t numRegions, RegionVector&& rvec) :
	numRegions(numRegions),
	numVars(nimrod.dimensionality),
	regionSteps((nimrod.upperBound - nimrod.lowerBound) / (double)numRegions),
	m_RegionCounts(std::move(rvec)),
	m_Nimrod(nimrod)
{

}

void LTMContainer::add(const Point::Ref& pt)
{
	if(!m_Nimrod.isValid(*pt))
		throw std::runtime_error("LTMContainer::add(): Invalid point");

	auto& coords = pt->getCoordinates();

	for(size_t i = 0; i < coords.getLength(); ++i)
		++m_RegionCounts[i][getRegionIndex(coords[i], i)];
}

size_t LTMContainer::getRegionIndex(double val, size_t coordIndex)
{
	double stepSize = regionSteps[coordIndex];

	double start = m_Nimrod.lowerBound[coordIndex];
	for(size_t i = 0; i < numRegions; ++i)
	{
		double end = start + stepSize;
		if(val >= start && val < end)
			return i;
		start = end;
	}

	/* Special case if we're right on the boundaty of the last region. */
	if(val == m_Nimrod.lowerBound[coordIndex] + numRegions * stepSize)
		return numRegions - 1;

	throw std::runtime_error("LTMContainer::getRegionIndex(): Check failed. This should never happen.");
}

double LTMContainer::getCoordFromLeastVisitedRegion(size_t coordIndex)
{
	if(coordIndex >= numVars)
		throw std::invalid_argument("coordIndex too large");

	const auto& counts = m_RegionCounts[coordIndex];

	/* Phase 1: Get the region with the least count */
	size_t currMin = SIZE_MAX;
	size_t minIndex = SIZE_MAX;
	for(size_t i = 0; i < numRegions; ++i)
	{
		if(counts[i] < currMin)
		{
			currMin = counts[i];
			minIndex = i;
		}
	}

	/* Phase 2: Count the number of times that occurs. */
	size_t minCount = 0;
	for(size_t i = 0; i < numRegions; ++i)
	{
		if(counts[i] == currMin)
			++minCount;
	}

	/* Phase 3: Generate a random number (r) between 0 and minCount and
	 * select the r'th one with the minimum. */
	assert(minCount <= UINT32_MAX);
	uint32_t r = m_Nimrod.randomUInt(0, (uint32_t)minCount);
	minIndex = 0;
	for(size_t i = 0; i < numRegions; ++i)
	{
		if(minIndex == r)
		{
			minIndex = i;
			break;
		}

		++minIndex;
	}

	double stepSize = regionSteps[coordIndex];
	double offset = m_Nimrod.randomDouble(0, stepSize);

	return m_Nimrod.lowerBound[coordIndex] + (minIndex * stepSize) + offset;
}

void LTMContainer::reset() noexcept
{
	for(size_t i = 0; i < numVars; ++i) for(size_t j = 0; j < numRegions; ++j)
		m_RegionCounts[i][j] = 0;
}

Point::Ref LTMContainer::generatePotentialPoint()
{
	DoubleVector coords(numVars);

	for(size_t i = 0; i < numVars; ++i)
		coords[i] = getCoordFromLeastVisitedRegion(i);

	return m_Nimrod.pointDb.createPoint(coords);
}

void LTMContainer::save(CIOContext& ctx, const char *name) const
{
	CIOContext lctx(ctx, false, name);
	lctx.write_uint64((uint64_t)numRegions, "num_regions");

	{
		CIOContext cctx(lctx, true, "region_counts");

		for(const auto& i : m_RegionCounts)
		{
			CIOContext rctx(cctx, true, nullptr);
			for(const auto& j : i)
				rctx.write_uint64((uint64_t)j);
		}
	}
}

LTMContainer LTMContainer::restore(Nimrod& nimrod, CIOContext& ctx, const char *name)
{
	CIOContext lctx(ctx, false, name);

	size_t numRegions = (size_t)lctx.read_uint64("num_regions");

	RegionVector rvec(nimrod.dimensionality, CountVector(numRegions, 0));

	{
		CIOContext cctx(lctx, true, "region_counts");

		for(size_t i = 0; i < nimrod.dimensionality; ++i)
		{
			CIOContext rctx(cctx, true, nullptr);
			for(size_t j = 0; j < numRegions; ++j)
				rvec[i][j] = (size_t)rctx.read_uint64();
		}
	}

	return LTMContainer(nimrod, numRegions, std::move(rvec));
}