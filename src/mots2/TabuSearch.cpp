/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nimroda/mots2/TabuSearch.hpp"
#include <cmath>
#include <sstream>
#include <fstream>
#include <iostream>
#include <assert.h>

using namespace NimrodA;
using namespace NimrodA::MOTS2;

using TabuState = TabuSearch::TabuState;

typedef struct
{
	TabuState value;
	const char *name;
	int intVal;
} enuminfo_t;

static enuminfo_t g_sStateInfo[] = 
{
	{TabuState::Initial, "Initial", 0},
	{TabuState::InitialEval, "InitialEval", 1},
	{TabuState::Iterating, "Iterating", 2},
	{TabuState::Pattern, "Pattern", 3},
	{TabuState::PatternEval, "PatternEval", 4},
	{TabuState::HookeJeeves, "HookeJeeves", 5},
	{TabuState::HookeJeevesEval, "HookeJeevesEval", 6},
	{TabuState::HookeJeevesFinal, "HookeJeevesFinal", 7},
	{TabuState::Line18, "Line18", 8},
	{TabuState::Diversify, "Diversify", 9},
	{TabuState::DiversifyEval, "DiversifyEval", 10}
};

constexpr int g_sNumStates = sizeof(g_sStateInfo) / sizeof(enuminfo_t);

static int stateToInt(TabuState ts)
{
	switch(ts)
	{
		case TabuState::Initial: return 0;
		case TabuState::InitialEval: return 1;
		case TabuState::Iterating: return 2;
		case TabuState::Pattern: return 3;
		case TabuState::PatternEval: return 4;
		case TabuState::HookeJeeves: return 5;
		case TabuState::HookeJeevesEval: return 6;
		case TabuState::HookeJeevesFinal: return 7;
		case TabuState::Line18: return 8;
		case TabuState::Diversify: return 9;
		case TabuState::DiversifyEval: return 10;
	}

	throw std::runtime_error("Invalid TabuState value");
}

static const char *stateToString(TabuState state)
{
	return g_sStateInfo[stateToInt(state)].name;
}

static TabuState intToState(int i)
{
	if(i < 0 || i >= g_sNumStates)
		throw std::runtime_error("Invalid TabuState index");
	
	return g_sStateInfo[i].value;
}

TabuSearch::TabuSearch(Nimrod& nimrod) :
	m_Nimrod(nimrod),
	m_Config(nimrod),
	m_STM(m_Config.stmSize), m_MTM(), m_IM(),
	m_LTM(nimrod, m_Config.numRegions),
	m_NextMoveHookJeeves(true),
	m_InitialStep(m_Config.stepSize),
	m_LoopCounter(0),
	m_iLocal(0), m_Diversification(0), m_Intensification(0), m_Reduction(0),
	m_State(TabuState::Initial)
{
	if(m_InitialStep.getLength() != m_Nimrod.dimensionality)
		throw std::invalid_argument("Invalid or no value for step size.");

	m_InitialStep *= m_Nimrod.range;
}

void TabuSearch::writeStats() const
{
	/* So much fucking easier than doing it in C. */
	Nimrod::StatsVector stats;
	stats.emplace_back("iteration", formatType(m_LoopCounter));
	stats.emplace_back("i_local", formatType(m_iLocal));
	stats.emplace_back("diversification", formatType(m_Diversification));
	stats.emplace_back("intensification", formatType(m_Intensification));
	stats.emplace_back("im_size", formatType(m_IM.size()));
	stats.emplace_back("reduction", formatType(m_Reduction));
	stats.emplace_back("base_point", m_Points.front()->toString(false));
	stats.emplace_back("objective_values", m_Points.front()->toString(true));
	m_Nimrod.writeStats(stats);
}

void TabuSearch::fire()
{
	if(m_Nimrod.debugEnabled(DebugLevel::Trace))
		m_Nimrod.printf("TabuSearch::fire() called with state %s\n", stateToString(m_State));

	OptimisationState optimState = m_Nimrod.getState();
	if(optimState == OptimisationState::Stopped)
	{
		/* Initial move. */
		assert(m_State == TabuState::Initial);

		Point::Ref start = m_Nimrod.pointDb.createPoint(m_Nimrod.getStartingPoint());

		PointVector vec;
		vec.push_back(start);

		m_Nimrod.setState(OptimisationState::WaitingForBatch);
		m_Nimrod.setBatch(vec);
		m_State = TabuState::InitialEval;
		updateStatus(2, "Pending initial evaluation.");
		return;
	}

	/* optimState here will always be WaitingForBatch or Refire. */
	assert(optimState == OptimisationState::WaitingForBatch || optimState == OptimisationState::Refire);

	if(m_State == TabuState::InitialEval)
	{
		Point::Ref start = m_Nimrod.getBatch().get(0);
		m_MTM.insert(start);
		m_IM.insert(start);
		pushBasePoint(start, MoveType::Initial);
		m_LoopCounter = 0;

		m_State = TabuState::Iterating;
		m_Nimrod.setState(OptimisationState::Refire);
	}
	else if(m_State == TabuState::Iterating)
	{
		if(!stoppingCriteriaNotMet())
		{
			m_Nimrod.setState(OptimisationState::Finished);
			m_MTM.removeDominatedPoints();
			updateStatus(0, "Success");
			return;
		}

		if(m_Nimrod.debugEnabled(DebugLevel::Debug))
			m_Nimrod.printf("TabuSearch::fire(), iteration %lu\n", (unsigned long)m_LoopCounter);

		++m_LoopCounter;

		writeStats();

		m_State = m_NextMoveHookJeeves ? TabuState::HookeJeeves : TabuState::Pattern;
		m_Nimrod.setState(OptimisationState::Refire);
	}
	else if(m_State == TabuState::Pattern || m_State == TabuState::PatternEval)
	{
		patternMoveSM();
	}
	else if(m_State == TabuState::HookeJeeves || m_State == TabuState::HookeJeevesEval || m_State == TabuState::HookeJeevesFinal)
	{
		hookeJeevesSM();
	}
	else if(m_State == TabuState::Line18)
	{
		updateMemories();
		m_State = TabuState::Iterating;
		if(m_iLocal == m_Config.diversify)
		{
			m_State = TabuState::Diversify;
		}
		else if(m_iLocal == m_Config.intensify)
		{
			pushBasePoint(intensifyMove(), MoveType::Intensify);
			++m_iLocal;
			m_NextMoveHookJeeves = true;
		}
		else if(m_iLocal == m_Config.reduce)
		{
			pushBasePoint(reduceMove(), MoveType::Reduce);
			m_NextMoveHookJeeves = true;
			m_iLocal = 0;
		}

		m_Nimrod.setState(OptimisationState::Refire);
	}
	else if(m_State == TabuState::Diversify || m_State == TabuState::DiversifyEval)
	{
		diversifyMoveSM();
	}

	if(m_Nimrod.getState() == OptimisationState::WaitingForBatch)
		updateStatus(2, "Waiting for evaluation");
}

template <typename T>
static void saveContainer(const T& container, CIOContext& ctx, const char *name)
{
	CIOContext cctx(ctx, false, name);

	cctx.write_uint64((uint64_t)container.size(), "length");

	{
		CIOContext pctx(cctx, true, "points");

		for(const auto& i : container)
			pctx.write_point_reference(i);
	}
}

template <typename T>
static void restoreContainer(PointDB& db, T& container, CIOContext& ctx, const char *name)
{
	CIOContext cctx(ctx, false, name);

	size_t length = (size_t)cctx.read_uint64("length");

	container.clear();

	{
		CIOContext pctx(cctx, true, "points");

		for(size_t i = 0; i < length; ++i)
			container.insert(container.end(), pctx.read_point_reference(db, nullptr));
	}
}

void TabuSearch::save(CIOContext& ctx)
{
	{
		CIOContext cctx(ctx, false, "configuration");
		m_Config.save(cctx);
	}

	saveContainer(m_STM, ctx, "stm");
	saveContainer(m_MTM, ctx, "mtm");
	saveContainer(m_IM, ctx, "im");
	m_LTM.save(ctx, "ltm");

	ctx.write_int32((int)m_NextMoveHookJeeves, "next_move_hooke_jeeves");
	ctx.write_coord_vector(m_InitialStep, "initial_step");
	ctx.write_uint64((uint64_t)m_LoopCounter, "loop_counter");
	ctx.write_uint64((uint64_t)m_iLocal, "i_local");
	ctx.write_uint64((uint64_t)m_Diversification, "diversification");
	ctx.write_uint64((uint64_t)m_Intensification, "intensification");
	ctx.write_uint64((uint64_t)m_Reduction, "reduction");

	saveContainer(m_Points, ctx, "points");

	ctx.write_int32(stateToInt(m_State), "state");

	{
		CIOContext hj(ctx, false, "hooke_jeeves");
		m_HookeJeeves.write(hj);
	}

	{
		CIOContext dv(ctx, false, "diversify");
		m_Diversify.write(dv);
	}
}

void TabuSearch::restore(CIOContext& ctx)
{
	{
		CIOContext cctx(ctx, false, "configuration");
		m_Config.restore(cctx);
	}

	restoreContainer(m_Nimrod.pointDb, m_STM, ctx, "stm");
	restoreContainer(m_Nimrod.pointDb, m_MTM, ctx, "mtm");
	restoreContainer(m_Nimrod.pointDb, m_IM, ctx, "im");
	LTMContainer::restore(m_Nimrod, ctx, "ltm");

	m_NextMoveHookJeeves = ctx.read_int32("next_move_hooke_jeeves") != 0;
	m_InitialStep = ctx.read_coord_vector("initial_step");
	m_LoopCounter = (size_t)ctx.read_uint64("loop_counter");
	m_iLocal = (size_t)ctx.read_uint64("i_local");
	m_Diversification = (size_t)ctx.read_uint64("diversification");
	m_Intensification = (size_t)ctx.read_uint64("intensification");
	m_Reduction = (size_t)ctx.read_uint64("reduction");

	restoreContainer(m_Nimrod.pointDb, m_Points, ctx, "points");

	m_State = intToState(ctx.read_int32("state"));

	{
		CIOContext hj(ctx, false, "hooke_jeeves");
		m_HookeJeeves.read(hj, m_Nimrod.pointDb);
	}

	{
		CIOContext dv(ctx, false, "diversify");
		m_Diversify.read(dv);
	}
}

void TabuSearch::patternMoveSM()
{
	if(m_State == TabuState::Pattern)
	{
		Point::Ref tempObjFunc;

		if(m_Nimrod.debugEnabled(DebugLevel::Debug))
			m_Nimrod.puts("TabuSearch::patternMove() enter\n");

		auto it = m_Points.begin();

		Point::Ref currentPoint = *it++;
		Point::Ref lastPoint = *it;

		/* The compiler should optimise the hell out of this. */
		const DoubleVector& currentCoords = currentPoint->getCoordinates();
		Point::Ref newPoint = m_Nimrod.pointDb.createPoint(currentCoords + (currentCoords - lastPoint->getCoordinates()));

		if(!m_Nimrod.isValid(*newPoint))
		{
			if(m_Nimrod.debugEnabled(DebugLevel::Debug))
				m_Nimrod.puts("TabuSearch::patternMove() EXIT-0 (Invalid design).\n");
			m_State = TabuState::HookeJeeves;
			m_Nimrod.setState(OptimisationState::Refire);
			return;
		}

		PointVector vec;
		vec.push_back(newPoint);

		m_Nimrod.setState(OptimisationState::WaitingForBatch);
		m_Nimrod.setBatch(vec);
		m_State = TabuState::PatternEval;
	}
	else if(m_State == TabuState::PatternEval)
	{
		Point::Ref newPoint = m_Nimrod.getBatch().get(0);

		if(!m_MTM.addIfNotDominated(newPoint))
		{
			if(m_Nimrod.debugEnabled(DebugLevel::Debug))
				m_Nimrod.puts("TabuSearch::patternMove() EXIT-0 (Did not find non-dominated design).\n");

			m_State = TabuState::HookeJeeves;
			m_Nimrod.setState(OptimisationState::Refire);
			return;
		}

		m_IM.insert(newPoint);
		++m_iLocal;

		pushBasePoint(newPoint, MoveType::Pattern);

		if(m_Nimrod.debugEnabled(DebugLevel::Debug))
			m_Nimrod.printf("    Move %s\n", newPoint->toString().c_str());

		if(m_Nimrod.debugEnabled(DebugLevel::Debug))
			m_Nimrod.puts("TabuSearch::patternMove() EXIT-1\n");

		m_NextMoveHookJeeves = true;
		m_State = TabuState::Line18;
		m_Nimrod.setState(OptimisationState::Refire);
	}
}

bool TabuSearch::hjValidate(const Point::Ref& pt) const
{
	bool tabu = m_STM.isTabu(pt);
	bool valid = m_Nimrod.isValid(*pt);

	if(m_Nimrod.debugEnabled(DebugLevel::Debug))
		m_Nimrod.printf("    %d %d %s\n", (int)tabu, (int)valid, pt->toString().c_str());

	return !tabu && valid;
}

void TabuSearch::hjGenerate(const Point::ConstRef& start, PointSet& points) const
{
	/* Generate local points to be evaluated. */
	if(m_Nimrod.debugEnabled(DebugLevel::Debug))
	{
		m_Nimrod.printf("  Starting Point: %s\n", start->toString().c_str());
		m_Nimrod.puts("  Generated Points:\n");
		m_Nimrod.puts("    T V\n");
	}

	for(size_t i = 0; i < m_Nimrod.dimensionality; ++i)
	{
		/* Step. */
		Point::Ref step = increase(*start, i);
		if(hjValidate(step))
			points.insert(step);

		/* -Step. */
		step = decrease(*start, i);
		if(hjValidate(step))
			points.insert(step);
	}
}

void TabuSearch::hookeJeevesSM()
{
	/* Because it's annoying writing m_HookeJeeves. */
	auto& genPoints = m_HookeJeeves.genPoints;
	size_t& remainingSampleSets = m_HookeJeeves.remainingSampleSets;
	size_t& setCounter = m_HookeJeeves.setCounter;
	Point::Ref& nextPoint = m_HookeJeeves.nextPoint;

	if(m_State == TabuState::HookeJeeves)
	{
		if(m_Nimrod.debugEnabled(DebugLevel::Debug))
			m_Nimrod.puts("TabuSearch::hookeJeevesSM() enter\n");

		m_HookeJeeves.reset();

		/* Generate the search points. */
		hjGenerate(m_Points.front(), genPoints);

		/* No points valid. Oh dear. Try to intensify. */
		if(genPoints.empty())
		{
			if(m_Nimrod.debugEnabled(DebugLevel::Debug))
				m_Nimrod.puts("  Point generation failed. Attempting to use IM...\n");

			genPoints.insert(m_Nimrod.pointDb.createPoint(*intensifyMove()));
		}

		/* Calculate the amount of sample sets. */
		remainingSampleSets = genPoints.size() / m_Config.numSamples;
		if(genPoints.size() % m_Config.numSamples)
			++remainingSampleSets;

		m_State = TabuState::HookeJeevesEval;
		m_HookeJeeves.inEval = false;
		m_Nimrod.setState(OptimisationState::Refire);
	}
	else if(m_State == TabuState::HookeJeevesEval)
	{
		if(m_HookeJeeves.inEval)
		{
			const Batch& batch = m_Nimrod.getBatch();

			for(const Point::Ref& pt : batch)
			{
				m_HookeJeeves.evaluatedPoints.insert(pt);

				bool dominant;
				if(m_MTM.addIfNotDominated(pt))
				{
					dominant = true;
					m_HookeJeeves.dominant.insert(pt);
				}
				else
				{
					dominant = false;
					m_HookeJeeves.dominated.insert(pt);
				}

				if(m_Nimrod.debugEnabled(DebugLevel::MoreDebug))
					m_Nimrod.printf("  Point %s Dominat%s\n", pt->toString().c_str(), dominant ? "" : "ed");
			}

			/* If we have dominated points, */
			if(!m_HookeJeeves.dominant.empty())
			{
				/* Use one. */
				nextPoint = m_Nimrod.selectRandomFromContainer(m_HookeJeeves.dominant, false);
				remainingSampleSets = 0;
				m_iLocal = 0;
			}
			else
			{
				/* Otherwise, try the next sample. */
				--remainingSampleSets;
			}

			m_HookeJeeves.inEval = false;
		}

		/* NB: If we have a MTM addition (remainingSampleSets == 0), then don't eval any more batches. */
		if(!genPoints.empty() && remainingSampleSets > 0)
		{
			++setCounter;

			if(m_Nimrod.debugEnabled(DebugLevel::Debug))
				m_Nimrod.printf("  Random Sampling: Set %zu:\n", setCounter);

			PointVector evalBuffer;
			for(size_t i = 0; i < m_Config.numSamples && !genPoints.empty(); ++i)
			{
				Point::Ref tempPoint = m_Nimrod.selectRandomFromContainer(genPoints, true);
				evalBuffer.push_back(tempPoint);

				if(m_Nimrod.debugEnabled(DebugLevel::Debug))
					m_Nimrod.printf("    Sample %u: %s\n", (uint32_t)i, tempPoint->toString().c_str());
			}

			m_Nimrod.setState(OptimisationState::WaitingForBatch);
			m_Nimrod.setBatch(evalBuffer);
			m_State = TabuState::HookeJeevesEval;
			m_HookeJeeves.inEval = true;
			return;
		}
		else
		{
			m_State = TabuState::HookeJeevesFinal;
			m_Nimrod.setState(OptimisationState::Refire);
		}
	}
	else if(m_State == TabuState::HookeJeevesFinal)
	{
		/* NB: If no MTM addition. */
		if(m_HookeJeeves.dominant.empty() && !m_HookeJeeves.dominated.empty())
		{
			MTMContainer bufferContainer(m_HookeJeeves.evaluatedPoints);
			bufferContainer.removeDominatedPoints();

			if(!bufferContainer.empty())
			{
				nextPoint = m_Nimrod.selectRandomFromContainer(bufferContainer, false);
				if(m_Nimrod.debugEnabled(DebugLevel::MoreDebug))
					m_Nimrod.printf("  Out of the dominated, the dominant was selected: %s\n", nextPoint->toString().c_str());
			}
			else
			{
				if(m_Nimrod.debugEnabled(DebugLevel::Warn))
					m_Nimrod.printf("  All sampled neighbourhood infeasible. Intensifying...\n");

				nextPoint = intensifyMove();
			}

			++m_iLocal;
		}

		pushBasePoint(nextPoint, MoveType::HookeJeeves);

		m_NextMoveHookJeeves = false;

		/* Add everything to the memories. I've been trying to defer this as long as possible. */
		for(const Point::Ref& pt : m_HookeJeeves.evaluatedPoints)
		{
			m_STM.add(pt);
			m_LTM.add(pt);
		}

		for(const Point::Ref& pt : m_HookeJeeves.dominant)
			m_IM.addIfNotDominated(pt);

		if(m_Nimrod.debugEnabled(DebugLevel::Debug))
			m_Nimrod.printf("TabuSearch::hookeJeevesSM() exit\n");

		m_State = TabuState::Line18;
		m_Nimrod.setState(OptimisationState::Refire);
	}
}

void TabuSearch::diversifyMoveSM()
{
	if(m_State == TabuState::Diversify)
	{
		if(m_Nimrod.debugEnabled(DebugLevel::Debug))
			m_Nimrod.printf("TabuSearch::diversifyMoveSM2() enter\n");

		size_t patience = m_Diversify.tabuPatience;

		/* Generate a potential point. */
		Point::Ref newPoint;
		do
		{
			newPoint = m_LTM.generatePotentialPoint();
		}
		while(m_STM.isTabu(newPoint) && patience--);

		/* If that failed, intensify and abort the diversification. */
		if(patience == 0)
		{
			newPoint = intensifyMove();
			m_Diversify.evalPatience = 0;
		}

		PointVector vec(1, newPoint);

		m_State = TabuState::DiversifyEval;
		m_Nimrod.setState(OptimisationState::WaitingForBatch);
		m_Nimrod.setBatch(vec);
	}
	else if(m_State == TabuState::DiversifyEval)
	{
		Point::Ref newPoint = m_Nimrod.getBatch().get(0);

		if(m_Nimrod.debugEnabled(DebugLevel::Debug))
			m_Nimrod.printf("  Move Attempt: %s\n", newPoint->toString().c_str());

		/* The evaluation failed or was too bad. */
		if(newPoint->getJobStatus() == JOB_FAILED)
		{
			/* If we still have patience left for evaluations, retry. */
			if(m_Diversify.evalPatience)
			{
				m_Diversify.tabuPatience = 20;
				m_State = TabuState::Diversify;
			}
			else
			{
				/* Otherwise, intensify and continue. */
				newPoint = intensifyMove();
				m_State = TabuState::Iterating;
			}
		}
		else
		{
			++m_Diversification;

			if(m_MTM.addIfNotDominated(newPoint))
				m_iLocal = 0;
			else
				++m_iLocal;

			m_Diversify.reset();

			pushBasePoint(newPoint, MoveType::Diversify);
			m_State = TabuState::Iterating;

			if(m_Nimrod.debugEnabled(DebugLevel::Debug))
				m_Nimrod.printf("TabuSearch::diversifyMoveSM2() EXIT\n");
		}

		m_NextMoveHookJeeves = true;
		m_Nimrod.setState(OptimisationState::Refire);
	}
}

const MTMContainer& TabuSearch::getParetoFront()
{
	return m_MTM;
}

void TabuSearch::dumpParetoFront(std::ostream& s)
{
	s << "# Pareto Front for MOTS2 Job" << std::endl;
	s << "# Dimensionality = " << m_Nimrod.dimensionality << std::endl;
	s << "# Num. Objectives = " << m_Nimrod.numObjectives << std::endl;

	for(const Point::Ref& pt : m_MTM)
	{
		for(size_t i = 0; i < m_Nimrod.numObjectives; ++i)
		{
			if(i != 0)
				s << " ";

			const DoubleVector& objectives = pt->getObjectives();
			s << objectives[i];
		}

		s << std::endl;
	}
}

void TabuSearch::reset()
{
	m_NextMoveHookJeeves = true;
	m_STM.clear();
	m_MTM.clear();
	m_LTM.reset();
	m_Points.clear();
	m_iLocal = 0;
	m_LoopCounter = 0;
}

bool TabuSearch::stoppingCriteriaNotMet()
{
	bool e = m_Nimrod.getEvalCount() >= m_Config.evalLimit && m_Config.evalLimit != 0;
	bool l = m_LoopCounter >= m_Config.loopLimit;

	return !e && !l;
	//return m_LoopCounter < m_Config.loopLimit && (m_Nimrod.getEvalCount() < m_Config.evalLimit && m_Config.evalLimit != 0);
	//return m_LoopCounter >= m_Config.loopLimit && (m_Nimrod.getEvalCount() >= m_Config.evalLimit && m_Config.evalLimit != 0);
}

void TabuSearch::updateStatus(int32_t code, const char *message)
{
	OptimStatus& optimStatus = m_Nimrod.getOptimStatus();
	optimStatus.clearResults();

	for(const Point::Ref& pt : m_MTM)
		optimStatus.addResultPoint(pt);

	optimStatus.setErrorString(message);
	optimStatus.setCode(code);
}

void TabuSearch::updateMemories()
{
	checkMemories();
}

template <typename T>
static bool validateContainer(T& c)
{
	for(auto& pt : c) if(!pt)
		return false;

	return true;
}

void TabuSearch::checkMemories()
{
	if(!validateContainer(m_STM))
		throw std::runtime_error("Contract violation in Short-Term Memory - empty point and/or objectives.");

	if(!validateContainer(m_IM))
		throw std::runtime_error("Contract violation in Immediate Memory - empty point and/or objectives.");

	if(!validateContainer(m_MTM))
		throw std::runtime_error("Contract violation in Mid-Term Memory - empty point and/or objectives.");

	//if(!validateContainer(m_LTM))
	//	throw std::runtime_error("Contract violation in Long-Term Memory - empty point and/or objectives.");
}

static const char *moveTypeToString(TabuSearch::MoveType type)
{
	switch(type)
	{
		case TabuSearch::MoveType::Initial: return "InitialMove";
		case TabuSearch::MoveType::Pattern: return "PatternMove";
		case TabuSearch::MoveType::Diversify: return "DiversifyMove";
		case TabuSearch::MoveType::Intensify: return "IntensifyMove";
		case TabuSearch::MoveType::Reduce: return "ReduceMove";
		case TabuSearch::MoveType::HookeJeeves: return "HookeJeevesMove";
	}

	return "Unknown Move";
}

void TabuSearch::pushBasePoint(const Point::Ref& pt, MoveType moveType)
{
	const char *moveTypeName = moveTypeToString(moveType);
	if(m_Nimrod.debugEnabled(DebugLevel::Debug))
		m_Nimrod.printf("TabuSearch::pushBasePoint(%s, %s)\n", pt->toString().c_str(), moveTypeName);

	m_Points.push_front(pt);
	m_MTM.removeDominatedPoints();
}

Point::Ref TabuSearch::intensifyMove()
{
	if(m_Nimrod.debugEnabled(DebugLevel::Debug))
	{
		m_Nimrod.printf("TabuSearch::intensifyMove() enter\n");
		m_Nimrod.printf("  IM size (before) = %u\n", (uint32_t)m_IM.size());
		m_Nimrod.printf("  Intensification Loop:\n");
	}

	Point::Ref newPoint;

	/* FIXME: pls */
	try
	{
		do
		{
			newPoint = m_Nimrod.selectRandomFromContainer(m_IM, true);

			if(m_Nimrod.debugEnabled(DebugLevel::Debug))
				m_Nimrod.printf("    Point: %s, IM size = %u\n", newPoint->toString().c_str(), (uint32_t)(m_IM.size() + 1));
		}
		while(m_STM.isTabu(newPoint) && !m_IM.empty());
	}
	catch(std::out_of_range&)
	{
		/* IM is empty, reduce. */
		newPoint = reduceMove();
	}

	if(m_Nimrod.debugEnabled(DebugLevel::Debug))
		m_Nimrod.printf("  IM size (after) = %u\n", (uint32_t)m_IM.size());

	++m_Intensification;

	if(m_Nimrod.debugEnabled(DebugLevel::Debug))
		m_Nimrod.printf("TabuSearch::intensifyMove() exit\n");

	return newPoint;
}

Point::Ref TabuSearch::reduceMove()
{
	Point::Ref newPoint;

	if(m_Nimrod.debugEnabled(DebugLevel::Debug))
	{
		m_Nimrod.printf("TabuSearch::reduceMove() enter\n");
		m_Nimrod.printf("  IM size (before) = %u\n", (uint32_t)m_IM.size());
		m_Nimrod.printf("  Reduction Loop:\n");
	}

	if(m_Nimrod.debugEnabled(DebugLevel::MoreDebug))
	{
		m_Nimrod.printf("  STM Dump:\n");
		size_t i = 0;
		for(const Point::Ref& pt : m_STM)
		{
			m_Nimrod.printf("  [%02u] %s\n", (uint32_t)i, pt->toString(false).c_str());
			++i;
		}
	}

	do
	{
		newPoint = m_Nimrod.selectRandomFromContainer(m_MTM, false);

		if(m_Nimrod.debugEnabled(DebugLevel::Debug))
			m_Nimrod.printf("    Point: %s, IM size = %u\n", newPoint->toString().c_str(), (uint32_t)m_IM.size());
	}
	while(m_STM.isTabu(newPoint));

	++m_Reduction;

	if(m_Nimrod.debugEnabled(DebugLevel::Debug))
		m_Nimrod.printf("TabuSearch::reduceMove() exit\n");

	return newPoint;
}

Point::Ref TabuSearch::increase(const Point& _template, size_t index) const
{
	double tempStep = m_InitialStep[index] * std::pow(m_Config.SSRF, m_Reduction);

	DoubleVector coords(_template.getCoordinates());

	double& val = coords[index];

	if(std::fabs((val += tempStep)) < DoubleVector::epsilon)
		val = 0.0;

	return m_Nimrod.pointDb.createPoint(coords);
}

Point::Ref TabuSearch::decrease(const Point& _template, size_t index) const
{
	double tempStep = m_InitialStep[index] * std::pow(m_Config.SSRF, m_Reduction);

	DoubleVector coords(_template.getCoordinates());

	double& val = coords[index];

	if(std::fabs((val -= tempStep)) < DoubleVector::epsilon)
		val = 0.0;

	return m_Nimrod.pointDb.createPoint(coords);
}

HookeJeevesState::HookeJeevesState()
{
	reset();
}

void HookeJeevesState::reset()
{
	remainingSampleSets = 0;
	genPoints.clear();
	evaluatedPoints.clear();
	nextPoint = nullptr;
	dominated.clear();
	dominant.clear();
	setCounter = -1;

	inEval = false;
}

void HookeJeevesState::write(CIOContext& ctx)
{
	ctx.write_uint64((uint64_t)remainingSampleSets, "remaining_sample_sets");

	saveContainer(genPoints, ctx, "gen_points");
	saveContainer(evaluatedPoints, ctx, "evaluated_points");

	ctx.write_point_reference(nextPoint, "next_point");

	saveContainer(dominated, ctx, "dominated");
	saveContainer(dominant, ctx, "dominant");

	ctx.write_uint64((uint64_t)setCounter, "set_counter");
	ctx.write_int32((int32_t)inEval, "in_eval");
}

void HookeJeevesState::read(CIOContext& ctx, PointDB& db)
{
	remainingSampleSets = (size_t)ctx.read_uint64("remaining_sample_sets");

	restoreContainer(db, genPoints, ctx, "gen_points");
	restoreContainer(db, evaluatedPoints, ctx, "evaluated_points");

	nextPoint = ctx.read_point_reference(db, "next_point");

	restoreContainer(db, dominated, ctx, "dominated");
	restoreContainer(db, dominant, ctx, "dominant");

	setCounter = (size_t)ctx.read_uint64("set_counter");
	inEval = ctx.read_int32("in_eval") != 0;
}

DiversifyState::DiversifyState()
{
	reset();
}

void DiversifyState::reset()
{
	evalPatience = tabuPatience = 20;
}

void DiversifyState::write(CIOContext& ctx)
{
	ctx.write_uint64(static_cast<uint64_t>(evalPatience), "eval_patience");
	ctx.write_uint64(static_cast<uint64_t>(tabuPatience), "tabu_patience");
}

void DiversifyState::read(CIOContext& ctx)
{
	evalPatience = static_cast<size_t>(ctx.read_uint64("eval_patience"));
	tabuPatience = static_cast<size_t>(ctx.read_uint64("tabu_patience"));
}