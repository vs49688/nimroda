/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <assert.h>
#include "nimroda/mots2/MTMContainer.hpp"

using namespace NimrodA;
using namespace NimrodA::MOTS2;

MTMContainer::MTMContainer() : PointSet() {}

MTMContainer::MTMContainer(const PointSet& pt) : PointSet(pt) {}

void MTMContainer::removeDominatedPoints()
{
	/* Find all dominated points, we can't remove them as we go. */
	std::vector<Point::Ref> dominated(this->get_allocator());
	for(const Point::Ref& i : *this) for(const Point::Ref& j : *this)
	{
		/* We explicitly want a pointer compare here. */
		if(i == j)
			continue;

		if(i->dominates(*j) == 1)
			dominated.push_back(j);
	}

	/* Now we can remove them. */
	for(const Point::Ref& pt : dominated)
		erase(pt);
}

bool MTMContainer::addIfNotDominated(const Point::Ref& pt)
{
	Job_Status status = pt->getJobStatus();
	if(status != JOB_FAILED && status != JOB_COMPLETE)
		throw std::invalid_argument("MTMContainer::addIfNotDominated(), unevaluated point");

	for(const Point::Ref& point : *this)
	{
		if(point->dominates(*pt) == 1)
			return false;
	}

	return insert(pt), true;
}


double MTMContainer::calculateQualityIndicator(const DoubleVector& refPt) const
{
	double hv2 = 0;
	for(const Point::Ref& pt : *this)
		hv2 += pt->getObjectives().euclideanDistance(refPt);

	return hv2;
}