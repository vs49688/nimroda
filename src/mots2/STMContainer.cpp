/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <algorithm>
#include "nimroda/mots2/STMContainer.hpp"

using namespace NimrodA;
using namespace NimrodA::MOTS2;

STMContainer::STMContainer(size_t stmSize) :
	m_STMSize(stmSize)
{}

void STMContainer::add(const Point::Ref& pt)
{
	/*
	* Two options here:
	* 1. Ignore it if the point already exists.
	* 2. If it exists, delete it and insert it at the front.
	*/

	/* Option 1 */
	if(isTabu(pt))
		return;

	/* Option 2 */
	//IndexMap::const_iterator it = m_Set.find(pt);
	//if(it != m_Set.end())
	//{
	//	m_List.erase(it->second);
	//	m_Set.erase(it);
	//}

	/* If we're too big, cull a point. */
	if(size() >= m_STMSize)
	{
		Point::Ref old = back();
		pop_back();
	}

	/* Add the point. */
	push_front(pt);
}

bool STMContainer::isTabu(const Point::Ref& pt) const
{
	for(const Point::Ref& point : *this) if(*point == *pt)
		return true;

	return false;

	//return std::find(begin(), end(), pt) != end();
}