/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <cstring>
#include <stdexcept>
#include <algorithm>
#include "nimroda/nimroda_ip.h"
#include "nimroda/cxx/Nimrod.hpp"
#include "nimroda/cxx/CIOContext.hpp"

using namespace NimrodA;

Nimrod::Nimrod(nimroda_instance_t *instance) :
	instance(instance),
	dimensionality(instance->num_dims),
	numObjectives(instance->num_objectives),
	upperBound(instance->var_attribs.upper_bound, instance->num_dims),
	lowerBound(instance->var_attribs.lower_bound, instance->num_dims),
	range(upperBound - lowerBound),
	pointDb(instance->num_dims, instance->num_objectives),
	m_StartingPoint(pointDb.createPoint(&instance->starting_point)),
	m_CurrentBatch(nullptr),
	m_OptimStatus(*this)
{}

Point::ConstRef Nimrod::getStartingPoint()
{
	return m_StartingPoint;
}

void Nimrod::setState(OptimisationState state)
{
	m_CurrentBatch.reset();
	switch(state)
	{
		case OptimisationState::Stopped: this->instance->optim.state = STATE_STOPPED; break;
		case OptimisationState::Refire: this->instance->optim.state = STATE_REFIRE; break;
		case OptimisationState::WaitingForBatch: this->instance->optim.state = STATE_WAITING_FOR_BATCH; break;
		case OptimisationState::Finished: this->instance->optim.state = STATE_FINISHED; break;
	}
}

OptimisationState Nimrod::getState() const noexcept
{
	switch(instance->optim.state)
	{
		case STATE_STOPPED: return OptimisationState::Stopped;
		case STATE_WAITING_FOR_BATCH: return OptimisationState::WaitingForBatch;
		case STATE_REFIRE: return OptimisationState::Refire;
		case STATE_FINISHED: return OptimisationState::Finished;
	}

	return OptimisationState::Stopped;
}

void Nimrod::setBatch(const PointVector& batch)
{
	m_CurrentBatch.reset();
	m_CurrentBatch = std::unique_ptr<Batch>(new Batch(&instance->optim.current_batch, batch));
}

const Batch& Nimrod::getBatch() const
{
	if(getState() != OptimisationState::WaitingForBatch)
		throw std::runtime_error("Attempted batch retrieval in invalid state");

	if(m_CurrentBatch->m_pBatch != &instance->optim.current_batch)
		throw std::runtime_error("Detached batch. This is a bug - please report this to the developer.");

	return *m_CurrentBatch;
}

bool Nimrod::isValid(const Point& pt)
{
	const DoubleVector& point = pt.getCoordinates();

	for(size_t i = 0; i < point.getLength(); ++i)
	{
		if((point[i] > upperBound[i]) || (point[i] < lowerBound[i]))
			return false;
	}

	return true;
}

void Nimrod::puts(const char *s)
{
	return instance->puts(instance, s);
}

void Nimrod::printf(const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	this->vprintf(fmt, ap);
	va_end(ap);
}

void Nimrod::vprintf(const char *fmt, va_list ap)
{
	va_list _ap;
	va_copy(_ap, ap);
	nimroda_vprintf(instance, fmt, _ap);
	va_end(_ap);
}

DebugLevel Nimrod::getDebugLevel() const noexcept
{
	switch(instance->config.debug)
	{
		case 0: return DebugLevel::None;
		case 1: return DebugLevel::Error;
		case 2: return DebugLevel::Warn;
		case 3: return DebugLevel::Info;
		case 4: return DebugLevel::MoreInfo;
		case 5: return DebugLevel::Debug;
		case 6: return DebugLevel::MoreDebug;
		default: return DebugLevel::Trace;
	}
}

bool Nimrod::debugEnabled(DebugLevel level) const noexcept
{
	switch(level)
	{
		case DebugLevel::None: return true;
		case DebugLevel::Error: return instance->config.debug >= 1;
		case DebugLevel::Warn: return instance->config.debug >= 2;
		case DebugLevel::Info: return instance->config.debug >= 3;
		case DebugLevel::MoreInfo: return instance->config.debug >= 4;
		case DebugLevel::Debug: return instance->config.debug >= 5;
		case DebugLevel::MoreDebug: return instance->config.debug >= 6;
		case DebugLevel::Trace: return instance->config.debug >= 7;
	}

	return false;
}

uint32_t Nimrod::randomUInt(uint32_t min, uint32_t max) noexcept
{
	return nimroda_random_uint(instance, min, max);
}

double Nimrod::randomDouble(double min, double max) noexcept
{
	return nimroda_random_double(instance, min, max);
}

nimroda_point_t *Nimrod::copyPoint(nimroda_point_t *dst, const Point& src)
{
	nimroda_point_t *pt = nimroda_point_copy_inplace(instance, dst, src);
	if(!pt)
		throw std::bad_alloc();

	return pt;
}

const char *Nimrod::getConfig(const char *key) const noexcept
{
	const ConfigMap& map = getConfig();
	auto it = map.find(key);
	return it != map.end() ? it->second.c_str() : "";
}

const ConfigMap& Nimrod::getConfig() const noexcept
{
	return *reinterpret_cast<ConfigMap*>(instance->__cconfig);
}

OptimStatus& Nimrod::getOptimStatus() noexcept
{
	return m_OptimStatus;
}

size_t Nimrod::getEvalCount() const noexcept
{
	return instance->optim.num_evaluations;
}

void Nimrod::writeStats(const StatsVector& stats) noexcept
{
	nimroda_kv_t *_stats = new nimroda_kv_t[stats.size()];
	for(size_t i = 0; i < stats.size(); ++i)
	{
		_stats[i].key = stats[i].first.c_str();
		_stats[i].value = stats[i].second.c_str();
	}

	instance->write_stats(instance, _stats, stats.size());

	delete[] _stats;
}

void Nimrod::save(CIOContext& ctx)
{
	ctx.write_point_reference(m_StartingPoint, "starting_point");

	int32_t hasBatch = (int32_t)(m_CurrentBatch && m_CurrentBatch->m_pBatch == &instance->optim.current_batch);
	ctx.write_int32(hasBatch, "has_batch");

	if(hasBatch)
	{
		CIOContext bctx(ctx, true, "batch");
		for(const auto& i : *m_CurrentBatch)
			bctx.write_point_reference(i);
	}
}

void Nimrod::restore(CIOContext& ctx)
{
	/* PointDB is restored externally, we can't hold onto the references here for long enough. */

	m_StartingPoint = ctx.read_point_reference(pointDb, "starting_point");

	int32_t hasBatch = ctx.read_int32("has_batch");

	if(hasBatch > 0)
	{
		/* This is a PITA. We have to copy backup the old batch,
		 * and restore it in the wrapped version. */
		nimroda_batch_t batch = instance->optim.current_batch;

		CIOContext bctx(ctx, true, "batch");
		PointVector points(batch.num_points);
		for(size_t i = 0; i < batch.num_points; ++i)
			points[i] = bctx.read_point_reference(pointDb);

		/* Now, release the old batch (if any). This has to be done here
		 * as the old batch might be pointing to an existing managed batch. */
		m_CurrentBatch.reset(); /* <-- If managed, free everything. */
		nimroda_batch_cleanup(&batch);

		this->setBatch(points);
	}
	else
	{
		m_CurrentBatch.reset();
	}
}