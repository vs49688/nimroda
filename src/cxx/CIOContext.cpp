/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <cstring>
#include "nimroda/cxx/CIOContext.hpp"
#include "nimroda/cxx/Point.hpp"
#include "nimroda/cxx/PointDB.hpp"

using namespace NimrodA;

CIOContext::CIOContext(cio_context_t *ctx, bool wrap) :
	m_pContext(ctx)
{
	if(!wrap)
		memset(&m_Context, 0, sizeof(m_Context));
}

CIOContext::CIOContext(CIO_PUSHPOPPROC push, CIO_PUSHPOPPROC pop, CIO_READPROC read, CIO_WRITEPROC write, void *user) :
	CIOContext(&m_Context)
{
	m_pContext->push = push;
	m_pContext->pop = pop;
	m_pContext->read = read;
	m_pContext->write = write;
	m_pContext->user = user;
}

CIOContext::CIOContext(cio_context_t *parent, bool list, const char *name) : CIOContext(&m_Context)
{
	cio_context_t *ctx = list ? cio_push_list(m_pContext, parent, name) : cio_push_dict(m_pContext, parent, name);
	if(!ctx)
		throw Exception();
}

CIOContext::~CIOContext()
{
	if(m_pContext == &m_Context)
		cio_pop(m_pContext);
}

void CIOContext::write(const void *buffer, size_t size, cio_type_t type, const char *name)
{
	if(cio_write(*this, buffer, size, type, name))
		throw Exception();
}

void CIOContext::read(void *buffer, size_t size, cio_type_t type, const char *name)
{
	if(cio_read(*this, buffer, size, type, name))
		throw Exception();
}

void CIOContext::write_string(const char *s, const char *name)
{
	if(cio_write_string(*this, s, name))
		throw Exception();
}

void CIOContext::write_string(const std::string& s, const char *name)
{
	return write_string(s.c_str(), name);
}

std::string CIOContext::read_string(const char *name)
{
	size_t len = 0;
	if(cio_read_string(*this, nullptr, &len, name))
		throw Exception();

	std::unique_ptr<char[]> buffer(new char[len + 1]);

	/* The standard says I can do this. #yolo */
	if(cio_read_string(*this, buffer.get(), &len, name))
		throw Exception();

	return std::string(buffer.get());
}

void CIOContext::write_point(const nimroda_point_t * const point)
{
	write_uint64((uint64_t)point->num_dims, "num_dims");
	write_uint64((uint64_t)point->num_objectives, "num_objectives");
	write_int32((int32_t)point->status, "status");

	{
		CIOContext t(*this, true, "coords");
		for(size_t i = 0; i < point->num_dims; ++i)
			t.write_float64(point->coord[i]);
	}

	{
		CIOContext t(*this, true, "objectives");
		for(size_t i = 0; i < point->num_objectives; ++i)
			t.write_float64(point->objectives[i]);
	}
}

void CIOContext::read_point(nimroda_point_t *point)
{
	size_t numDims = (size_t)read_uint64("num_dims");
	size_t numObjectives = (size_t)read_uint64("num_objectives");

	memset(point, 0, sizeof(nimroda_point_t));
	if(!nimroda_point_alloc_inplace_manual(numDims, numObjectives, point))
		throw std::bad_alloc();

	try
	{
		point->num_dims = numDims;
		point->num_objectives = numObjectives;
		read_point_noalloc2(point);
	}
	catch(...)
	{
		nimroda_point_release_inplace(point);
		throw;
	}
}

void CIOContext::read_point_noalloc(nimroda_point_t *point)
{
	if((size_t)read_uint64("num_dims") != point->num_dims)
		throw std::runtime_error("Invalid coordinate count");

	if((size_t)read_uint64("num_objectives") != point->num_objectives)
		throw std::runtime_error("Invalid objective count");

	read_point_noalloc2(point);
}

void CIOContext::read_point_noalloc2(nimroda_point_t *point)
{
	point->status = (Job_Status)read_int32("status");

	{
		CIOContext t(*this, true, "coords");
		for(size_t i = 0; i < point->num_dims; ++i)
			point->coord[i] = t.read_float64(i);
	}

	{
		CIOContext t(*this, true, "objectives");
		for(size_t i = 0; i < point->num_objectives; ++i)
			point->objectives[i] = t.read_float64(i);
	}
}

ScopedPoint CIOContext::read_point()
{
	size_t numDims = (size_t)read_uint64("num_dims");
	size_t numObjectives = (size_t)read_uint64("num_objectives");

	ScopedPoint point(nimroda_point_alloc_manual(numDims, numObjectives), nimroda_point_release);
	if(!point)
		throw std::bad_alloc();

	point->num_dims = numDims;
	point->num_objectives = numObjectives;
	read_point_noalloc2(point.get());

	return point;
}

void CIOContext::write_point_reference(const std::shared_ptr<const Point>& pt, const char *name)
{
	CIOContext ctx(*this, false, name);
	if(!pt)
		ctx.write_uint32(0, "instance_id");
	else
		ctx.write_uint32(pt->instanceId, "instance_id");
}

std::shared_ptr<Point> CIOContext::read_point_reference(PointDB& db, const char *name)
{
	CIOContext ctx(*this, false, name);

	uint32_t id = ctx.read_uint32("instance_id");

	if(id == 0)
		return std::shared_ptr<Point>();

	return db.lookupPoint(id);
}

void CIOContext::write_coord_vector(const CoordinateVector& vec, const char *name)
{
	CIOContext ctx(*this, false, name);
	ctx.write_uint64((size_t)vec.getLength(), "length");

	{
		CIOContext pctx(ctx, true, "coordinates");
		for(size_t i = 0; i < vec.getLength(); ++i)
			pctx.write_float64(vec[i]);
	}
}

CoordinateVector CIOContext::read_coord_vector(const char *name)
{
	CIOContext ctx(*this, false, name);
	size_t length = (size_t)ctx.read_uint64("length");

	CoordinateVector vec(length);

	{
		CIOContext pctx(ctx, true, "coordinates");
		for(size_t i = 0; i < vec.getLength(); ++i)
			vec[i] = pctx.read_float64();
	}

	return vec;
}

CIOContext::operator cio_context_t *() const noexcept { return m_pContext; }

CIOContext::Exception::Exception() :
	std::runtime_error("I/O error")
{}