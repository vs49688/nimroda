/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <functional>
#include "nimroda/cxx/PointDB.hpp"
#include "nimroda/cxx/CIOContext.hpp"

using namespace NimrodA;

PointDB::PointDB(size_t numCoords, size_t numObjectives) :
	m_NumCoords(numCoords),
	m_NumObjectives(numObjectives),
	m_FailedObjectivesVector(m_NumObjectives, HUGEISH),
	m_RNG(0)
{}

Point::Ref PointDB::lookupPoint(Point::Id id) noexcept
{
	auto it = m_Points.find(id);
	if(it != m_Points.end())
		return it->second->shared_from_this();

	return Point::Ref();
}

Point::ConstRef PointDB::lookupPoint(Point::Id id) const noexcept
{
	auto it = m_Points.find(id);
	if(it != m_Points.end())
		return it->second->shared_from_this();

	return Point::ConstRef();
}


Point::Ref PointDB::createPoint(const DoubleVector& coords)
{
	return createPoint(coords, m_FailedObjectivesVector);
}

Point::Ref PointDB::createPoint(const std::vector<double>& coords)
{
	return createPoint(coords.data(), coords.size(), m_FailedObjectivesVector.getData(), m_FailedObjectivesVector.getLength());
}

Point::Ref PointDB::createPoint(const DoubleVector& coords, const DoubleVector& objectives)
{
	return createPoint(coords.getData(), coords.getLength(), objectives.getData(), objectives.getLength());
}

Point::Ref PointDB::createPoint(const Point& pt)
{
	return createPoint(pt.getCoordinates(), pt.getObjectives());
}

Point::Ref PointDB::createPoint(const Point::ConstRef& pt)
{
	return createPoint(*pt);
}

Point::Ref PointDB::createPoint(const nimroda_point_t *pt)
{
	struct tmp : public Point { tmp(Point::Id n, const nimroda_point_t * const pt) : Point(n, pt) {} };

	Point::Id id = generateId();
	Point *_pt = new tmp(id, pt);
	Point::Ref spt(_pt, std::bind(&PointDB::deletePoint, this, std::placeholders::_1));
	m_Points.insert(std::make_pair(id, _pt));
	return spt;
}

Point::Ref PointDB::createPoint(const double *coords, size_t numCoords, const double *objectives, size_t numObjectives)
{
	return createPoint(generateId(), coords, numCoords, objectives, numObjectives);
}

Point::Ref PointDB::createPoint(Point::Id id, const double *coords, size_t numCoords, const double *objectives, size_t numObjectives)
{
	if(numCoords != m_NumCoords)
		throw std::invalid_argument("numCoords != m_NumCoords");

	if(numObjectives != m_NumObjectives)
		throw std::invalid_argument("numObjectives != m_NumObjectives");

	struct tmp : public Point
	{
		tmp(Point::Id n, const double *c, size_t nc, const double *o, size_t no) : Point(n, c, nc, o, no) {}
	};

	Point *pt = new tmp(id, coords, numCoords, objectives, numObjectives);

	Point::Ref spt(pt, std::bind(&PointDB::deletePoint, this, std::placeholders::_1));

	m_Points.insert(std::make_pair(id, pt));
	return spt;
}

Point::Id PointDB::generateId() noexcept
{
	Point::Id id;
	do
	{
		/* We don't care about determinism here, so just use uniform_int_distribution */
		id = std::uniform_int_distribution<Point::Id>(0, UINT32_MAX)(m_RNG);
	}
	while(m_Points.find(id) != m_Points.end());

	return id;
}

void PointDB::deletePoint(Point *pt)
{
	m_Points.erase(pt->instanceId);
	delete pt;
}

void PointDB::save(CIOContext& ctx)
{
	ctx.write_uint64((uint64_t)m_Points.size(), "num_points");

	CIOContext dbctx(ctx, true, "points");

	for(const auto& pt : m_Points)
	{
		CIOContext pctx(dbctx, false, nullptr);
		pctx.write_uint32(pt.second->instanceId, "instance_id");
		pctx.write_point(pt.second->operator const nimroda_point_t *());
	}
}

std::vector<Point::Ref> PointDB::restore(CIOContext& ctx)
{
	m_Points.clear();

	size_t numPoints = (size_t)ctx.read_uint64("num_points");
	std::vector<Point::Ref> points(numPoints);

	CIOContext dbctx(ctx, true, "points");

	nimroda_point_t pt;

	ScopedPoint tmpPoint(nimroda_point_alloc_inplace_manual(m_NumCoords, m_NumObjectives, &pt), nimroda_point_release_inplace);
	if(!tmpPoint)
		throw std::bad_alloc();

	for(size_t i = 0; i < numPoints; ++i)
	{
		CIOContext pctx(dbctx, false, nullptr);
		Point::Id id = pctx.read_uint32("instance_id");
		pctx.read_point_noalloc(tmpPoint.get());
		points[i] = createPoint(id, tmpPoint->coord, tmpPoint->num_dims, tmpPoint->objectives, tmpPoint->num_objectives);
	}

	return points;
}
