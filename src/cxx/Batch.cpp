/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "nimroda/cxx/Batch.hpp"
#include "nimroda/cxx/Nimrod.hpp"

using namespace NimrodA;

Batch::Batch(const std::vector<Point::Ref>& points) :
	Batch(&m_Batch, points)
{
}

/* Wrapping constructor */
Batch::Batch(nimroda_batch_t *batch, const std::vector<Point::Ref>& points) :
	numPoints(points.size()),
	m_pBatch(batch),
	m_PointVector(points),
	m_RawVector(numPoints, nullptr)
{
	m_pBatch->num_points = numPoints;
	m_pBatch->points = const_cast<nimroda_point_t**>(m_RawVector.data());

	for(size_t i = 0; i < m_PointVector.size(); ++i)
		m_pBatch->points[i] = &m_PointVector[i]->m_Point;
}

Batch::~Batch()
{
	/* If we're wrapping, reset the structure. */
	if(m_pBatch != &m_Batch)
	{
		m_pBatch->num_points = 0;
		m_pBatch->points = nullptr;
	}
}

Point::Ref Batch::get(size_t index) const
{
	if(index >= numPoints)
		throw std::out_of_range("point index out of range");

	return m_PointVector[index];
}

PointVector::const_iterator Batch::begin() const noexcept
{
	return m_PointVector.begin();
}

PointVector::const_iterator Batch::end() const noexcept
{
	return m_PointVector.end();
}