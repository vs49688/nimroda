/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <string.h>
#include "nimroda/nimroda_ip.h"
#include "nimroda/cxx/OptimStatus.hpp"
#include "nimroda/cxx/Nimrod.hpp"

using namespace NimrodA;

OptimStatus::OptimStatus(Nimrod& nimrod) :
	m_Nimrod(nimrod),
	m_RawStatus(nimrod.instance->optim.status)
{}

OptimStatus::~OptimStatus()
{
	/* If we were using the optim status, nullify everything so nimroda_release
	 * doesn't try to double-free. */
	if(m_RawStatus.results == m_RawVector.data())
	{
		m_RawStatus.results = nullptr;
		m_RawStatus.result_count = 0;
	}
}

OptimisationState OptimStatus::getState() const noexcept
{
	m_RawStatus.state = this->m_Nimrod.instance->optim.state;
	return this->m_Nimrod.getState();
}

const std::vector<Point::Ref>& OptimStatus::getResults() const noexcept
{
	return m_Results;
}

const char *OptimStatus::getErrorString() const noexcept
{
	return m_RawStatus.message;
}

int32_t OptimStatus::getCode() const noexcept
{
	return m_RawStatus.code;
}

void OptimStatus::clearResults() noexcept
{
	m_Results.clear();
	m_RawVector.clear();
	m_RawStatus.results = m_RawVector.data();
	m_RawStatus.result_count = m_RawVector.size();
}

void OptimStatus::addResultPoint(const Point::Ref& pt)
{
	m_Results.push_back(pt);
	m_RawVector.push_back(&pt->m_Point);
	m_RawStatus.results = m_RawVector.data();
	m_RawStatus.result_count = m_RawVector.size();
}

void OptimStatus::setErrorString(const char *s)
{
	if(s == nullptr)
	{
		m_RawStatus.message[0] = '\0';
		return;
	}

	strncpy(m_RawStatus.message, s, LONGEST_ERROR_STRING);
	m_RawStatus.message[LONGEST_ERROR_STRING - 1] = '\0';
}

void OptimStatus::setCode(int32_t status)
{
	m_RawStatus.code = status;
}

void OptimStatus::setErrorString(const std::string& s)
{
	setErrorString(s.c_str());
}