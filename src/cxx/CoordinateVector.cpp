/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <cmath>
#include <istream>
#include <ostream>
#include <sstream>
#include <stdexcept>

#include "nimroda/nimroda_ip.h"
#include "nimroda/cxx/CoordinateVector.hpp"

using namespace NimrodA;

CoordinateVector::CoordinateVector() : CoordinateVector(0) {}

CoordinateVector::CoordinateVector(size_t size) : CoordinateVector(size, 0) {}

CoordinateVector::CoordinateVector(size_t size, double val) :
	m_Vector(size, val)
{}

CoordinateVector::CoordinateVector(const std::vector<double>& vec) :
	m_Vector(vec)
{}

CoordinateVector::CoordinateVector(const double *coords, size_t count) :
	m_Vector(coords, coords + count)
{}

CoordinateVector::CoordinateVector(const CoordinateVector& rhs) :
	m_Vector(rhs.m_Vector.begin(), rhs.m_Vector.end())
{}

CoordinateVector::CoordinateVector(CoordinateVector&& rhs) : 
	m_Vector(std::move(rhs.m_Vector))
{}

size_t CoordinateVector::getLength() const { return m_Vector.size(); }

const double *CoordinateVector::getData() const { return m_Vector.data(); }

double *CoordinateVector::getData() { return m_Vector.data(); }

double CoordinateVector::euclideanDistance(const CoordinateVector& pt) const
{
	ensureCompatibility(pt);

	double ret = 0.0;

	for(size_t i = 0; i < m_Vector.size(); ++i)
		ret += std::pow(m_Vector[i] - pt[i], 2);

	return pow(ret, 0.5);
}

size_t CoordinateVector::hashCode() const
{
	return nimroda_hash_double_array(m_Vector.data(), m_Vector.size());
}

double CoordinateVector::operator[](size_t index) const
{
	if(index >= m_Vector.size())
		throw std::out_of_range("invalid index");

	return m_Vector[index];
}

double& CoordinateVector::operator[](size_t index)
{
	if(index >= m_Vector.size())
		throw std::out_of_range("invalid index");

	return m_Vector[index];
}

bool CoordinateVector::isCompatible(const CoordinateVector& pt) const noexcept
{
	return this->getLength() == pt.getLength();
}

void CoordinateVector::ensureCompatibility(const CoordinateVector& pt) const
{
	if(!isCompatible(pt))
		throw std::length_error("Mismatching coordinate count");
}

CoordinateVector& CoordinateVector::operator+=(const CoordinateVector& rhs)
{
	ensureCompatibility(rhs);

	for(size_t i = 0; i < m_Vector.size(); ++i)
	{
		m_Vector[i] = m_Vector[i] + rhs.m_Vector[i];
		if(fabs(m_Vector[i]) < epsilon)
			m_Vector[i] = 0.0;
	}

	return *this;
}

CoordinateVector& CoordinateVector::operator+=(double n)
{
	for(size_t i = 0; i < m_Vector.size(); ++i)
	{
		m_Vector[i] = m_Vector[i] + n;
		if(fabs(m_Vector[i]) < epsilon)
			m_Vector[i] = 0.0;
	}

	return *this;
}

CoordinateVector& CoordinateVector::operator-=(const CoordinateVector& rhs)
{
	ensureCompatibility(rhs);

	for(size_t i = 0; i < m_Vector.size(); ++i)
	{
		m_Vector[i] = m_Vector[i] - rhs.m_Vector[i];
		if(fabs(m_Vector[i]) < epsilon)
			m_Vector[i] = 0.0;
	}

	return *this;
}

CoordinateVector& CoordinateVector::operator-= (double n)
{
	for(size_t i = 0; i < m_Vector.size(); ++i)
	{
		m_Vector[i] = m_Vector[i] - n;
		if(fabs(m_Vector[i]) < epsilon)
			m_Vector[i] = 0.0;
	}

	return *this;
}

CoordinateVector& CoordinateVector::operator*=(const CoordinateVector& rhs)
{
	ensureCompatibility(rhs);

	double result = 0.0;

	//calculate the absolute error
	for(size_t i = 0; i < m_Vector.size(); ++i)
	{
		result = m_Vector[i] * rhs.m_Vector[i];
		if(fabs(result) < epsilon)
			result = 0.0;

		m_Vector[i] = result;
	}

	return *this;
}

CoordinateVector& CoordinateVector::operator*= (double n)
{
	double result = 0.0;

	//calculate the absolute error
	for(size_t i = 0; i < m_Vector.size(); ++i)
	{
		result = m_Vector[i] * n;
		if(fabs(result) < epsilon)
			result = 0.0;

		m_Vector[i] = result;
	}

	return *this;
}

CoordinateVector& CoordinateVector::operator/=(const CoordinateVector& rhs)
{
	double result = 0.0;

	//calculate the absolute error
	for(size_t i = 0; i < m_Vector.size(); ++i)
	{
		result = m_Vector[i] / rhs.m_Vector[i];
		if(fabs(result) < epsilon)
			result = 0.0;

		m_Vector[i] = result;
	}

	return *this;
}

CoordinateVector& CoordinateVector::operator/=(double n)
{
	double result = 0.0;

	//calculate the absolute error
	for(size_t i = 0; i < m_Vector.size(); ++i)
	{
		result = m_Vector[i] / n;
		if(fabs(result) < epsilon)
			result = 0.0;

		m_Vector[i] = result;
	}

	return *this;
}

CoordinateVector CoordinateVector::operator+(const CoordinateVector& rhs) const
{
	CoordinateVector vec(*this);
	vec += rhs;
	return vec;
}

CoordinateVector CoordinateVector::operator+(double n) const
{
	CoordinateVector vec(*this);
	vec += n;
	return vec;
}

CoordinateVector CoordinateVector::operator-(const CoordinateVector& rhs) const
{
	CoordinateVector vec(*this);
	vec -= rhs;
	return vec;
}

CoordinateVector CoordinateVector::operator-(double n) const
{
	CoordinateVector vec(*this);
	vec -= n;
	return vec;
}

CoordinateVector CoordinateVector::operator/(const CoordinateVector& rhs) const
{
	CoordinateVector vec(*this);
	vec /= rhs;
	return vec;
}

CoordinateVector CoordinateVector::operator/(double n) const
{
	CoordinateVector vec(*this);
	vec /= n;
	return vec;
}

CoordinateVector& CoordinateVector::operator=(const CoordinateVector& rhs)
{
	return *this = rhs.m_Vector;
}

CoordinateVector& CoordinateVector::operator=(CoordinateVector&& rhs)
{
	return *this = std::move(rhs.m_Vector);
}

CoordinateVector& CoordinateVector::operator=(double n)
{
	for(size_t i = 0; i < m_Vector.size(); ++i)
		m_Vector[i] = n;

	return *this;
}

CoordinateVector& CoordinateVector::operator=(const std::vector<double>& rhs)
{
	return m_Vector = rhs, *this;
}

CoordinateVector& CoordinateVector::operator=(std::vector<double>&& rhs)
{
	return m_Vector = std::move(rhs), *this;
}


bool CoordinateVector::operator==(const CoordinateVector& rhs) const
{
	if(m_Vector.size() != rhs.m_Vector.size())
		return false;

	for(size_t i = 0; i < m_Vector.size(); ++i)
	{
		/* Should I use epsilon here or just a direct compare? */
		double diff = fabs(m_Vector[i] - rhs.m_Vector[i]);
		if(diff >= epsilon)
			return false;
	}

	return true;
}

bool CoordinateVector::operator!=(const CoordinateVector& rhs) const
{
	return !(*this == rhs);
}

bool CoordinateVector::operator<(const CoordinateVector& rhs) const
{
	ensureCompatibility(rhs);

	//typedef int32_t fixed_ls; /* signed 15.16 fixed-point */
	//double val = 0;
	//fixed_ls fVal = (fixed_ls)(val*65536.0f);
	//return nimroda_hash(&fVal, sizeof(fVal));

	for(size_t i = 0; i < m_Vector.size(); ++i)
	{
		//double v1 = (*this)[i];
		//double v2 = rhs[i];

		//int32_t fV1 = (int32_t)(v1 * 65536.0f);
		//int32_t fV2 = (int32_t)(v2 * 65536.0f);

		//double nv1 = fV1 / 65536.0f;
		//double nv2 = fV2 / 65536.0f;
		//if(fV1 > fV2)
		//	return false;

		//volatile double mv1 = (v1 * 1000000.0f) / 1000000.0f;
		//volatile double mv2 = (v2 * 1000000.0f) / 1000000.0f;

		//if(mv1 > mv2)
			//return false;
		//double diff = (*this)[i] - rhs[i];
		//if(diff > epsilon)
		//	return false;

		if((*this)[i] > rhs[i])
			return false;
	}

	return true;
}

std::string CoordinateVector::toString() const
{
	std::stringstream ss;
	
	ss << "(";
	
	for(size_t i = 0; i < m_Vector.size(); ++i)
	{
		if(i != 0)
			ss << ", ";
	
		ss << m_Vector[i];
	}
	
	ss << ")";
	
	return ss.str();
}

int CoordinateVector::dominates(const CoordinateVector& rhs) const
{
	ensureCompatibility(rhs);

	size_t numGreater = 0, numLess = 0;

	for(size_t i = 0; i < m_Vector.size(); ++i)
	{
		double o1 = (*this)[i];
		double o2 = rhs[i];

		if(o1 > o2)
			++numGreater;
		else if(o1 < o2)
			++numLess;
	}

	/* No component is greater and at least one component is smaller. */

	if(numGreater > 0 && numLess == 0)
		return -1; /* pt dominates this */
	else if(numLess > 0 && numGreater == 0)
		return 1; /* this dominates pt */
	else
		return 0;
}


CoordinateVector CoordinateVector::readVector(std::istream& s)
{
	try
	{
		uint32_t coordCount;

		s >> coordCount;

		if(!coordCount)
			return CoordinateVector();

		CoordinateVector coords(coordCount);

		for(uint32_t i = 0; i < coordCount; ++i)
			s >> coords[i];

		return coords;
	}
	catch(...)
	{
		return CoordinateVector();
	}
}


void CoordinateVector::writeVector(std::ostream& s)
{
	// <size> <coord1> <coord2> ...

	s << (uint32_t)m_Vector.size() << " ";

	for(size_t i = 0; i < m_Vector.size(); ++i)
		s << m_Vector[i] << " ";
}
