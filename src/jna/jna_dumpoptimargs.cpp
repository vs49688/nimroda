/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stdio.h>
#include "nimroda/nimroda_ip.h"

template <class T>
static void dumpArray(FILE *f, T *coords, size_t num, const char *format)
{
	fprintf(f, "(");
	for(size_t i = 0; i < num; ++i)
	{
		fprintf(f, format, coords[i]);
		
		if(i != num-1)
		{
			fprintf(f, ", ");
		}
	}
	
	fprintf(f, ")");
}

static const char *getJobString(enum Job_Status status)
{
	switch(status)
	{
		case JOB_COMPLETE: return "JOB_COMPLETE";
		case JOB_FAILED: return "JOB_FAILED";
		case JOB_NOT_SENT: return "JOB_NOT_SENT";
		default: return "Unknown";
	}
}

static void dumpPoint(FILE *f, nimroda_point_t *point, size_t numPoints)
{
	int dim = 0, numObj = 0;
	for(int i = 0; i < (int)numPoints; ++i)
	{
		nimroda_point_t *p = &point[i];
		fprintf(f, "    &(Point %d)                             = %p\n", i, p);
		fprintf(f, "     (Point %u)->num_dims                   = %d\n", i, (uint32_t)p->num_dims);
		
		fprintf(f, "     (Point %d)->coord                      = %p\n", i, p->coord);
		if(p->coord)
		{
			fprintf(f, "    *(Point %d)->coord                      = ", i);
			dumpArray(f, p->coord, p->num_dims, "%lf");
			fprintf(f, "\n");
		}
		
		fprintf(f, "     (Point %d)->status                     = %s (%d)\n", i, getJobString(p->status), (int)p->status);
		
		fprintf(f, "     (Point %d)->num_objectives             = %u\n", i, (uint32_t)p->num_objectives);
		fprintf(f, "     (Point %d)->objectives        = %p\n", i, p->objectives);
		if(p->objectives)
		{
			fprintf(f, "    *(Point %d)->objectives        = ", i);
			dumpArray(f, p->objectives, p->num_objectives, "%lf");
			fprintf(f, "\n");
		}
	}
}

static void dumpStatus(FILE *f, nimroda_optim_status_t *status)
{
	fprintf(f, "    how_opt_ended                          = %d\n", status->code);
	fprintf(f, "    message                                = \"%s\"\n", status->message);
}

static void dumpVarble(FILE *f, nimroda_variable_attributes_t *attrib)
{
	fprintf(f, "    lower_bound                            = ");
	dumpArray(f, attrib->lower_bound, MAX_DIMS, "%lf");
	fprintf(f, "\n");
	
	fprintf(f, "    upper_bound                            = ");
	dumpArray(f, attrib->upper_bound, MAX_DIMS, "%lf");
	fprintf(f, "\n");
	
	fprintf(f, "    name : data_type : incrementer\n");
	
	for(int i = 0; i < MAX_DIMS; ++i)
	{
		fprintf(f, "      [%03d] = \"%s\"\n", i, attrib->name[i]);
	}

	fprintf(f, "    range                                  = ");
	dumpArray(f, attrib->range, MAX_DIMS, "%lf");
	fprintf(f, "\n");
}


extern "C" void jnaDumpOptimArgs(nimroda_point_t *current_point, double penalty_factor, int which_start, struct Counts *counts, struct Combined_results *results_for_set, double standard_deviation, nimroda_optim_status_t *op_stat, nimroda_variable_attributes_t *vars, struct Opt_Settings *setting, char title[])
{
	FILE *f = fopen("optargs-dump.log", "a+");
	if(!f)
		return;
	
	fprintf(f, "Optimize Called:\n");
	fprintf(f, "  Current Point:\n");
	dumpPoint(f, current_point, 1);
	fprintf(f, "  Penalty Factor: %lf\n", penalty_factor);
	fprintf(f, "  Which Start:    %d\n", which_start);
	fprintf(f, "  Standard Dev:   %lf\n", standard_deviation);
	fprintf(f, "  Optim Status:\n");
	dumpStatus(f, op_stat);
	fprintf(f, "  Varble Attribs:\n");
	dumpVarble(f, vars);
	
	
	fprintf(f, "  Title:          \"%s\"\n", title);
	
	fclose(f);
}

extern "C" NIMRODA_INTERNAL_API void jnaDumpVarbleAttribs(nimroda_variable_attributes_t *vars)
{
	dumpVarble(stdout, vars);
}

extern "C" NIMRODA_INTERNAL_API void jnaDumpStateVarbleAttribs(nimroda_instance_t *instance)
{
	dumpVarble(stdout, &instance->var_attribs);
}
