/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* A lightweight wrapper for JNA */
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "nimroda/nimroda_ip.h"

NIMRODA_API const nimroda_algorithm_t *jnaQueryAlgorithms(int32_t *size)
{
	size_t num = 0;
	const nimroda_algorithm_t *algos = nimroda_query_algorithms(&num);

	*size = num > INT32_MAX ? INT32_MAX : (int32_t)num;

	return algos;
}

NIMRODA_API nimroda_instance_t *jnaInit(nimroda_config_t *config)
{
	if(!config)
		return NULL;

	return nimroda_init(config);
}

NIMRODA_API void jnaRelease(nimroda_instance_t *instance)
{
	nimroda_release(instance);
}

NIMRODA_API int32_t jnaOptimise(nimroda_instance_t *instance)
{
	return (int32_t)nimroda_optimise(instance);
}

NIMRODA_API nimroda_batch_t *jnaGetBatch(nimroda_instance_t *instance)
{
	return nimroda_get_batch(instance);
}

NIMRODA_API int32_t jnaGetState(nimroda_instance_t *instance)
{
	return (int32_t)nimroda_get_optim_state(instance);
}

NIMRODA_API nimroda_optim_status_t *jnaGetOptimStatus(nimroda_instance_t *instance)
{
	return nimroda_get_optim_status(instance);
}

NIMRODA_API nimroda_variable_attributes_t *jnaGetVars(nimroda_instance_t *instance)
{
	return nimroda_get_vars(instance);
}

NIMRODA_API int32_t jnaGetDimensionality(nimroda_instance_t *instance)
{
	return (int32_t)nimroda_get_dimensionality(instance);
}

NIMRODA_API int32_t jnaGetObjectiveCount(nimroda_instance_t *instance)
{
	return (int32_t)nimroda_get_objective_count(instance);
}

NIMRODA_API cio_context_t *jnaCIOPushDict(cio_context_t *ctx, cio_context_t *parent, const char *name)
{
	return cio_push_dict(ctx, parent, name);
}

NIMRODA_API cio_context_t *jnaCIOPushList(cio_context_t *ctx, cio_context_t *parent, const char *name)
{
	return cio_push_list(ctx, parent, name);
}

NIMRODA_API cio_context_t *jnaCIOPop(cio_context_t *ctx)
{
	return cio_pop(ctx);
}

NIMRODA_API int jnaCIORead(cio_context_t *ctx, void *buffer, size_t size, int32_t type, const char *name)
{
	cio_type_t _type = (cio_type_t)type;
	return cio_read(ctx, buffer, size, type, name);
}

NIMRODA_API int jnaCIOWrite(cio_context_t *ctx, const void *buffer, size_t size, int32_t type, const char *name)
{
	cio_type_t _type = (cio_type_t)type;
	return cio_write(ctx, buffer, size, type, name);
}

NIMRODA_API uint64_t jnaHash(const void *data, size_t size)
{
	return nimroda_hash(data, size);
}

NIMRODA_API uint64_t jnaHashDouble(double val)
{
	return nimroda_hash_double(val);
}

NIMRODA_API uint64_t jnaHashDoubleArray(const double *p, size_t count)
{
	return nimroda_hash_double_array(p, count);
}

NIMRODA_API int32_t jnaSave(nimroda_instance_t *instance, cio_context_t *ctx)
{
	return nimroda_save(instance, ctx);
}

NIMRODA_API nimroda_instance_t *jnaRestore(nimroda_callbacks_t *callbacks, cio_context_t *ctx)
{
	return nimroda_restore(callbacks, ctx);
}

NIMRODA_API void *jnaTestMalloc(size_t size)
{
	void *ptr = malloc(size);

	if(ptr == NULL)
		abort();
	return ptr;
}

NIMRODA_API void jnaTestFree(void *ptr)
{
	free(ptr);
}

NIMRODA_API void jnaTestOneFill(void *ptr, size_t size)
{
	memset(ptr, ~0, size);
}

#define TESTPARAM_ALGORITHM 0
#define TESTPARAM_CONFIG 1
#define TESTPARAM_CUSTOM_CONFIG 2
#define TESTPARAM_OPTIM_STATUS 3
#define TESTPARAM_POINT 4
#define TESTPARAM_VARBLE_ATTRIB 5
#define TESTPARAM_BATCH 6

NIMRODA_API void jnaTestGetSampleStructure(int32_t structure, void *ptr)
{
	switch(structure)
	{
		case TESTPARAM_ALGORITHM:
			{
				nimroda_algorithm_t *algo = (nimroda_algorithm_t*)ptr;
				memset(algo, 0, sizeof(nimroda_algorithm_t));

				algo->unique_name = "SAMPLEALGORITHM";
				algo->name = "Sample Algorithm";
				algo->multi_objective = 100;
				algo->init = (PFNINIT)(uintptr_t)0xDEADBABE;
				algo->optimise = (PFNOPTIMISE)(uintptr_t)0xCAFEBABE;
				algo->deinit = (PFNDEINIT)(uintptr_t)0xDEADBEEF;
				break;
			}
		case TESTPARAM_CONFIG:
			{
				nimroda_config_t *cfg = (nimroda_config_t*)ptr;
				memset(cfg, 0, sizeof(nimroda_config_t));
				break;
			}
		case TESTPARAM_CUSTOM_CONFIG:
			{
				nimroda_custom_config_t *cfg = (nimroda_custom_config_t*)ptr;
				memset(cfg, 0, sizeof(nimroda_custom_config_t));

				cfg->key = "key";
				cfg->value = "value";
				break;
			}
		case TESTPARAM_OPTIM_STATUS:
			{
				nimroda_optim_status_t *opstat = (nimroda_optim_status_t*)ptr;
				memset(opstat, 0, sizeof(nimroda_optim_status_t));
				opstat->code = 200;
				opstat->state = STATE_WAITING_FOR_BATCH;
				strncpy(opstat->message, "Sample Optim_Status message", LONGEST_ERROR_STRING);
				opstat->results = (void*)(uintptr_t)0xDEADBABE;
				opstat->result_count = 101010101;
				break;
			}
		case TESTPARAM_POINT:
			{
				nimroda_point_t *pt = (nimroda_point_t*)ptr;
				memset(pt, 0, sizeof(nimroda_point_t));

				pt->num_dims = 10;
				pt->coord = (void*)(uintptr_t)0xDEADBABE;
				pt->status = JOB_FAILED;
				pt->num_objectives = 2;
				pt->objectives = (void*)(uintptr_t)0xDEADBEEF;
				break;
			}
		case TESTPARAM_VARBLE_ATTRIB:
			{
				nimroda_variable_attributes_t *vars = (nimroda_variable_attributes_t*)ptr;
				memset(vars, 0, sizeof(nimroda_variable_attributes_t));
				break;
			}
		case TESTPARAM_BATCH:
			{
				nimroda_batch_t *batch = (nimroda_batch_t*)ptr;
				batch->num_points = 69;
				batch->points = (void*)(uintptr_t)0xDEADBABE;
				break;
			}
	}
}

NIMRODA_API int32_t jnaTestSizeOf(int32_t structure)
{
	switch(structure)
	{
		case TESTPARAM_ALGORITHM: return (int32_t)sizeof(nimroda_algorithm_t);
		case TESTPARAM_CONFIG: return (int32_t)sizeof(nimroda_config_t);
		case TESTPARAM_CUSTOM_CONFIG: return (int32_t)sizeof(nimroda_custom_config_t);
		case TESTPARAM_OPTIM_STATUS: return (int32_t)sizeof(nimroda_optim_status_t);
		case TESTPARAM_POINT: return (int32_t)sizeof(nimroda_point_t);
		case TESTPARAM_VARBLE_ATTRIB: return (int32_t)sizeof(nimroda_variable_attributes_t);
		case TESTPARAM_BATCH: return (int32_t)sizeof(nimroda_batch_t);
		default: return -1;
	}
}