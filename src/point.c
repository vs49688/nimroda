/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "nimroda/nimroda_ip.h"

NIMRODA_INTERNAL_API nimroda_point_t *nimroda_point_alloc(nimroda_instance_t *instance)
{
	nimroda_point_t *point = (nimroda_point_t*)malloc(sizeof(nimroda_point_t));
	if(!point)
		return NULL;

	if(!(point = nimroda_point_alloc_inplace(instance, point)))
		free(point);

	return point;
}

NIMRODA_INTERNAL_API nimroda_point_t *nimroda_point_alloc_manual(size_t num_dims, size_t num_objectives)
{
	nimroda_point_t *point = (nimroda_point_t*)malloc(sizeof(nimroda_point_t));
	if(!point)
		return NULL;

	if(!(point = nimroda_point_alloc_inplace_manual(num_dims, num_objectives, point)))
		free(point);

	return point;
}

NIMRODA_INTERNAL_API nimroda_point_t *nimroda_point_alloc_inplace_manual(size_t num_dims, size_t num_objectives, nimroda_point_t *point)
{
	point->num_dims = num_dims;
	point->status = JOB_NOT_SENT;
	point->num_objectives = num_objectives;

	if(!(point->coord = (double*)malloc(sizeof(double)*num_dims)))
		return NULL;

	if(!(point->objectives = (double*)malloc(sizeof(double)*num_objectives)))
	{
		free(point->coord);
		return NULL;
	}

	for(size_t i = 0; i < num_dims; ++i)
		point->coord[i] = NAN;

	for(size_t i = 0; i < num_objectives; ++i)
		point->objectives[i] = NAN;

	return point;
}

NIMRODA_INTERNAL_API nimroda_point_t *nimroda_point_alloc_inplace(nimroda_instance_t *instance, nimroda_point_t *point)
{
	if(!nimroda_point_alloc_inplace_manual(instance->num_dims, instance->num_objectives, point))
		return NULL;
	point->__owner = instance;
	return point;
}

NIMRODA_INTERNAL_API nimroda_point_t *nimroda_point_copy(nimroda_instance_t *instance, const nimroda_point_t * const src)
{
	nimroda_point_t *point = (nimroda_point_t*)malloc(sizeof(nimroda_point_t));
	if(!point)
		return NULL;

	memset(point, 0, sizeof(nimroda_point_t));

	nimroda_point_t *pt = nimroda_point_copy_inplace(instance, point, src);
	if(!pt)
		free(pt);

	return pt;
}

NIMRODA_INTERNAL_API nimroda_point_t *nimroda_point_copy_inplace(nimroda_instance_t *instance, nimroda_point_t *dst, const nimroda_point_t * const src)
{
	if(src == dst)
		return dst;

	/* If different owners, ensure we don't try to re-use memory. */
	if(instance != dst->__owner && dst->__owner)
	{
		if(dst->coord)
			free(dst->coord);

		dst->coord = NULL;

		if(dst->objectives)
			free(dst->objectives);

		dst->objectives = NULL;
	}

	/* See if we can resuse the coordinate storage. */
	if(dst->num_dims < src->num_dims && dst->coord)
	{
		free(dst->coord);
		dst->coord = NULL;
	}

	/* See if we can reuse the objective storage. */
	if(dst->num_objectives < src->num_objectives && dst->objectives)
	{
		free(dst->objectives);
		dst->objectives = NULL;
	}

	dst->num_dims = src->num_dims;

	/* Allocate new coordinate storage if required. */
	if(!dst->coord && !(dst->coord = (double*)malloc(dst->num_dims * sizeof(double))))
		return NULL;

	dst->num_objectives = src->num_objectives;

	/* Allocate new objective storage if required. */
	if(!dst->objectives && !(dst->objectives = (double*)malloc(dst->num_objectives * sizeof(double))))
	{
		free(dst->coord);
		return NULL;
	}


	memcpy(dst->coord, src->coord, dst->num_dims * sizeof(double));
	memcpy(dst->objectives, src->objectives, dst->num_objectives * sizeof(double));

	dst->status = src->status;
	dst->__owner = instance;

	return dst;
}

NIMRODA_INTERNAL_API void nimroda_point_release(nimroda_point_t *point)
{
	nimroda_point_release_inplace(point);
	free(point);
}

NIMRODA_INTERNAL_API void nimroda_point_release_inplace(nimroda_point_t *point)
{
	free(point->coord);
	free(point->objectives);
}

//void utils2FormatDoubleArray(nimroda_instance_t *state, double *doubles, uint32_t count)
//{
//	nimroda_printf(state, "(%lg", doubles[0]);
//
//	for(uint32_t i = 1; i < count; ++i)
//		nimroda_printf(state, ", %lg", doubles[i]);
//
//	nimroda_printf(state, ")");
//}
