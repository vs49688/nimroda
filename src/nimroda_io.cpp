/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <cassert>
#include <cstring>
#include "nimroda/cxx/Nimrod.hpp"
#include "nimroda/cxx/CIOContext.hpp"

using namespace NimrodA;

static void nimroda_write_var_attribs(CIOContext& ctx, size_t numDims, nimroda_variable_attributes_t& vars)
{
	{
		CIOContext t(ctx, true, "lower_bound");
		for(size_t i = 0; i < numDims; ++i)
			t.write_float64(vars.lower_bound[i]);
	}

	{
		CIOContext t(ctx, true, "upper_bound");
		for(size_t i = 0; i < numDims; ++i)
			t.write_float64(vars.upper_bound[i]);
	}

	{
		CIOContext t(ctx, true, "range");
		for(size_t i = 0; i < numDims; ++i)
			t.write_float64(vars.range[i]);
	}

	{
		CIOContext t(ctx, true, "name");
		for(size_t i = 0; i < numDims; ++i)
			t.write_string(vars.name[i]);
	}
}

static void nimroda_read_var_attribs(CIOContext& ctx, size_t numDims, nimroda_variable_attributes_t& vars)
{
	assert(numDims < MAX_DIMS);

	{
		CIOContext t(ctx, true, "lower_bound");
		for(size_t i = 0; i < numDims; ++i)
			vars.lower_bound[i] = t.read_float64();
	}

	{
		CIOContext t(ctx, true, "upper_bound");
		for(size_t i = 0; i < numDims; ++i)
			vars.upper_bound[i] = t.read_float64();
	}

	{
		CIOContext t(ctx, true, "range");
		for(size_t i = 0; i < numDims; ++i)
			vars.range[i] = t.read_float64();
	}

	{
		CIOContext t(ctx, true, "name");
		for(size_t i = 0; i < numDims; ++i)
		{
			std::string s = t.read_string();
			if(s.size() >= MAX_NAME_LENGTH)
				throw CIOContext::Exception("Variable name too long");

			strncpy(vars.name[i], s.c_str(), MAX_NAME_LENGTH);
			vars.name[i][MAX_NAME_LENGTH - 1] = '\0';
		}
	}
}


static void nimroda_write_optim(CIOContext& ctx, nimroda_optim_data_t& optim)
{
	ctx.write_int32((int32_t)optim.state, "state");

	{
		CIOContext t(ctx, false, "status");
		t.write_int32(optim.status.code, "code");
		t.write_int32((int32_t)optim.status.state, "state");
		t.write_string(optim.status.message, "message");
		t.write_uint64((uint64_t)optim.status.result_count, "result_count");

		{
			CIOContext rctx(t, true, "results");
			for(size_t i = 0; i < optim.status.result_count; ++i)
				CIOContext(rctx, false, nullptr).write_point(optim.status.results[i]);
		}
	}

	{
		nimroda_batch_t& batch = optim.current_batch;
		CIOContext t(ctx, false, "batch");
		t.write_uint64((uint64_t)batch.num_points, "num_points");

		{
			CIOContext pctx(t, true, "points");
			for(size_t i = 0; i < batch.num_points; ++i)
				CIOContext(pctx, false, nullptr).write_point(batch.points[i]);
		}
	}

	ctx.write_uint64((uint64_t)optim.num_evaluations, "num_evaluations");
}

static void nimroda_read_optim(CIOContext& ctx, nimroda_optim_data_t& optim)
{
	optim.state = (nimroda_optim_state_t)ctx.read_int32("state");

	std::vector<ScopedPoint> resultVec;
	std::unique_ptr<nimroda_point_t*, decltype(std::free)*> results(nullptr, std::free);
	{
		CIOContext t(ctx, false, "status");
		optim.status.code = t.read_int32("code");
		optim.status.state = (nimroda_optim_state_t)t.read_int32("state");

		std::string s = t.read_string("message");
		if(s.size() >= LONGEST_ERROR_STRING)
			throw std::runtime_error("Optim message too long");
		strncpy(optim.status.message, s.c_str(), LONGEST_ERROR_STRING);
		optim.status.message[LONGEST_ERROR_STRING - 1] = '\0';

		optim.status.result_count = (size_t)t.read_uint64("result_count");
		resultVec.reserve(optim.status.result_count);
		{
			CIOContext rctx(t, true, "results");
			for(size_t i = 0; i < optim.status.result_count; ++i)
				resultVec.push_back(CIOContext(rctx, false, nullptr).read_point());
		}

		if(optim.status.result_count > 0)
		{
			results.reset((nimroda_point_t**)std::malloc(sizeof(nimroda_point_t*) * optim.status.result_count));
			if(!results)
				throw std::bad_alloc();
			optim.status.results = results.get();

			for(size_t i = 0; i < optim.status.result_count; ++i)
				optim.status.results[i] = resultVec[i].get();
		}
		else
		{
			optim.status.results = nullptr;
		}

	}

	std::vector<ScopedPoint> batchResultVec;
	std::unique_ptr<nimroda_point_t*, decltype(std::free)*> batchPoints(nullptr, std::free);
	{
		nimroda_batch_t& batch = optim.current_batch;
		CIOContext t(ctx, false, "batch");
		batch.num_points = (size_t)t.read_uint64("num_points");
		batchResultVec.reserve(batch.num_points);

		{
			CIOContext pctx(t, true, "points");
			for(size_t i = 0; i < batch.num_points; ++i)
				batchResultVec.push_back(CIOContext(pctx, false, nullptr).read_point());
		}

		if(batch.num_points > 0)
		{
			batchPoints.reset((nimroda_point_t**)std::malloc(sizeof(nimroda_point_t*) * batch.num_points));
			if(!batchPoints)
				throw std::bad_alloc();

			batch.points = batchPoints.get();

			for(size_t i = 0; i < batch.num_points; ++i)
				batch.points[i] = batchResultVec[i].get();
		}
		else
		{
			batch.points = nullptr;
		}
	}

	/* All good, relinquish control of the memory. It's up to the user now. */
	results.release();
	for(size_t i = 0; i < resultVec.size(); ++i)
		resultVec[i].release();

	batchPoints.release();
	for(size_t i = 0; i < batchResultVec.size(); ++i)
		batchResultVec[i].release();
}

static void nimroda_write_stringmap(CIOContext& ctx, const ConfigMap& map)
{
	ctx.write_uint64((uint64_t)map.size(), "length");

	CIOContext l(ctx, true, "entries");
	for(const auto& kv : map)
	{
		CIOContext t(l, true, nullptr);
		t.write_string(kv.first.c_str());
		t.write_string(kv.second.c_str());
	}
}

/**
* @brief Read a string map.
*
* @param[in] ctx The C/IO context.
* @param[in] map The map to read the entries to. This is cleared before use.
*
* @returns An array of nimroda_custom_config_t structures that can be used in nimroda_config_t.
* This array references the storage in @p map.
*/
static std::unique_ptr<nimroda_custom_config_t[]> nimroda_read_stringmap(CIOContext& ctx, NimrodA::ConfigMap& map)
{
	map.clear();

	size_t length = (size_t)ctx.read_uint64("length");

	CIOContext l(ctx, true, "entries");

	for(size_t i = 0; i < length; ++i)
	{
		CIOContext t(l, true, nullptr);
		std::string key = t.read_string();
		std::string val = t.read_string();
		map.emplace(std::make_pair(key, val));
	}

	return nimroda_create_backed_custom_config(map);
}

extern "C" NIMRODA_API int nimroda_save(nimroda_instance_t *instance, cio_context_t *ctx)
{
	assert(instance != nullptr);
	assert(ctx != nullptr);

	NimrodA::Nimrod *nim = reinterpret_cast<NimrodA::Nimrod*>(instance->__cpp);

	try
	{
		CIOContext cctx(ctx);

		cctx.write_uint32(instance->config.rng_seed, "rng_seed");
		cctx.write_uint32(instance->config.debug, "debug");

		size_t uniqLen = strlen(instance->algorithm->unique_name);
		cctx.write_string(instance->algorithm->unique_name, "unique_name");
		cctx.write_uint64((uint64_t)instance->num_dims, "num_dims");
		cctx.write_uint64((uint64_t)instance->num_objectives, "num_objectives");
		cctx.write_uint64((uint64_t)instance->__rng_roll, "rng_roll");

		{
			CIOContext t(ctx, false, "custom_config");
			nimroda_write_stringmap(t, *reinterpret_cast<ConfigMap*>(instance->__cconfig));
		}

		{
			CIOContext t(ctx, false, "variables");
			nimroda_write_var_attribs(t, instance->num_dims, instance->var_attribs);
		}

		{
			CIOContext t(ctx, false, "starting_point");
			t.write_point(&instance->starting_point);
		}

		{
			CIOContext t(ctx, false, "optim");
			nimroda_write_optim(t, instance->optim);
		}
		{
			CIOContext t(ctx, false, "point_db");
			nim->pointDb.save(t);
		}
		{
			CIOContext t(ctx, false, "cxx_backend");
			nim->save(t);
		}
		{
			CIOContext t(ctx, false, "data");
			if(instance->algorithm->save(instance, t) < 0)
				throw CIOContext::Exception();
		}

	}
	catch(std::exception& e)
	{
		nimroda_printf(instance, "Caught exception during save: %s\n", e.what());
		return -1;
	}
	catch(...)
	{
		nimroda_printf(instance, "Caught unknown exception during save.\n");
		return -1;
	}
	return 0;
}

extern "C" NIMRODA_API nimroda_instance_t *nimroda_restore(nimroda_callbacks_t *callbacks, cio_context_t *ctx)
{
	if(!nimroda_validate_callbacks(callbacks))
		return nullptr;

	nimroda_config_t cfg;
	memset(&cfg, 0, sizeof(cfg));

	nimroda_point_t startingPoint;
	memset(&startingPoint, 0, sizeof(startingPoint));

	nimroda_instance_t *nimrod = nullptr;
	try
	{
		CIOContext cctx(ctx);
		cfg.callbacks = *callbacks;
		cfg.rng_seed = cctx.read_uint32("rng_seed");
		cfg.debug = cctx.read_uint32("debug");

		/* It's fine if this is NULL, nimroda_init() will handle it. */
		cfg.algorithm = nimroda_lookup_algorithm(cctx.read_string("unique_name").c_str());

		cfg.num_dimensions = (uint32_t)cctx.read_uint64("num_dims");
		cfg.num_objectives = (uint32_t)cctx.read_uint64("num_objectives");
		size_t rngRoll = (size_t)cctx.read_uint64("rng_roll");

		/* Read the custom configuration */
		NimrodA::ConfigMap customConfigMap;
		std::unique_ptr<nimroda_custom_config_t[]> customConfig;
		{
			CIOContext t(ctx, false, "custom_config");
			customConfig = nimroda_read_stringmap(t, customConfigMap);
		}
		cfg.custom_config = customConfig.get();

		/* Read the variables. */
		nimroda_variable_attributes_t vars;
		{
			CIOContext t(ctx, false, "variables");
			nimroda_read_var_attribs(t, cfg.num_dimensions, vars);
		}
		cfg.vars = &vars;

		{
			CIOContext t(ctx, false, "starting_point");
			t.read_point(&startingPoint);
		}
		cfg.starting_point = &startingPoint;

		ScopedPoint startCoords(&startingPoint, nimroda_point_release_inplace);
		ScopedNimrod nimrod(nimroda_init(&cfg), nimroda_release);

		if(!nimrod)
			return nullptr;

		/* Here, every field in nimroda_instance_t is initialised except "optim". */

		/* Restore the optimisation state data. */
		{
			CIOContext t(ctx, false, "optim");
			nimroda_read_optim(t, nimrod->optim);
		}

		NimrodA::Nimrod *nim = reinterpret_cast<NimrodA::Nimrod*>(nimrod->__cpp);
		/* TODO: Restore the C++ data. */
		std::vector<Point::Ref> pointDb;
		{
			CIOContext t(ctx, false, "point_db");
			pointDb = nim->pointDb.restore(t);
		}
		{
			CIOContext t(ctx, false, "cxx_backend");
			nim->restore(t);
		}

		/* Roll the RNG */
		for(size_t i = 0; i < rngRoll; ++i)
			nimroda_random_uint(nimrod.get(), 0, UINT32_MAX);

		assert(nimrod->__rng_roll == rngRoll);

		/* Restore any algorithm-specific data.
		 * This should get references to the points in pointDb, so it should
		 * be safe to release the vector. */
		{
			CIOContext t(ctx, false, "data");
			if(nimrod->algorithm->restore(nimrod.get(), t) < 0)
				throw CIOContext::Exception();
		}

		return nimrod.release();

	}
	catch(std::exception& e)
	{
		callbacks->puts(nullptr, "Caught exception during restore: ");
		callbacks->puts(nullptr, e.what());
		callbacks->puts(nullptr, "\n");
	}
	catch(...)
	{
		callbacks->puts(nullptr, "Caught unknown exception during restore.\n");
	}

	if(nimrod != nullptr)
		nimroda_release(nimrod);

	return nullptr;
}
