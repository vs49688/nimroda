/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <cstring>
#include "nimroda/algorithms.hpp"
#include "nimroda/mots2/TabuSearch.hpp"

static nimroda_algorithm_t g_sAlgorithms[] = {
	g_MOTS2Definition,
	//{"ALWAYS0",		"Always 0",							1,	&always0Init,		&always0Optimise,			&always0DeInit},
};

constexpr size_t NUM_ALGORITHMS = (sizeof(g_sAlgorithms) / sizeof(nimroda_algorithm_t));

extern "C" NIMRODA_API const nimroda_algorithm_t * const nimroda_query_algorithms(size_t *num)
{
	*num = NUM_ALGORITHMS;
	return g_sAlgorithms;
}

extern "C" NIMRODA_API const nimroda_algorithm_t *nimroda_lookup_algorithm(const char *name)
{
	if(name == nullptr)
		return nullptr;

	size_t num;
	const nimroda_algorithm_t *algos = nimroda_query_algorithms(&num);

	const nimroda_algorithm_t *algo = nullptr;
	for(size_t i = 0; i < num; ++i) if(strcmp(algos[i].unique_name, name) == 0)
	{
		algo = &algos[i];
		break;
	}

	return algo;
}