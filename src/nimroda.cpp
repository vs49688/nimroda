/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <cstdlib>
#include <cstring>
#include <cstdarg>
#include <cassert>
#include <exception>
#include <memory>
#include <random>
#include "nimroda/nimroda_ip.h"

#include "nimroda/algorithms.hpp"
#include "nimroda/cxx/Nimrod.hpp"
#include "nimroda/cxx/CIOContext.hpp"

using CIOContext = NimrodA::CIOContext;

/*
Debug Levels:
0 - None
1 - Error
2 - Warn
3 - Info
4 - MoreInfo
5 - Debug
6 - MoreDebug
7 - Trace. Max Verbosity, dispatch + return of every point, etc. This will make your program SLOW.
*/

NIMRODA_INTERNAL_API void NimrodA::nimroda_map_custom_config(NimrodA::ConfigMap& map, const nimroda_custom_config_t *config)
{
	map.clear();
	for(const nimroda_custom_config_t *cfg = config; cfg->key; ++cfg)
		map.emplace(std::make_pair(cfg->key, cfg->value));
}

NIMRODA_INTERNAL_API std::unique_ptr<nimroda_custom_config_t[]> NimrodA::nimroda_create_backed_custom_config(const NimrodA::ConfigMap& map)
{
	std::unique_ptr<nimroda_custom_config_t[]> cfg(new nimroda_custom_config_t[map.size() + 1]);

	size_t i = 0;
	for(const auto& it : map)
	{
		cfg[i].key = it.first.c_str();
		cfg[i].value = it.second.c_str();
		++i;
	}

	cfg[i].key = nullptr;
	cfg[i].value = nullptr;

	return cfg;
}

extern "C" NIMRODA_INTERNAL_API bool nimroda_validate_callbacks(const nimroda_callbacks_t * const callbacks)
{
	if(!callbacks)
		return false;

	/* This is essential, we can't even report errors without it. */
	if(!callbacks->puts)
		return false;

	if(!callbacks->write_stats)
	{
		callbacks->puts(nullptr, "nimroda_init(): Invalid stats callback supplied. Cannot continue...\n");
		return false;
	}

	return true;
}

extern "C" NIMRODA_INTERNAL_API bool nimroda_validate_config(const nimroda_config_t * const config)
{
	if(!config)
		return false;

	if(!nimroda_validate_callbacks(&config->callbacks))
		return false;

	if(!config->algorithm)
	{
		config->callbacks.puts(nullptr, "nimroda_init(): No algorithm supplied. Cannot continue...\n");
		return false;
	}

	size_t count;
	const nimroda_algorithm_t *algo = nimroda_query_algorithms(&count);
	if(config->algorithm < algo || config->algorithm >(algo + count - 1))
	{
		config->callbacks.puts(nullptr, "nimroda_init(): Supplied algorithm is not registered. Nice try. Cannot continue...\n");
		return false;
	}

	/* For now, check if we're not trying to use one of the not-implemented algorithms. */
	if(!config->algorithm->init || !config->algorithm->optimise || !config->algorithm->deinit || !config->algorithm->save || !config->algorithm->restore)
	{
		config->callbacks.puts(nullptr, "nimroda_init(): Selected algorithm not fully implemented. Cannot continue...\n");
		config->callbacks.puts(nullptr, "nimroda_init(): If you are seeing this, please contact the developer.\n");
		return false;
	}

	/* Other checks. */
	if(!config->num_dimensions)
	{
		config->callbacks.puts(nullptr, "nimroda_init(): Invalid dimension count supplied. Cannot continue...\n");
		return false;
	}

	if(!config->num_objectives)
	{
		config->callbacks.puts(nullptr, "nimroda_init(): Invalid objective count supplied. Cannot continue...\n");
		return false;
	}

	if(!config->vars)
	{
		config->callbacks.puts(nullptr, "nimroda_init(): Invalid variable definition supplied. Cannot continue...\n");
		return false;
	}

	return true;
}

extern "C" NIMRODA_API nimroda_instance_t *nimroda_init(const nimroda_config_t * const config)
{
	if(!nimroda_validate_config(config))
		return nullptr;

	nimroda_instance_t *instance = nullptr;
	try
	{
		/* Allocate the instance memory */
		instance = new nimroda_instance_t();
		memset(instance, 0, sizeof(nimroda_instance_t));

		instance->puts = config->callbacks.puts;
		instance->write_stats = config->callbacks.write_stats;
		instance->algorithm = config->algorithm;
		instance->num_dims = config->num_dimensions;
		instance->num_objectives = config->num_objectives;
		instance->var_attribs = *config->vars;
		instance->config.debug = config->debug;
		instance->config.rng_seed = config->rng_seed;
		instance->user = config->callbacks.user;
		instance->__rng = new NimrodA::RNGImpl(config->rng_seed);

		/* Load the custom configuration options into a map. */
		NimrodA::ConfigMap *cfgMap = new NimrodA::ConfigMap();
		instance->__cconfig = cfgMap;
		NimrodA::nimroda_map_custom_config(*cfgMap, config->custom_config);

		/* Create our C-friendly version. */
		instance->custom_config = NimrodA::nimroda_create_backed_custom_config(*cfgMap).release();

		/* Copy the starting point. */
		if(!nimroda_point_copy_inplace(instance, &instance->starting_point, config->starting_point))
		{
			config->callbacks.puts(nullptr, "nimroda_init(): Error copying starting point - Memory allocation failed.\n");
			goto init_failed;
		}

		/* Initialise the C++ backend. */
		instance->__cpp = new NimrodA::Nimrod(instance);

		/* Finally, initialise the algorithm. */
		if(instance->algorithm->init(instance, config->custom_config) != instance)
			goto init_failed;
	}
	catch(std::exception& e)
	{
		config->callbacks.puts(nullptr, "nimroda_init(): Caught std::exception(");
		config->callbacks.puts(nullptr, e.what());
		config->callbacks.puts(nullptr, ")");
		goto init_failed;
	}
	catch(...)
	{
		config->callbacks.puts(nullptr, "nimroda_init(): Caught unknown exception.\n");
		goto init_failed;
	}

	return instance;

init_failed:

	if(instance != nullptr)
	{
		if(instance->__cpp != nullptr)
			delete reinterpret_cast<NimrodA::Nimrod*>(instance->__cpp);

		nimroda_point_release_inplace(&instance->starting_point);

		if(instance->custom_config != nullptr)
			delete[] instance->custom_config;

		if(instance->__cconfig != nullptr)
			delete reinterpret_cast<NimrodA::ConfigMap*>(instance->__cconfig);

		if(instance->__rng != nullptr)
			delete reinterpret_cast<NimrodA::RNGImpl*>(instance->__rng);

		delete instance;
	}
	return NULL;
}

NIMRODA_INTERNAL_API void nimroda_release_optim_data(nimroda_optim_data_t *optim)
{
	for(size_t i = 0; i < optim->status.result_count; ++i)
	{
		if(optim->status.results[i] != nullptr)
			nimroda_point_release(optim->status.results[i]);

		optim->status.results[i] = nullptr;
	}

	if(optim->status.results != nullptr)
		free(optim->status.results);

	optim->status.results = nullptr;

	/* Clean up the batch (if any). */
	nimroda_batch_cleanup(&optim->current_batch);
}

extern "C" NIMRODA_API void nimroda_release(nimroda_instance_t *instance)
{
	if(instance == nullptr)
		return;

	try
	{
		instance->algorithm->deinit(instance);
	}
	catch(std::exception& e)
	{
		nimroda_printf(instance, "nimroda_release(): Caught std::exception(%s).\n", e.what());
	}
	catch(...)
	{
		instance->puts(instance, "nimroda_release(): Caught unknown exception.\n");
	}

	delete reinterpret_cast<NimrodA::Nimrod*>(instance->__cpp);
	nimroda_point_release_inplace(&instance->starting_point);
	delete[] instance->custom_config;
	delete reinterpret_cast<NimrodA::ConfigMap*>(instance->__cconfig);
	delete reinterpret_cast<NimrodA::RNGImpl*>(instance->__rng);

	/* Here, any CXX points should have already been freed. */
	nimroda_release_optim_data(&instance->optim);
	delete instance;
}

static const char *stateToString(int state)
{
	switch(state)
	{
		case STATE_STOPPED: return "STATE_STOPPED";
		case STATE_WAITING_FOR_BATCH: return "STATE_WAITING_FOR_BATCH";
		case STATE_REFIRE: return "STATE_REFIRE";
		case STATE_FINISHED: return "STATE_FINISHED";
	}

	return "Unknown";
}

extern "C" NIMRODA_API nimroda_optim_state_t nimroda_optimise(nimroda_instance_t *instance)
{
	assert(instance != nullptr);

	/* If we're finished, just return. */
	if(instance->optim.state == STATE_FINISHED)
		return STATE_FINISHED;

	/* If we're waiting for a batch, check that batch is actually done. */
	if(instance->optim.state == STATE_WAITING_FOR_BATCH)
	{
		nimroda_batch_t *batch = &instance->optim.current_batch;

		/* Any points marked with JOB_NOT_SENT count as not being finished. */
		for(size_t i = 0; i < batch->num_points; ++i) if(batch->points[i]->status == JOB_NOT_SENT)
			return STATE_WAITING_FOR_BATCH;

		/* Track the number of evals. */
		instance->optim.num_evaluations += batch->num_points;
	}

	try
	{
		for(;;)
		{
			if(instance->config.debug >= 7)
				nimroda_printf(instance, "nimroda_optimise(): Entering algorithm with state %s\n", stateToString(instance->optim.state));

			/* Do optimisation here */
			instance->algorithm->optimise(instance);

			if(instance->config.debug >= 7)
				nimroda_printf(instance, "nimroda_optimise(): Algorithm exitied with state %s\n", stateToString(instance->optim.state));

			if(instance->optim.state == STATE_WAITING_FOR_BATCH)
			{
				break;
			}
			else if(instance->optim.state == STATE_STOPPED)
			{
				throw std::runtime_error("Invalid post-optimise state");
			}
			else if(instance->optim.state == STATE_REFIRE)
			{
				continue;
			}
			else if(instance->optim.state == STATE_FINISHED)
			{
				break;
			}
		}
	}
	catch(std::exception& e)
	{
		instance->optim.state = STATE_FINISHED;

		instance->optim.status.code = -1;
		strncpy(instance->optim.status.message, e.what(), LONGEST_ERROR_STRING - 1);
		instance->optim.status.message[LONGEST_ERROR_STRING - 1] = '\0';

		instance->optim.status.result_count = 0;
		instance->optim.status.results = nullptr;

		nimroda_printf(instance, "nimroda_optimise(): Caught std::exception(%s), Aborting optimisation...\n", e.what());
	}
	catch(...)
	{
		instance->optim.state = STATE_FINISHED;

		instance->optim.status.code = -1;
		strncpy(instance->optim.status.message, "Caught unknown exception", LONGEST_ERROR_STRING - 1);
		instance->optim.status.message[LONGEST_ERROR_STRING - 1] = '\0';

		instance->optim.status.result_count = 0;
		instance->optim.status.results = nullptr;

		nimroda_printf(instance, "nimroda_optimise(): Caught unknown exception, Aborting optimisation...\n");
	}

	return instance->optim.state;
}

extern "C" NIMRODA_API nimroda_optim_status_t *nimroda_get_optim_status(nimroda_instance_t *instance)
{
	assert(instance != nullptr);
	/* Sync the two state values. We really don't want the use screwing with this, so do it here. */
	instance->optim.status.state = instance->optim.state;
	return &instance->optim.status;
}

extern "C" NIMRODA_API nimroda_variable_attributes_t *nimroda_get_vars(nimroda_instance_t *instance)
{
	assert(instance != nullptr);
	return &instance->var_attribs;
}

NIMRODA_API size_t nimroda_get_dimensionality(nimroda_instance_t *instance)
{
	assert(instance != nullptr);
	return instance->num_dims;
}

NIMRODA_API size_t nimroda_get_objective_count(nimroda_instance_t *instance)
{
	assert(instance != nullptr);
	return instance->num_objectives;
}

extern "C" NIMRODA_API nimroda_batch_t *nimroda_get_batch(nimroda_instance_t *instance)
{
	assert(instance != nullptr);
	return instance->optim.state == STATE_WAITING_FOR_BATCH ? &instance->optim.current_batch : nullptr;
}

extern "C" NIMRODA_API void *nimroda_get_user_pointer(nimroda_instance_t *instance)
{
	assert(instance != nullptr);
	return instance->user;
}

extern "C" NIMRODA_INTERNAL_API nimroda_optim_state_t nimroda_get_optim_state(nimroda_instance_t *instance)
{
	assert(instance != nullptr);
	return instance->optim.state;
}

extern "C" NIMRODA_INTERNAL_API double nimroda_random_double(nimroda_instance_t *instance, double min, double max)
{
	assert(instance != nullptr);
	NimrodA::RNGImpl& rng = *reinterpret_cast<NimrodA::RNGImpl*>(instance->__rng);
	++instance->__rng_roll;
	//double val = std::uniform_real_distribution<double>(min, max)(rng);
	return (double)(min + (rng() / ((double)rng.max())) * (max - min));
}

extern "C" NIMRODA_INTERNAL_API uint32_t nimroda_random_uint(nimroda_instance_t *instance, uint32_t min, uint32_t max)
{
	assert(instance != nullptr);
	NimrodA::RNGImpl& rng = *reinterpret_cast<NimrodA::RNGImpl*>(instance->__rng);
	++instance->__rng_roll;
	/* Until we can get a deterministic implementation of this in the C++ standard,
	* use the non-uniform % method. */
	uint32_t val;// = std::uniform_int_distribution<uint32_t>(min, max)(rng);
	{
		uint32_t newMax = max != rng.max() ? max + 1 : max;
		val = (rng() % (newMax - min)) + min;
	}

	return val;
}

extern "C" NIMRODA_INTERNAL_API char *nimroda_sprintf(nimroda_instance_t *instance, const char *fmt, ...)
{
	assert(instance != nullptr);
	va_list ap;
	va_start(ap, fmt);
	char *s = nimroda_vsprintf(instance, fmt, ap);
	va_end(ap);
	return s;
}

extern "C" NIMRODA_INTERNAL_API char *nimroda_vsprintf(nimroda_instance_t *instance, const char *fmt, va_list ap)
{
	assert(instance != nullptr);
	va_list _ap;
	va_copy(_ap, ap);
	int formatCount = vsnprintf(NULL, 0, fmt, _ap) + 1;
	va_end(_ap);

	char *s = (char*)malloc(formatCount);
	va_copy(_ap, ap);
	vsnprintf(s, formatCount, fmt, _ap);
	va_end(_ap);

	return s;
}

extern "C" NIMRODA_INTERNAL_API void nimroda_printf(nimroda_instance_t *instance, const char *fmt, ...)
{
	assert(instance != nullptr);
	va_list ap;
	va_start(ap, fmt);
	nimroda_vprintf(instance, fmt, ap);
	va_end(ap);
}

extern "C" NIMRODA_INTERNAL_API void nimroda_vprintf(nimroda_instance_t *instance, const char *fmt, va_list ap)
{
	assert(instance != nullptr);
	va_list _ap;
	va_copy(_ap, ap);
	char *s = nimroda_vsprintf(instance, fmt, _ap);
	va_end(_ap);

	instance->puts(instance, s);
	free(s);
}

#include "fnv/fnv.h"

extern "C" NIMRODA_API uint64_t nimroda_hash(const void *data, size_t size)
{
	return fnv_64a_buf(data, size, FNV1A_64_INIT);
}

extern "C" NIMRODA_API uint64_t nimroda_hash_double(double val)
{
	typedef int32_t fixed_ls; /* signed 15.16 fixed-point */
	fixed_ls fVal = ((fixed_ls)((val)*65536.0f));
	return nimroda_hash(&fVal, sizeof(fVal));
}

extern "C" NIMRODA_API uint64_t nimroda_hash_double_array(const double *p, size_t count)
{
	size_t hash = 7;

	for(size_t i = 0; i < count; ++i)
		hash = 89 * hash + nimroda_hash_double(p[i]);

	return hash;
}

extern "C" NIMRODA_INTERNAL_API void nimroda_batch_cleanup(nimroda_batch_t *batch)
{
	/* Clean up the batch (if any). */
	for(size_t i = 0; i < batch->num_points; ++i)
	{
		if(batch->points[i] != nullptr)
			nimroda_point_release(batch->points[i]);

		batch->points[i] = nullptr;
	}

	if(batch->points != nullptr)
		free(batch->points);

	batch->points = nullptr;
	batch->num_points = 0;
}