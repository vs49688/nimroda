#ifndef CIO_PRIVATE_CIO_IO_H
#define CIO_PRIVATE_CIO_IO_H

#include <cio/cio_config.h>
#include <stdint.h>
#include <stddef.h>

/** @brief The types valid for writing. */
typedef enum cio_type
{
	/** @brief Untyped (void*). */
	CIO_TYPE_NONE = 0,
	/** @brief A signed 8-bit integer (int8_t). */
	CIO_TYPE_INT8 = 1,
	/** @brief A unsigned 8-bit integer (uint8_t). */
	CIO_TYPE_UINT8 = 2,
	/** @brief A signed 16-bit integer (int16_t). */
	CIO_TYPE_INT16 = 3,
	/** @brief A unsigned 16-bit integer (uint16_t). */
	CIO_TYPE_UINT16 = 4,
	/** @brief A signed 32-bit integer (int32_t). */
	CIO_TYPE_INT32 = 5,
	/** @brief A unsigned 32-bit integer (uint32_t). */
	CIO_TYPE_UINT32 = 6,
	/** @brief A signed 64-bit integer (int64_t). */
	CIO_TYPE_INT64 = 7,
	/** @brief A unsigned 64-bit integer (uint64_t). */
	CIO_TYPE_UINT64 = 8,
	/** @brief A IEEE754 32-bit float (float). */
	CIO_TYPE_FLOAT32 = 9,
	/** @brief A IEEE754 64-bit float (double). */
	CIO_TYPE_FLOAT64 = 10,
	/** @brief A NULL-terminated UTF-8 string. */
	CIO_TYPE_STRING = 11
} cio_type_t;

struct cio_context;
typedef struct cio_context cio_context_t;

/**
 * @brief A user-defined read procedure.
 *
 * @param[in] ctx The IO context.
 * @param[out] buffer A pointer to the memory the read data will be written to.
 * @param[in] size_t The number of bytes to read.
 * @param[in] type The id of the data type being read/written. If this equals ::CIO_TYPE_NONE, then
 * this is raw, untyped data. a.k.a. (```void*```) and the @p size field should be used.\n
 * There is a special case if this equals ::CIO_TYPE_STRING.
 * - If ```size``` is 0, then ```buffer``` is a pointer to a ```size_t``` to contain the size (in bytes)
 * of the string, NOT including the NULL-terminator.
 * - If ```size``` is nonzero, then ```buffer``` is a pointer to a buffer of at least ```size + 1``` bytes,
 * which should receive the actual string.
 * @param[in] name The name of the field being written. This is optional, and really only useful for
 * text-based backends.
 *
 * @returns On successful read, this should return 0. Otherwise, a non-zero value should be returned.
 */
typedef int(*CIO_READPROC)(struct cio_context *ctx, void *buffer, size_t size, cio_type_t type, const char *name);

/**
 * @brief A user-defined write procedure.
 *
 * @param[in] ctx The IO context.
 * @param[in] data A pointer to the data to be read/written.
 * @param[in] size If writing, the size of the data in @p buffer.
 * @param[in] type The id of the data type being read/written. If this equals ::CIO_TYPE_NONE, then
 * this is raw, untyped data. a.k.a. (void*) and the @p size field should be used.
 * @param[in] name The name of the field being written. This is optional, and really only useful for
 * text-based backends.
 *
 * @returns On successful write, this should return 0. Otherwise, a non-zero value should be returned.
 */
typedef int(*CIO_WRITEPROC)(struct cio_context *ctx, const void *data, size_t size, cio_type_t type, const char *name);

/**
 * @brief Do a Push/Pop operation on the IO context.
 *
 * @param[in] oldCtx The old context.
 * @param[in] newCtx The new context.
 *
 * @returns If the operation is allowed, this function returns 0. Otherwise, -1.
 */
typedef int(*CIO_PUSHPOPPROC)(struct cio_context *oldCtx, struct cio_context *newCtx);

struct cio_context
{
	CIO_PUSHPOPPROC push;
	CIO_PUSHPOPPROC pop;
	CIO_READPROC read;
	CIO_WRITEPROC write;

	void *user;

	struct cio_context *__parent;
	const char *__name;
	int __listFlag;
};

#include "cio_p.h"

#endif /* CIO_PRIVATE_CIO_IO_H */