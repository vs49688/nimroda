#ifndef CIO_PRIVATE_CIO_IO_IP_H
#define CIO_PRIVATE_CIO_IO_IP_H

#include "cio.h"

#ifdef __cplusplus
extern "C" {
#endif

	/**
	 * @brief Push a new dictionary context. A dictionary context is a list of key-value pairs.
	 * Contexts inherit their configurations from their parent.
	 *
	 * @param[in] ctx The storage for the new context data. May not be NULL.
	 * @param[in] parent The parent context. May not be NULL. If initialising for the first time,
	 * fill in the cio_context_t structure manually.
	 * @param[in] name The name of the context.
	 *
	 * @returns If @p ctx or @p parent are NULL or if @code ctx->push() @endcode returns nonzero,
	 * this function returns NULL. Otherwise, @p ctx is returned.
	 *
	 * @sa cio_push_list
	 * @sa cio_pop
	 */
	CIO_API cio_context_t *cio_push_dict(cio_context_t *ctx, cio_context_t *parent, const char *name);

	/**
	 * @brief Push a new list context. A list context is a list of values.
	 * Contexts inherit their configurations from their parent.
	 *
	 * @param[in] ctx The storage for the new context data. May not be NULL.
	 * @param[in] parent The parent context. May not be NULL. If initialising for the first time,
	 * fill in the cio_context_t structure manually.
	 * @param[in] name The name of the context.
	 *
	 * @returns If @p ctx or @p parent are NULL or if @code ctx->push() @endcode returns nonzero,
	 * this function returns NULL. Otherwise, @p ctx is returned.
	 *
	 * @sa cio_push_dict
	 * @sa cio_pop
	 */
	CIO_API cio_context_t *cio_push_list(cio_context_t *ctx, cio_context_t *parent, const char *name);

	/**
	 * @brief Pop a context, ending its life.
	 *
	 * @param[in] ctx The context to pop. May not be NULL.
	 *
	 * @returns If @p ctx is NULL or if @code ctx->pop() @endcode returns nonzero, this function
	 * returns NULL. Otherwise, the parent context is returned. THe buffer pointed to by @ctx may
	 * safely reused.
	 */
	CIO_API cio_context_t *cio_pop(cio_context_t *ctx);

	/**
	 * @brief Write the given data.
	 *
	 * @param[in] ctx The IO context.
	 * @param[in] buffer The data to write.
	 * @param[in] size The size of the data pointed to by @p buffer.
	 * @param[in] type The type of the value. If this equals ::CIO_TYPE_NONE, then the @p size field
	 * contains the size of the @p buffer. If this is any of the other values, then the @p size field will
	 * match the value returned by cio_get_type_size().
	 * @param[in] name The name of the field. This may be used by text-based backends to identifiy the field 
	 * being written. This field may be ignored. If @p ctx represents a list, then this will be NULL.
	 *
	 * @returns If the write was successful, 0 is returned. Otherwise, a nonzero value is returned.
	 */
	CIO_API int cio_write(cio_context_t *ctx, const void *buffer, size_t size, cio_type_t type, const char *name);

	/**
	 * @brief Read data.
	 *
	 * @param[in] ctx The IO context.
	 * @param[out] buffer A pointer to the buffer to read the data into.
	 * @param[in] size The number of bytes to read. This must be less than or equal to the size of @p buffer.
	 * @param[in] type The type of the value. If this equals ::CIO_TYPE_NONE, then the @p size field
	 * contains the size of the @p buffer. If this is any of the other values, then the @p size field will
	 * match the value returned by cio_get_type_size().
	 * @param[in] name The name of the field. This may be used by text-based backends to identifiy the field 
	 * being read. This field may be ignored.
	 *
	 * @returns If the read was successful, 0 is returned. Otherwise, a nonzero value is returned.
	 */
	CIO_API int cio_read(cio_context_t *ctx, void *buffer, size_t size, cio_type_t type, const char *name);

	/**
	 * @brief Write a null-terminated, UTF-8 string.
	 *
	 * @param[in] ctx The IO context.
	 * @param[in] s A pointer to the string to write. May not be NULL.
	 * @param[in] name The name of the field. This may be used by text-based backends to identifiy the field 
	 * being written.
	 *
	 * @returns If the write was successful, 0 is returned. Otherwise, a nonzero value is returned.
	 */
	CIO_API int cio_write_string(cio_context_t *ctx, const char *s, const char *name);

	/**
	 * @brief Read a UTF-8 string.
	 *
	 * @param[in] ctx The IO context.
	 * @param[in] buffer A pointer to the buffer to store the string. If NULL, the length of the string will be
	 * placed at the address pointed to by @p buffer_length.
	 * @param[inout] buffer_length A poitner to the size of @p buffer. If @p buffer is NULL, the length of the string
	 * will be written.
	 * @param[in] name The name of the field. This may be used by text-based backends to identifiy the field 
	 * being read. This field may be ignored.
	 *
	 * @returns If the read was successful, 0 is returned. Otherwise, a nonzero value is returned.
	 */
	CIO_API int cio_read_string(cio_context_t *ctx, char *buffer, size_t *buffer_length, const char *name);

	/**
	 * @brief Get the size of the given type.
	 *
	 * @param[in] The type id.
	 *
	 * @returns The size of the given type. If @p type == ::CIO_TYPE_NONE, 0 is returned.
	 */
	CIO_API size_t cio_get_type_size(cio_type_t type);
	CIO_API int cio_write_type(cio_context_t *ctx, const void *val, cio_type_t type, const char *name);
	CIO_API int cio_read_type(cio_context_t *ctx, void *buffer, cio_type_t type, const char *name);

	CIO_API int cio_null_read(cio_context_t *ctx, void *buffer, size_t size, cio_type_t type, const char *name);
	CIO_API int cio_null_write(cio_context_t *ctx, const void *buffer, size_t size, cio_type_t type, const char *name);

#define CIO_IO_DECLARE_PRIMITIVE_WRITER(fnname, type, enumType)										\
	CIO_API static inline int cio_write_##fnname(cio_context_t *ctx, type val, const char *name)	\
	{																								\
		return cio_write_type(ctx, &val, enumType, name);											\
	}

#define CIO_IO_DECLARE_PRIMITIVE_READER(fnname, type, enumType)										\
	CIO_API static inline type cio_read_##fnname(cio_context_t *ctx, int *status, const char *name)	\
	{																								\
		type val;																					\
		*status = cio_read_type(ctx, &val, enumType, name);											\
		return val;																					\
	}

#define CIO_IO_DECLARE_PRIMITIVE_IO(name, type, enumType)	\
	CIO_IO_DECLARE_PRIMITIVE_WRITER(name, type, enumType)	\
	CIO_IO_DECLARE_PRIMITIVE_READER(name, type, enumType)

	CIO_IO_DECLARE_PRIMITIVE_IO(int8, int8_t, CIO_TYPE_INT8);
	CIO_IO_DECLARE_PRIMITIVE_IO(uint8, uint8_t, CIO_TYPE_UINT8);
	CIO_IO_DECLARE_PRIMITIVE_IO(int16, int16_t, CIO_TYPE_INT16);
	CIO_IO_DECLARE_PRIMITIVE_IO(uint16, uint16_t, CIO_TYPE_UINT16);
	CIO_IO_DECLARE_PRIMITIVE_IO(int32, int32_t, CIO_TYPE_INT32);
	CIO_IO_DECLARE_PRIMITIVE_IO(uint32, uint32_t, CIO_TYPE_UINT32);
	CIO_IO_DECLARE_PRIMITIVE_IO(int64, int64_t, CIO_TYPE_INT64);
	CIO_IO_DECLARE_PRIMITIVE_IO(uint64, uint64_t, CIO_TYPE_UINT64);
	CIO_IO_DECLARE_PRIMITIVE_IO(float32, float, CIO_TYPE_FLOAT32);
	CIO_IO_DECLARE_PRIMITIVE_IO(float64, double, CIO_TYPE_FLOAT64);

#ifdef __cplusplus
}
#endif
#endif /* CIO_PRIVATE_CIO_IO_IP_H */
