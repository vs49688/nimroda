### C/IO
A lightweight C typed-I/O infrastructure.

### License
WTFPL - See ```COPYING``` or [http://www.wtfpl.net](http://www.wtfpl.net) for more information.