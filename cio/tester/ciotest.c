#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <inttypes.h>
#include <cio/cio.h>

typedef struct user_text
{
	FILE *text_out;
	FILE *binary;
	size_t depth;
} user_t;

static void fill_tabs(FILE *f, size_t num)
{
	for(size_t i = 0; i < num; ++i)
		fprintf(f, "\t");
}

static int text_write(cio_context_t *ctx, const void *buffer, size_t size, cio_type_t type, const char *name)
{
	user_t *user = (user_t*)ctx->user;

	fill_tabs(user->text_out, user->depth);
	if(name && !ctx->__listFlag)
		fprintf(user->text_out, "'%s' = ", name);

	switch(type)
	{
		case CIO_TYPE_NONE:
			fprintf(user->text_out, "raw:[");
			for(size_t i = 0; i < size; ++i)
			{
				fprintf(user->text_out, "0x%02x", *((int8_t*)buffer + i));

				if(i != size - 1)
					fprintf(user->text_out, ", ");
			}
			fprintf(user->text_out, "]");
			break;
		case CIO_TYPE_INT8:
			fprintf(user->text_out, "int8:'%d'", *((int8_t*)buffer));
			break;
		case CIO_TYPE_UINT8:
			fprintf(user->text_out, "uint8:'%u'", *((uint8_t*)buffer));
			break;
		case CIO_TYPE_INT16:
			fprintf(user->text_out, "int16:'%hd'", *((int16_t*)buffer));
			break;
		case CIO_TYPE_UINT16:
			fprintf(user->text_out, "uint16:'%hu'", *((uint16_t*)buffer));
			break;
		case CIO_TYPE_INT32:
			fprintf(user->text_out, "int32:'%d'", *((int32_t*)buffer));
			break;
		case CIO_TYPE_UINT32:
			fprintf(user->text_out, "uint32:'%u'", *((uint32_t*)buffer));
			break;
		case CIO_TYPE_INT64:
			fprintf(user->text_out, "int64:'%" PRId64 "'", *((int64_t*)buffer));
			break;
		case CIO_TYPE_UINT64:
			fprintf(user->text_out, "uint64:'%" PRIu64 "'", *((uint64_t*)buffer));
			break;
		case CIO_TYPE_FLOAT32:
			fprintf(user->text_out, "float32:'%f'", *((float*)buffer));
			break;
		case CIO_TYPE_FLOAT64:
			fprintf(user->text_out, "float64:'%lf'", *((double*)buffer));
			break;
		case CIO_TYPE_STRING:
			/* TODO: Escape it */
			fprintf(user->text_out, "string:'%s'", (const char*)buffer);
			break;
	}

	fprintf(user->text_out, "\n");
	return 0;
}

static int text_push(cio_context_t *oldCtx, cio_context_t *newCtx)
{
	user_t *user = (user_t*)oldCtx->user;
	fill_tabs(user->text_out, user->depth);
	fprintf(user->text_out, "'%s' = %c\n", newCtx->__name, newCtx->__listFlag ? '[' : '{');
	++user->depth;
	return 0;
}

static int text_pop(cio_context_t *oldCtx, cio_context_t *newCtx)
{
	user_t *user = (user_t*)oldCtx->user;
	--user->depth;
	fill_tabs(user->text_out, user->depth);
	fprintf(user->text_out, "%c\n", oldCtx->__listFlag ? ']' : '}');
	return 0;
}

static int binary_read(cio_context_t *ctx, void *buffer, size_t size, cio_type_t type, const char *name)
{
	user_t *user = (user_t*)ctx->user;
	return fread(buffer, size, 1, user->binary) != 1;
}

static int binary_write(cio_context_t *ctx, const void *buffer, size_t size, cio_type_t type, const char *name)
{
	user_t *user = (user_t*)ctx->user;
	return fwrite(buffer, size, 1, user->binary) != 1;
}

static int binary_push(cio_context_t *oldCtx, cio_context_t *newCtx)
{
	return 0;
}

static int binary_pop(cio_context_t *oldCtx, cio_context_t *newCtx)
{
	return 0;
}

typedef struct kek
{
	int32_t x;
	uint64_t y;
	double d;

	uint32_t num_floats;
	float *floats;
} kek_t;


static int write_kek(kek_t *kek, cio_context_t *ctx)
{
	if(cio_write_int32(ctx, kek->x, "x"))
		goto write_failed;

	if(cio_write_uint64(ctx, kek->y, "y"))
		goto write_failed;

	if(cio_write_float64(ctx, kek->d, "d"))
		goto write_failed;

	if(cio_write_uint32(ctx, kek->num_floats, "num_floats"))
		goto write_failed;

	cio_context_t tmpCtx;

	if(!(ctx = cio_push_list(&tmpCtx, ctx, "floats")))
		goto write_failed;

	for(size_t i = 0; i < kek->num_floats; ++i)
	{
		if(cio_write_float32(ctx, kek->floats[i], NULL))
			goto write_failed;
	}

	ctx = cio_pop(ctx);

	return 0;
write_failed:
	return -1;
}

static int read_kek(kek_t *kek, cio_context_t *ctx)
{
	int status;

	kek->x = cio_read_int32(ctx, &status, "x");
	kek->y = cio_read_uint64(ctx, &status, "y");
	kek->d = cio_read_float64(ctx, &status, "d");
	kek->num_floats = cio_read_uint32(ctx, &status, "num_floats");


	cio_context_t tmpCtx;

	if(!(ctx = cio_push_list(&tmpCtx, ctx, "floats")))
		goto read_failed;

	kek->floats = (float*)malloc(sizeof(float) * kek->num_floats);

	for(size_t i = 0; i < kek->num_floats; ++i)
		kek->floats[i] = cio_read_float32(ctx, &status, NULL);

	ctx = cio_pop(ctx);

read_failed:
	return -1;
}

static void write_kek_text(kek_t *kek, user_t *user)
{
	cio_context_t ctx;
	memset(&ctx, 0, sizeof(cio_context_t));

	ctx.read = NULL;
	ctx.write = &text_write;
	ctx.push = &text_push;
	ctx.pop = &text_pop;
	ctx.user = user;

	write_kek(kek, &ctx);
}

static void write_kek_binary(kek_t *kek, user_t *user)
{
	cio_context_t ctx;
	memset(&ctx, 0, sizeof(cio_context_t));

	ctx.read = NULL;
	ctx.write = &binary_write;
	ctx.push = &binary_push;
	ctx.pop = &binary_pop;
	ctx.user = user;

	write_kek(kek, &ctx);
}

static void read_kek_binary(kek_t *kek, user_t *user)
{
	cio_context_t ctx;
	memset(&ctx, 0, sizeof(cio_context_t));

	ctx.read = &binary_read;
	ctx.write = NULL;
	ctx.push = &binary_push;
	ctx.pop = &binary_pop;
	ctx.user = user;

	read_kek(kek, &ctx);
}

static float floats[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

int main(int argc, char **argv)
{
	kek_t kek;
	kek.x = 69;
	kek.y = 12345;
	kek.d = 3.141592;
	kek.num_floats = sizeof(floats) / sizeof(float);
	kek.floats = floats;

	user_t user;
	user.depth = 0;
	user.text_out = stdout;
	if(!(user.binary = fopen("kek.bin", "wb")))
	{
		fprintf(stderr, "Unable to open kek.bin for writing\n");
		return 1;
	}


	write_kek_text(&kek, &user);
	write_kek_binary(&kek, &user);

	fclose(user.binary);

	if(!(user.binary = fopen("kek.bin", "rb")))
	{
		fprintf(stderr, "Unable to open kek.bin for reading\n");
		return 1;
	}

	memset(&kek, 0, sizeof(kek));
	read_kek_binary(&kek, &user);
	fclose(user.binary);
	free(kek.floats);

	return 0;
}