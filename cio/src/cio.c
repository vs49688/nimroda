#include <assert.h>
#include <cio/cio_p.h>

static cio_context_t *cio_push(cio_context_t *ctx, cio_context_t *parent, const char *name, int listFlag)
{
	if(!ctx || !parent)
		return NULL;

	ctx->push = parent->push;
	ctx->pop = parent->pop;
	ctx->read = parent->read;
	ctx->write = parent->write;
	ctx->user = parent->user;
	ctx->__name = name;
	ctx->__parent = parent;
	ctx->__listFlag = !!listFlag;

	return ctx->push(parent, ctx) < 0 ? NULL : ctx;
}

CIO_API cio_context_t *cio_push_dict(cio_context_t *ctx, cio_context_t *parent, const char *name)
{
	return cio_push(ctx, parent, name, 0);
}

CIO_API cio_context_t *cio_push_list(cio_context_t *ctx, cio_context_t *parent, const char *name)
{
	return cio_push(ctx, parent, name, 1);
}

CIO_API cio_context_t *cio_pop(cio_context_t *ctx)
{
	if(!ctx)
		return NULL;

	if(ctx->pop && ctx->pop(ctx, ctx->__parent) < 0)
		return NULL;

	return ctx;
}

CIO_API size_t cio_get_type_size(cio_type_t type)
{
	switch(type)
	{
		case CIO_TYPE_INT8: return sizeof(int8_t);
		case CIO_TYPE_UINT8: return sizeof(uint8_t);
		case CIO_TYPE_INT16: return sizeof(int16_t);
		case CIO_TYPE_UINT16: return sizeof(uint16_t);
		case CIO_TYPE_INT32: return sizeof(int32_t);
		case CIO_TYPE_UINT32: return sizeof(uint32_t);
		case CIO_TYPE_INT64: return sizeof(int64_t);
		case CIO_TYPE_UINT64: return sizeof(uint64_t);
		case CIO_TYPE_FLOAT32: return sizeof(float);
		case CIO_TYPE_FLOAT64: return sizeof(double);
		default: return 0;
	}
}

CIO_API int cio_write(cio_context_t *ctx, const void *buffer, size_t size, cio_type_t type, const char *name)
{
	if(ctx->write == NULL)
		return 0;

	/* Ignore the name if we're a list. */
	if(ctx->__listFlag)
		name = NULL;

	return ctx->write(ctx, buffer, size, type, name);
}

CIO_API int cio_read(cio_context_t *ctx, void *buffer, size_t size, cio_type_t type, const char *name)
{
	if(ctx->read == NULL)
		return 0;

	return ctx->read(ctx, buffer, size, type, name);
}

CIO_API int cio_write_string(cio_context_t *ctx, const char *s, const char *name)
{
	return cio_write_type(ctx, s, CIO_TYPE_STRING, name);
}

CIO_API int cio_read_string(cio_context_t *ctx, char *buffer, size_t *buffer_length, const char *name)
{
	/* If buffer is NULL, get the length. */
	if(!buffer)
		return cio_read(ctx, buffer_length, 0, CIO_TYPE_STRING, name);
	/* Otherwise, get the string. */
	else
		return cio_read(ctx, buffer, *buffer_length, CIO_TYPE_STRING, name);
}

CIO_API int cio_write_type(cio_context_t *ctx, const void *val, cio_type_t type, const char *name)
{
	/* For now, just forward directly. */
	return cio_write(ctx, val, cio_get_type_size(type), type, name);
}

CIO_API int cio_read_type(cio_context_t *ctx, void *buffer, cio_type_t type, const char *name)
{
	/* For now, just forward directly. */
	return cio_read(ctx, buffer, cio_get_type_size(type), type, name);
}

CIO_API int cio_null_pushpop(cio_context_t *oldCtx, cio_context_t *newCtx)
{
	return 0;
}

CIO_API int cio_null_read(cio_context_t *ctx, void *buffer, size_t size, cio_type_t type, const char *name)
{
	return 0;
}

CIO_API int cio_null_write(cio_context_t *ctx, const void *buffer, size_t size, cio_type_t type, const char *name)
{
	return 0;
}
