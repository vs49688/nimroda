/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef NIMRODA_PUBLIC_NIMRODA_H
#define NIMRODA_PUBLIC_NIMRODA_H

#include "nimroda/config.h"

#include <time.h>
#include <stdint.h>
#include <stdbool.h>
#include "cio/cio.h"

#ifdef _MSC_VER
#	pragma warning(disable:4996) /* Deprecation */
#endif

 /* Limit on the number of numeric parameters */
#define MAX_DIMS 128		

 /* Length of parameter names */
#define MAX_NAME_LENGTH     30

 /* a big number */
#define HUGEISH 1.0e200

#define LONGEST_ERROR_STRING 200

struct nimroda_instance;
typedef struct nimroda_instance nimroda_instance_t;

typedef struct nimroda_variable_attributes
{
	/** @brief Coordinates of the lower bounds of the search space */
	double lower_bound[MAX_DIMS];

	/** @brief Coordinates of the upper bounds of the search space. */
	double upper_bound[MAX_DIMS];

	/**
	* @brief The 'range' (span of domain) or the search space.
	* This is the same as <code>upper_bound - lower_bound</code>
	*/
	double range[MAX_DIMS];

	/** @brief The variable names. */
	char name[MAX_DIMS][MAX_NAME_LENGTH];
} nimroda_variable_attributes_t;

enum Job_Status
{
	/** @brief Job Successfully completed. */
	JOB_COMPLETE,
	/** @brief Job failed. */
	JOB_FAILED,
	/** @brief The job hasn't been sent/evaluated yet. */
	JOB_NOT_SENT
};

typedef struct nimroda_point
{
	size_t num_dims;				/* dimensionality */
	double *coord;				/* coordinates of the point */
	enum Job_Status status;		/* what happened when this job was evaluated? */
	size_t num_objectives;		/* for multiple objectives */
	double *objectives;			/* for jobs that return more than one objective */

	/** @brief The Nimrod/A instance that owns this point. */
	nimroda_instance_t *__owner;
} nimroda_point_t;

/** @brief Possible states for an optimisation. */
typedef enum nimroda_optim_state
{
	/** @brief The optimisation hasn't started. */
	STATE_STOPPED = 0,
	/** @brief The optimisation has halted, waiting for a batch evaluation to finish. */
	STATE_WAITING_FOR_BATCH = 1,
	/**
	* @brief An internal state used by the implementation. This signifies that the implementation
	* has returned and wants to be fired again without returning to the application.
	*
	* This will never be returned as a result of nimroda_optimise().
	*/
	STATE_REFIRE = 2,
	/** @brief The optimisation has finished successfully. */
	STATE_FINISHED = 3,
} nimroda_optim_state_t;

typedef struct nimroda_optim_status
{
	/**
	 * @brief The status code.
	 *
	 * This may take any value, however there are some values that have special meaning to Nimrod/A.
	 * Algorithm implementations should be aware of these.
	 *
	 * <ul>
	 * 	<li>0 - The optimisation completed successfully.</li>
	 * 	<li>1 - The optimisation failed miserably.</li>
	 * 	<li>2 - The optimisation hasn't finished.</li>
	 * </ul>
	 * Any other values are valid, however their meaning is implementation-defined.
	 */
	int32_t code;

	/** @brief The state of the algorithm. This is the same as the value returned by nimroda_optimise(). */
	nimroda_optim_state_t state;

	/** @brief A description of the current status of the optimisation. */
	char message[LONGEST_ERROR_STRING];

	/** @brief The optimisation results. This is an array of @p result_count points. */
	nimroda_point_t **results;

	/** @brief The number of points in the results. */
	size_t result_count;
} nimroda_optim_status_t;

struct nimroda_batch;
typedef struct nimroda_batch nimroda_batch_t;

/** @brief A key-value pair of strings. */
typedef struct nimroda_kv
{
	/** @brief The key. */
	const char *key;
	/** @brief The value. */
	const char *value;
} nimroda_kv_t;

typedef nimroda_kv_t nimroda_custom_config_t;

/**
 * @brief Receive a stats package from the algorithm.
 *
 * @param[in] instance The Nimrod/A instance. May not be NULL.
 * @param[in] stats A key-value list of the stats.
 * @param[in] count The number of entries in @p stats.
 *
 */
typedef void(*PFNSTATS)(nimroda_instance_t *instance, const nimroda_kv_t *stats, size_t count);

/**
 * @brief An algorithm initialisation function.
 *
 * @param[in] instance The Nimrod/A instance. May not be NULL.
 * @param[in] config A null-terminated list of key-value pairs.
 *
 * @returns If successful, @p instance will be returned. Otherwise, NULL will be returned.
 *
 * @remark By the time this is called, all of fields (except <code>optim</code>) in
 * @p instance have been initialised.
 * @remark This function is expected to initialise the <code>optim</code> field in
 * @p instance.
 */
typedef nimroda_instance_t *(*PFNINIT)(nimroda_instance_t *instance, const nimroda_custom_config_t *config);

/**
 * @brief Perform an optimisation iteration
 *
 * @param[in] instance The Nimrod/A instance. May not be NULL.
 */
typedef void(*PFNOPTIMISE)(nimroda_instance_t *instance);

/**
 * @brief Deinitialise an algorithm, performing any cleanup.
 *
 * @param[in] instance The Nimrod/A instance. May not be NULL.
 */
typedef void(*PFNDEINIT)(nimroda_instance_t *instance);

/**
 * @brief Save/restore the algorithm from the given IO context.
 *
 * @param[in] instance The Nimrod/A instance. May not be NULL.
 * @param[in] ctx C/IO context to use to. May not be NULL.
 */
typedef int(*PFNIOPROC)(nimroda_instance_t *instance, cio_context_t *ctx);

typedef struct nimroda_algorithm
{
	/** @brief The unique identifier for this algorithm. */
	const char *unique_name;
	/** @brief The pretty-printable name for this algorithm. */
	const char *name;
	/** @brief Does this algorithm support multiple objectives? */
	int32_t multi_objective;
	/** @brief The callback used to initialise the algorithm for use. */
	PFNINIT init;
	/** @brief The callback used to start the optimisation. */
	PFNOPTIMISE optimise;
	/** @brief The callback used to release any resources allocated by @p init. */
	PFNDEINIT deinit;
	/** @brief The callback used to save the current algorithm state. */
	PFNIOPROC save;
	/** @brief The callback used to restore the algorithm state. */
	PFNIOPROC restore;
} nimroda_algorithm_t;

typedef void(*PFNPUTS)(nimroda_instance_t *instance, const char *s);

typedef struct nimroda_callbacks
{
	/** @brief The callback used to log messages. May not be NULL. */
	PFNPUTS puts;
	/** @brief The callback used to reports stats to the application. May not be NULL. */
	PFNSTATS write_stats;
	/** @brief A pointer to user-stored data. */
	void *user;
} nimroda_callbacks_t;

typedef struct nimroda_config
{
	/** @brief One of the algorithm structures returned by nimroda_query_algorithms(). May not be NULL. */
	const nimroda_algorithm_t *algorithm;
	/** @brief The variable definitions. May not be NULL. */
	const nimroda_variable_attributes_t *vars;
	/** @brief The number of dimensions for each point. */
	uint32_t num_dimensions;
	/** @brief The number of objectives for each point. */
	uint32_t num_objectives;
	/** @brief The callbacks used for reporting and printing. */
	nimroda_callbacks_t callbacks;
	/** @brief The maximum debug level. */
	uint32_t debug;
	/** @brief The seed for the RNG. */
	uint32_t rng_seed;
	/** @brief An array of custom configuration options. These are passed directly to the selected algorithm. May be NULL. */
	const nimroda_custom_config_t *custom_config;
	/** @brief The starting point. */
	const nimroda_point_t *starting_point;
} nimroda_config_t;

/**
* @brief A batch of points.
*/
struct nimroda_batch
{
	/** @brief The number of points in the batch. */
	size_t num_points;
	/** @brief The points in the batch. This is @p numPoints long. */
	nimroda_point_t **points;
};

#include "nimroda_p.h"

#endif /* NIMRODA_PUBLIC_NIMRODA_H */
