/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef NIMRODA_PUBLIC_NIMRODA_P_H
#define NIMRODA_PUBLIC_NIMRODA_P_H

#include "nimroda.h"

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief Query the available algorithms.
 *
 * @param[out] num A pointer to receive the number of algorithms.
 *
 * @returns A pointer to an array of algorithms <code>*num</code> elements long.
 */
NIMRODA_API const nimroda_algorithm_t * const nimroda_query_algorithms(size_t *num);

/** 
 * @brief Lookup an algorithm by its unique name.
 *
 * @param[in] name The unique name of the algorithm to lookup.
 *
 * @returns If found, a pointer to the algorithm. Otherwise, NULL.
 */
NIMRODA_API const nimroda_algorithm_t *nimroda_lookup_algorithm(const char *name);

/**
 * @brief Allocate a new Nimrod/A instance.
 *
 * @param[in] config The initial algorithm configuration. May not be NULL.
 *
 * @returns A new Nimrod/A instance that may be operated upon. The initial
 * state of the instance is ::STATE_STOPPED.
 */
NIMRODA_API nimroda_instance_t *nimroda_init(const nimroda_config_t * const config);

/**
 * @brief Release any resources and deallocate the given instance.
 *
 * @param[in] instance The Nimrod/A instance. May not be NULL.
 *
 * @remark After calling, any memory allocations done through this instance are
 * not guaranteed to be valid.
 */
NIMRODA_API void nimroda_release(nimroda_instance_t *instance);

/**
 * @brief Tick the state machine.
 *
 * @param[in] instance The Nimrod/A instance. May not be NULL.
 *
 * @returns The current state of the optimisation. This will either be ::STATE_WAITING_FOR_BATCH
 * or ::STATE_FINISHED.
 *
 * If ::STATE_WAITING_FOR_BATCH:
 * <ul>
 * 	<li>The batch of points may be retrieved by a call to nimroda_get_batch().</li>
 * 	<li>One the batch has been retrieved, the application is expected to write the objective
 * 	values in the <code>objectives</code> field of each point and set the
 *	<code>status</code> field to either ::JOB_COMPLETE or ::JOB_FAILED.</li>
 * 	<li>If nimroda_optimise() is called again before evaluation is complete, it will simply return the same
 *	value.
 * 	</li>
 * </ul>
 * If ::STATE_FINISHED:
 * <ul>
 * 	<li>The optimisation status may be retrieved by a call to nimroda_get_optim_status().</li>
 * 	<li>If nimroda_optimise() is called again, it will simply return the same value.</li>
 * </ul>
 */
NIMRODA_API nimroda_optim_state_t nimroda_optimise(nimroda_instance_t *instance);

/**
 * @brief Get the status of the optimisation.
 *
 * @param[in] instance The Nimrod/A instance. May not be NULL.
 *
 * @returns The status of the optimisation.
 */
NIMRODA_API nimroda_optim_status_t *nimroda_get_optim_status(nimroda_instance_t *instance);

/**
 * @brief Get the current variable attributes structure.
 *
 * @param[in] instance The Nimrod/A instance. May not be NULL.
 *
 * @returns The current variable attributes structure.
 */
NIMRODA_API nimroda_variable_attributes_t *nimroda_get_vars(nimroda_instance_t *instance);

/**
 * @brief Get the dimensionality of the problem.
 *
 * @param[in] instance The Nimrod/A instance. May not be NULL.
 *
 * @returns The dimensionality of the problem.
 */
NIMRODA_API size_t nimroda_get_dimensionality(nimroda_instance_t *instance);

/**
 * @brief Get the objective count of the problem.
 *
 * @param[in] instance The Nimrod/A instance. May not be NULL.
 *
 * @returns The objective count of the problem.
 */
NIMRODA_API size_t nimroda_get_objective_count(nimroda_instance_t *instance);

/**
 * @brief Get the next batch of points to be evaluated.
 *
 * @param[in] instance The Nimrod/A instance. May not be NULL.
 *
 * @returns If the optimisation state is ::STATE_WAITING_FOR_BATCH, the next batch of points to be
 * evaluated is returned. Otherwise, NULL.
 *
 * @remark This function will only succeed if the optimisation is in ::STATE_WAITING_FOR_BATCH
 */
NIMRODA_API nimroda_batch_t *nimroda_get_batch(nimroda_instance_t *instance);

/**
 * @brief Get the user-supplied pointer from the current instance.
 *
 * @param[in] instance The Nimrod/A instance. May not be NULL.
 *
 * @returns The user-supplied pointer from the current instance.
 */
NIMRODA_API void *nimroda_get_user_pointer(nimroda_instance_t *instance);

/**
 * @brief Save the state of the algorithm to the given IO context.
 *
 * @param[in] instance The Nimrod/A instance. May not be NULL.
 * @param[in] ctx The C/IO context. May not be NULL.
 *
 * @returns On success, zero is returned. On failure, a non-zero value is returned.
 */
NIMRODA_API int nimroda_save(nimroda_instance_t *instance, cio_context_t *ctx);

/**
 * @brief Restore the state of the algorithm from the given IO context.
 *
 * @param[in] callbacks The Nimrod/A callbacks. May not be NULL.
 * @param[in] ctx The C/IO context. May not be NULL.
 *
 * @returns On success, a new Nimrod/A instance is returned. On failure, NULL is returned.
 */
NIMRODA_API nimroda_instance_t *nimroda_restore(nimroda_callbacks_t *callbacks, cio_context_t *ctx);

/**
 * @brief Hash the given data.
 *
 * @param[in] data A pointer to the data. May not be NULL.
 * @param[in] size The size of the data in bytes.
 *
 * @returns A platform-agnostic, deterministic hash code.
 */
NIMRODA_API uint64_t nimroda_hash(const void *data, size_t size);

/**
 * @brief Hash a IEEE754 double-precision floating point.
 *
 * This is a special hash that is used by nimroda_hash_double_array().
 *
 * @returns A hash.
 */
NIMRODA_API uint64_t nimroda_hash_double(double val);

/**
 * @brief Hash an array of IEEE754 double-precision floating point numbers.
 *
 * This is a special hash, allowing doubles to be used in hash table-based structures
 * without running into any precision issues.
 *
 * @returns A hash.
 */
NIMRODA_API uint64_t nimroda_hash_double_array(const double *p, size_t count);

#ifdef __cplusplus
}
#endif

#endif /* NIMRODA_PUBLIC_NIMRODA_P_H */
