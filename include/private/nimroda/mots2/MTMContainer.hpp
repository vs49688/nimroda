/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef NIMRODA_PRIVATE_MOTS2_MTMCONTAINER_HPP
#define NIMRODA_PRIVATE_MOTS2_MTMCONTAINER_HPP

#include "nimroda/cxx/Point.hpp"

namespace NimrodA {
namespace MOTS2 {

/**
 * @brief The Middle-Term-Memory (MTM) container implementation.
 */
class MTMContainer : private PointSet
{
public:
	MTMContainer();
	explicit MTMContainer(const PointSet& pt);

	using PointSet::size;
	using PointSet::clear;
	using PointSet::erase;
	using PointSet::insert;
	using PointSet::begin;
	using PointSet::end;
	using PointSet::empty;

	/**
	 * @brief Remove all dominated points from this container.
	 */
	void removeDominatedPoints();

	/**
	 * @brief If @p pt is not dominated, add it to this container.
	 *
	 * @param[in] pt The point to add.
	 *
	 * @returns If the point was not dominated, and thus added, this function
	 * returns true. If the point was dominated, this function returns false.
	 */
	bool addIfNotDominated(const Point::Ref& pt);

	double calculateQualityIndicator(const DoubleVector& refPt) const;

};

}
}
#endif /* NIMRODA_PRIVATE_MOTS2_MTMCONTAINER_HPP */