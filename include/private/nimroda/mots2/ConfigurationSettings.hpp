/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef NIMRODA_MOTS2_CONFIGURATIONSETTINGS_HPP
#define NIMRODA_MOTS2_CONFIGURATIONSETTINGS_HPP

#include "nimroda/config.h"

#include <stdio.h>
#include <string>
#include <vector>

#include "nimroda/cxx/CoordinateVector.hpp"

namespace NimrodA {
class Nimrod;
class CIOContext;

namespace MOTS2 {

/**
* @brief Defines the configuration for the MOTS2 implementation.
*/
class ConfigurationSettings
{
public:
	/**
	 * @brief Construct a new MOTS2 configuration structure using default values.
	 */
	ConfigurationSettings(Nimrod& nimrod);

	void save(CIOContext& ctx);
	void restore(CIOContext& ctx);

	/**
	 * @brief Represents the frequency (how many consecutive unsuccessful Hooke & Jeeves moves
	 * have been recorded) of the 'diversification move' execution during a search.
	 *
	 * @pre @p diversify > 0
	 */
	size_t diversify;

	/**
	 * @brief Represents the frequency (how many consecutive unsuccessful Hooke & Jeeves moves
	 * have been recorded) of the 'intensification move' execution during a search.
	 *
	 * @pre @p intensify > 0
	 */
	size_t intensify;

	/**
	 * @brief Represents the frequency (how many consecutive unsuccessful Hooke & Jeeves moves
	 * have been recorded) of the 'step size reduction and restart move' execution during a search.
	 *
	 * @pre @p reduce > 0
	 */
	size_t reduce;

	/**
	 * @brief The initial step size for each variable.
	 */
	DoubleVector stepSize;

	/**
	 * @brief Represents the percentage reduction of the 'initial step size'.
	 * Following the previous example, if two 'step size reduction and restart move' were executed
	 * then the current 'step size' will be `0.2*(0.5*0.5)=0.05`
	 * @pre SSRF > 0
	 */
	double SSRF;

	/**
	 * @brief Represents the frequency (how many consecutive unsuccessful Hooke & Jeeves moves have been recorded)
	 * of the step size reduction and restart move execution during a search.
	 *
	 * @pre @p numSamples > 0
	 */
	size_t numSamples;

	//loop limit
	size_t loopLimit;
	//Improvements limit , number of consecutive improvements
	size_t improveLimit;

	/**
	 * @brief The maximum number of evaluations that should be done.
	 *
	 * @pre @p evalLimit > 0
	 */
	size_t evalLimit;

	/**
	 * @brief Represents the number of subdomains the whole search domain will be divided
	 * into for each design variable. For the moment, the same number of regions is applied on
	 * variable range.
	 *
	 * @pre @p numRegions > 0
	 */
	size_t numRegions;

	/**
	 * @brief Defines how many points will be considered Tabu at each Hooke & Jeeves move.
	 *
	 * @pre @p stmSize > 0
	 */
	size_t stmSize;
};

}
}
#endif /* NIMRODA_MOTS2_CONFIGURATIONSETTINGS_HPP */
