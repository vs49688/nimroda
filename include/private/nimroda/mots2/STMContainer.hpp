/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef NIMRODA_PRIVATE_MOTS2_STMCONTAINER_HPP
#define NIMRODA_PRIVATE_MOTS2_STMCONTAINER_HPP

#include "nimroda/cxx/Point.hpp"

namespace NimrodA {
namespace MOTS2 {

/**
 * @brief The Short-Term-Memory (STM) container implementation.
 */
class STMContainer : private PointList
{
public:
	/**
	 * @brief Construct a new STM container.
	 *
	 * @param[in] stmSize The maximum size of the container.
	 *
	 * @pre @p stmSize > 0
	 */
	STMContainer(size_t stmSize);

	void add(const Point::Ref& pt);

	using PointList::insert;
	using PointList::clear;
	using PointList::begin;
	using PointList::cbegin;
	using PointList::end;
	using PointList::cend;
	using PointList::size;

	/**
	 * @brief Check if the specified point is Tabu.
	 * A point is considered Tabu if it is contained within this list.
	 *
	 * @param[in] pt The point to check for.
	 *
	 * @returns If this container contains @p pt, true, Otherwise false.
	 */
	bool isTabu(const Point::Ref& pt) const;

private:
	size_t m_STMSize;
};

}
}
#endif /* NIMRODA_PRIVATE_MOTS2_STMCONTAINER_HPP */