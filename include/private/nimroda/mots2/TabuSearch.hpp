/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef NIMRODA_MOTS2_TABUSEARCH_HPP
#define NIMRODA_MOTS2_TABUSEARCH_HPP

#include <stdio.h>
#include <stdint.h>
#include <string>
#include <deque>
#include <set>

#include "nimroda/cxx/Nimrod.hpp"
#include "nimroda/cxx/CIOContext.hpp"

#include "ConfigurationSettings.hpp"

#include "nimroda/mots2/STMContainer.hpp"
#include "nimroda/mots2/MTMContainer.hpp"
#include "nimroda/mots2/LTMContainer.hpp"

namespace NimrodA {
namespace MOTS2 {


class HookeJeevesState
{
public:
	HookeJeevesState();

	void reset();
	void write(CIOContext& ctx);
	void read(CIOContext& ctx, PointDB& db);

	size_t remainingSampleSets;
	PointSet genPoints;
	PointSet evaluatedPoints;
	Point::Ref nextPoint;
	PointSet dominated;
	PointSet dominant;
	size_t setCounter;

	bool inEval;
};

class DiversifyState
{
public:
	DiversifyState();

	void reset();
	void write(CIOContext& ctx);
	void read(CIOContext& ctx);

	size_t evalPatience;
	size_t tabuPatience;
};

class TabuSearch
{
public:
	enum class MoveType
	{
		Initial,
		Pattern,
		Diversify,
		Intensify,
		Reduce,
		HookeJeeves
	};

	enum class TabuState
	{
		Initial, InitialEval,
		Iterating,
		Pattern, PatternEval,
		HookeJeeves, HookeJeevesEval, HookeJeevesFinal,
		Line18,
		Diversify, DiversifyEval
	};

	/**
	* @brief Construct a new TabuSearch using the specified configuration.
	*
	* @param[in] nimrod The current Nimrod/A instance.
	*/
	explicit TabuSearch(NimrodA::Nimrod& nimrod);

	TabuSearch(const TabuSearch&) = delete;
	TabuSearch(TabuSearch&&) = delete;

	void fire();
	void save(CIOContext& ctx);
	void restore(CIOContext& ctx);

	const MTMContainer& getParetoFront();

	/**
	 * @brief Write the pareto front points to a stream in GNUPlot format.
	 *
	 * @param[in] s The stream to write to.
	 */
	void dumpParetoFront(std::ostream& s);

private:
	void reset();

	bool stoppingCriteriaNotMet();

	void updateMemories();
	void checkMemories();

	void pushBasePoint(const Point::Ref& pt, MoveType moveType);

	void patternMoveSM();

	bool hjValidate(const Point::Ref& pt) const;
	void hjGenerate(const Point::ConstRef& start, PointSet& points) const;

	void hookeJeevesSM();
	void diversifyMoveSM();

	/**
	 * @brief Perform an intensification move.
	 *
	 * This will select a point from the IM, and remove it.
	 *
	 * @remark No other members are changed.
	 *
	 * @returns The point that was selected.
	 */
	Point::Ref intensifyMove();
	Point::Ref reduceMove();

	void updateStatus(int32_t status, const char *message);

	/**
	 * @brief Create a new point with the coordinate at @p index
	 * incremented by a single step.
	 *
	 * @param[in] _template The template point.
	 * @param[in] index The index of the coordinate.
	 */
	Point::Ref increase(const Point& _template, size_t index) const;

	/**
	* @brief Create a new point with the coordinate at @p index
	* decremented by a single step.
	*
	* @param[in] _template The template point.
	* @param[in] index The index of the coordinate.
	*/
	Point::Ref decrease(const Point& _template, size_t index) const;

	template <typename U, typename V, typename W>
	U selectRandomFromContainer(std::set<U, V, W>& container, bool remove) const
	{
		if(container.size() == 0)
			throw std::out_of_range("container empty");

		size_t index = m_Nimrod.randomUInt(0, static_cast<uint32_t>(container.size() - 1));

		auto it = container.begin();
		std::advance(it, index);

		U ref = *it;

		if(remove)
			container.erase(it);

		return ref;
	}

	void writeStats() const;

	Nimrod& m_Nimrod;

	ConfigurationSettings m_Config;

	STMContainer m_STM;
	MTMContainer m_MTM;
	MTMContainer m_IM;
	LTMContainer m_LTM;

	bool m_NextMoveHookJeeves;

	DoubleVector m_InitialStep;

	size_t m_LoopCounter;

	size_t m_iLocal;

	/** @brief The number of diversifications. This is for reporting purposes only. */
	size_t m_Diversification;

	/** @brief The number of intensifications. This is for reporting purposes only. */
	size_t m_Intensification;
	size_t m_Reduction;

	std::list<Point::Ref> m_Points;

	TabuState m_State;
	HookeJeevesState m_HookeJeeves;
	DiversifyState m_Diversify;
};

}
}

#endif /* NIMRODA_MOTS2_TABUSEARCH_HPP */
