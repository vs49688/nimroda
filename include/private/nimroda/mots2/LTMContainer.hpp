/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef NIMRODA_PRIVATE_MOTS2_LTMCONTAINER_HPP
#define NIMRODA_PRIVATE_MOTS2_LTMCONTAINER_HPP

#include "nimroda/cxx/Nimrod.hpp"
#include "nimroda/cxx/PointDB.hpp"

namespace NimrodA {
namespace MOTS2 {

class LTMContainer
{
public:
	LTMContainer(Nimrod& nimrod, size_t numRegions);
	LTMContainer(const LTMContainer&) = default;
	LTMContainer(LTMContainer&&) = default;

	/** @brief The number of regions. */
	const size_t numRegions;

	/** @brief The number of variables. */
	const size_t numVars;

	const CoordinateVector regionSteps;

	void add(const Point::Ref& pt);

	void reset() noexcept;

	Point::Ref generatePotentialPoint();

	void save(CIOContext& ctx, const char *name) const;
	static LTMContainer restore(Nimrod& nimrod, CIOContext& ctx, const char *name);

	LTMContainer& operator=(const LTMContainer&) = default;
	LTMContainer& operator=(LTMContainer&&) = default;

private:
	using CountVector = std::vector<size_t>;
	using RegionVector = std::vector<CountVector>;

	LTMContainer(Nimrod& nimrod, size_t numRegions, RegionVector&& regionVec);

	size_t getRegionIndex(double val, size_t coordIndex);
	double getCoordFromLeastVisitedRegion(size_t coordIndex);

	RegionVector m_RegionCounts;

	Nimrod& m_Nimrod;
};

}
}
#endif /* NIMRODA_PRIVATE_MOTS2_LTMCONTAINER_HPP */