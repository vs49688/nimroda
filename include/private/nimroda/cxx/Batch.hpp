/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef NIMRODA_PRIVATE_CXX_BATCH_HPP
#define NIMRODA_PRIVATE_CXX_BATCH_HPP

#include <vector>
#include "nimroda/nimroda_ip.h"
#include "Point.hpp"

namespace NimrodA {

class Nimrod;

/**
 * @brief A wrapper around a ::nimroda_batch_t.
 *
 * This maintains a ::nimroda_batch_t, except the point list is backed by
 * a fixed-size std::vector.
 */
class Batch
{
public:
	~Batch();

	/** @brief The number of points in the batch. */
	const size_t numPoints;

	/**
	 * @brief Get the point at the given index.
	 *
	 * @returns The point at index @p index.
	 *
	 * @throws std::out_of_range - if @p index is invalid.
	 */
	Point::Ref get(size_t index) const;

	/**
	* @brief Get a const-iterator pointing to the first element in the batch.
	*
	* @returns Get a const-iterator pointing to the first element in the batch.
	*
	* @sa std::vector::cbegin
	*/
	PointVector::const_iterator begin() const noexcept;

	/**
	* @brief Get a const-iterator pointing to the past-the-end element in the batch.
	*
	* @returns Get a const-iterator pointing to the past-the-end element in the batch.
	*
	* @sa std::vector::cend
	*/
	PointVector::const_iterator end() const noexcept;

private:
	explicit Batch(const std::vector<Point::Ref>& points);
	Batch(nimroda_batch_t *batch, const std::vector<Point::Ref>& points);

	nimroda_batch_t *m_pBatch;
	nimroda_batch_t m_Batch;
	const std::vector<Point::Ref> m_PointVector;
	const std::vector<nimroda_point_t*> m_RawVector;

	friend class Nimrod;
};

}

#endif /* NIMRODA_PRIVATE_CXX_BATCH_HPP */
