/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef NIMRODA_PRIVATE_CXX_POINT_HPP
#define NIMRODA_PRIVATE_CXX_POINT_HPP

#include <memory>
#include <vector>
#include <list>
#include <unordered_set>
#include "nimroda/nimroda_ip.h"
#include "CoordinateVector.hpp"

namespace NimrodA {

/**
 * @brief A wrapper around a ::nimroda_point_t.
 *
 * This maintains a ::nimroda_point_t, except the coordinate and objective
 * memory is backed by a fixed-size DoubleVector.
 *
 * @remark Any modification to the point via the `+= -= /= *=` operators will
 * cause an invalidation of the objective values, which are reset to #HUGEISH.
 * @see Nimrod::createPoint(const DoubleVector& coords)
 * @see Nimrod::createPoint(const DoubleVector& coords, const DoubleVector& objectives)
 */
class Point : public std::enable_shared_from_this<Point>
{
public:
	typedef uint32_t Id;

	/** @brief A shared reference to a Point. */
	typedef std::shared_ptr<Point> Ref;
	/** @brief A shared const reference to a Point. */
	typedef std::shared_ptr<const Point> ConstRef;

	/** @brief The number of coordinates the point has. */
	const size_t numCoordinates;
	/** @brief The number of objectives this point has. */
	const size_t numObjectives;
	/** @brief The instance id. */
	const Id instanceId;

	/**
	 * @brief Get the coordinates of this point.
	 *
	 * @returns The coordinates of this point.
	 */
	const DoubleVector& getCoordinates() const;

	/**
	 * @brief Get the objective values of this point.
	 *
	 * @returns The objective values of this point.
	 */
	const DoubleVector& getObjectives() const;

	/**
	 * @brief Clamp the coordinates of this point between the given bounds.
	 *
	 * @remark If changes are applied, then the objectives are invalidated.
	 *
	 * @throws std::length_error - if the dimensions of either @p lower and @p upper
	 * differ from numCoordinates.
	 */
	void clampCoordinates(const DoubleVector& lower, const DoubleVector& upper);

	/**
	 * @brief Return the euclidean distance between this point and @p pt.
	 *
	 * @returns The euclidean distance between this point and @p pt.
	 *
	 * @throws std::length_error - if `numCoordinates != pt.numCoordinates`
	 */
	double euclideanDistance(const Point& pt) const;

	/**
	 * @brief Return the euclidean distance between this point and @p pt.
	 *
	 * @returns The euclidean distance between this point and @p pt.
	 *
	 * @throws std::length_error - if `numCoordinates != pt.size()`
	 */
	double euclideanDistance(const DoubleVector& pt) const;

	Job_Status getJobStatus() const;

	size_t hashCode() const;

	double operator[](size_t index) const;

	bool operator==(const Point& rhs) const;
	bool operator!=(const Point& rhs) const;
	bool operator<(const Point& rhs) const;

	/**
	 * @brief Get a string representation of this point.
	 *
	 * @param[in] objectives If true, the objective values will be formatted
	 * instead of the coordinates.
	 *
	 * @remark This is slow and should be used sparingly.
	 */
	std::string toString(bool objectives = false) const;

	/**
	 * @brief Does this point @p pt?
	 *
	 * @param[in] pt The point.
	 *
	 * @returns If this dominates @p pt, this function returns 1. If @p pt dominates this,
	 * this function returns -1. If both points are equal, 0 is returned.
	 *
	 * @throws std::length_error - if this and @p pt are incompatible in both coordinates
	 * and objectives.
	 *
	 * @remark A point @p x1, with an objective function vector @p f1, is said to dominate
	 * point @p x2, with and objective function vector @p f2, if no component of @p f1 is greater
	 * than its corresponding component in @p f2, and at least one component is smaller.
	 * @remark The current objective values of each point are used. Take care if the objectives
	 * have been previously invalidated.
	 */
	int dominates(const Point& pt) const;

	/**
	 * @brief Invalidate the objective values, setting them all to #HUGEISH.
	 */
	void invalidateObjectives();

	operator const nimroda_point_t*() const noexcept;

private:
	Point(Id instanceId, const double *coords, size_t numCoords, const double *objectives, size_t numObjectives);
	Point(Id instanceId, const nimroda_point_t * const pt);

	void ensureCoordinateCompatibility(const DoubleVector& pt) const;
	void ensureObjectiveCompatibility(const DoubleVector& pt) const;

	//Nimrod& m_Nimrod;
	nimroda_point_t m_Point;

	DoubleVector m_Coordinates;
	DoubleVector m_Objectives;

	/** @cond */
	friend class Batch;
	friend class OptimStatus;
	friend class PointDB;
	/** @endcond */
};

}

/** @cond */

/* std::hash and std::equal_to implementations for Point::ConstRef.
* These should work for Point::Ref too.
*/

namespace std {

template<> struct hash<NimrodA::Point::ConstRef>
{
	size_t operator()(const NimrodA::Point::ConstRef& pt) const
	{
		if(!pt)
			throw std::invalid_argument("pt == nullptr");

		return pt->hashCode();
	}
};


template<> struct hash<NimrodA::Point::Ref>
{
	size_t operator()(const NimrodA::Point::Ref& pt) const
	{
		return std::hash<NimrodA::Point::ConstRef>()(pt);
	}
};


template<> struct equal_to<NimrodA::Point::ConstRef>
{
	typedef NimrodA::Point::ConstRef Type;

	bool operator()(const Type& pt1, const Type& pt2) const
	{
		if(!pt1)
			throw std::invalid_argument("pt1 == null");

		if(!pt2)
			throw std::invalid_argument("pt2 == null");

		return *pt1 == *pt2;
	}
};

template<> struct equal_to<NimrodA::Point::Ref>
{
	typedef NimrodA::Point::ConstRef Type;

	bool operator()(const Type& pt1, const Type& pt2) const
	{
		return std::equal_to<NimrodA::Point::ConstRef>()(pt1, pt2);
	}
};

template<> struct less<NimrodA::Point::ConstRef>
{
	typedef NimrodA::Point::ConstRef Type;

	bool operator()(const Type& pt1, const Type& pt2) const
	{
		return *pt1 < *pt2;
	}
};

template<> struct less<NimrodA::Point::Ref>
{
	typedef NimrodA::Point::Ref Type;

	bool operator()(const Type& pt1, const Type& pt2) const
	{
		return std::less<NimrodA::Point::ConstRef>()(pt1, pt2);
	}
};

}

namespace NimrodA {
using PointList = std::list<Point::Ref>;
using PointVector = std::vector<Point::Ref>;
using PointSet = std::unordered_set<Point::Ref, std::hash<Point::Ref>, std::equal_to<Point::Ref>>;
}
/** @endcond */
#endif /* NIMRODA_PRIVATE_CXX_POINT_HPP */