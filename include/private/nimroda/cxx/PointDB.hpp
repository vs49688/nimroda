/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef NIMRODA_PRIVATE_CXX_POINTDB_HPP
#define NIMRODA_PRIVATE_CXX_POINTDB_HPP

#include <unordered_map>
#include <random>
#include "nimroda/cxx/Point.hpp"

namespace NimrodA {

class CIOContext;

class PointDB
{
public:
	PointDB(size_t numCoords, size_t numObjectives);

	PointDB(const PointDB&) = delete;
	PointDB(PointDB&&) = delete;

	PointDB& operator=(const PointDB&) = delete;
	PointDB& operator=(PointDB&&) = delete;

	Point::Ref lookupPoint(Point::Id id) noexcept;
	Point::ConstRef lookupPoint(Point::Id id) const noexcept;

	/**
	* @brief Create a new point using the given coordinates.
	* The objectives values are set to that of failedObjectivesVector.
	*
	* @returns A new point.
	*
	* @throws std::invalid_argument - if `coords.size() != dimensionality`
	* @throws std::bad_alloc - if memory allocation fails.
	*/
	Point::Ref createPoint(const DoubleVector& coords);

	Point::Ref createPoint(const std::vector<double>& coords);
	/**
	* @brief Create a new point with the given coordinates and objective values.
	*
	* @returns A new point with the given coordinates and objective values.
	*
	* @throws std::invalid_argument - if `coords.size() != dimensionality`
	* @throws std::invalid_argument - if `objectives.size() != numObjectives`
	*/
	Point::Ref createPoint(const DoubleVector& coords, const DoubleVector& objectives);
	Point::Ref createPoint(const Point& pt);
	Point::Ref createPoint(const Point::ConstRef& pt);
	Point::Ref createPoint(const nimroda_point_t *pt);

	void save(CIOContext& ctx);

	/**
	 * @brief Restore the point database from the given C/IO context.
	 *
	 * @param[in] ctx The C/IO context.
	 *
	 * @returns A vector of all the loaded points, keep them around if you don't
	 * want them destroyed.
	 *
	 * @remark The database will be cleared before any points are read.
	 */
	std::vector<Point::Ref> restore(CIOContext& ctx);
private:
	void deletePoint(Point *pt);

	Point::Id generateId() noexcept;

	Point::Ref createPoint(const double *coords, size_t numCoords, const double *objectives, size_t numObjectives);
	Point::Ref createPoint(Point::Id id, const double *coords, size_t numCoords, const double *objectives, size_t numObjectives);

	const size_t m_NumCoords;
	const size_t m_NumObjectives;
	const DoubleVector m_FailedObjectivesVector;
	std::unordered_map<Point::Id, Point*> m_Points;
	std::mt19937 m_RNG;
};

}
#endif /* NIMRODA_PRIVATE_CXX_POINTDB_HPP */