/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef NIMRODA_PRIVATE_CXX_COORDINATEVECTOR_HPP
#define NIMRODA_PRIVATE_CXX_COORDINATEVECTOR_HPP

#include <cstdint>
#include <vector>

namespace NimrodA {

class CoordinateVector
{
public:
	static constexpr double epsilon = 1e-4;

	CoordinateVector();
	explicit CoordinateVector(size_t size);
	CoordinateVector(size_t size, double val);
	CoordinateVector(const double *coords, size_t count);
	explicit CoordinateVector(const std::vector<double>& vec);
	CoordinateVector(const CoordinateVector& rhs);
	explicit CoordinateVector(CoordinateVector&& rhs);

	size_t getLength() const;

	const double *getData() const;
	double *getData();

	double euclideanDistance(const CoordinateVector& pt) const;

	size_t hashCode() const;

	double operator[](size_t index) const;
	double& operator[](size_t index);

	CoordinateVector& operator+=(const CoordinateVector& rhs);
	CoordinateVector& operator+=(double n);

	CoordinateVector& operator-=(const CoordinateVector& rhs);
	CoordinateVector& operator-= (double n);

	CoordinateVector& operator*=(const CoordinateVector& rhs);
	CoordinateVector& operator*= (double n);

	CoordinateVector& operator/=(const CoordinateVector& rhs);
	CoordinateVector& operator/=(double n);

	CoordinateVector operator+(const CoordinateVector& rhs) const;
	CoordinateVector operator+(double n) const;

	CoordinateVector operator-(const CoordinateVector& rhs) const;
	CoordinateVector operator-(double n) const;

	CoordinateVector operator/(const CoordinateVector& rhs) const;
	CoordinateVector operator/(double n) const;

	CoordinateVector& operator=(const CoordinateVector& rhs);
	CoordinateVector& operator=(CoordinateVector&& rhs);
	CoordinateVector& operator=(double n);
	CoordinateVector& operator=(const std::vector<double>& rhs);
	CoordinateVector& operator=(std::vector<double>&& rhs);

	bool operator==(const CoordinateVector& rhs) const;
	bool operator!=(const CoordinateVector& rhs) const;
	bool operator<(const CoordinateVector& rhs) const;

	std::string toString() const;

	int dominates(const CoordinateVector& rhs) const;

	/**
	 * @brief Read a double vector from the given stream.
	 * The format for a vector is `<count> <param_1> <param_2> ... <param_n>`
	 *
	 * @param[in] s The stream to read from.
	 *
	 * @returns A vector of read points, if any.
	 *
	 * @remark If any error occurs, the returned vector is empty.
	 */
	static CoordinateVector readVector(std::istream& s);

	/**
	 * @brief Write a double vector to the given stream.
	 * The format for a vector is `<count> <param_1> <param_2> ... <param_n>`
	 *
	 * @param[in] s The stream to write to.
	 *
	 * @remark This is able to be read by readVector()
	 */
	void writeVector(std::ostream& s);

	/**
	 * @brief Is @p pt compatible with this vector?
	 * Two vectors are _compatible_ iff there their lengths are the same.
	 *
	 * @param[in] pt The vector to check compatibility of.
	 *
	 * @returns If this vector is compatible with @p pt, true. Otherwise false.
	 */
	bool isCompatible(const CoordinateVector& pt) const noexcept;

	/**
	 * @brief Is @p pt compatible with this vector?
	 * Two vectors are _compatible_ iff there their lengths are the same.
	 *
	 * @param[in] pt The vector to check compatibility of.
	 *
	 * @throws std::length_error - If @p pt is not compatible with this vector.
	 */
	void ensureCompatibility(const CoordinateVector& pt) const;

	//Allocator& getAllocator() const noexcept;
private:
	std::vector<double> m_Vector;
};

using DoubleVector = CoordinateVector;

}

/** @cond */
namespace std {

template<> struct hash<NimrodA::CoordinateVector>
{
	size_t operator()(const NimrodA::CoordinateVector& pt) const { return pt.hashCode(); }
};

}
/** @endcond */

#endif /* NIMRODA_PRIVATE_CXX_COORDINATEVECTOR_HPP */