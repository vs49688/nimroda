/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef NIMRODA_PRIVATE_CXX_CIOCONTEXT_HPP
#define NIMRODA_PRIVATE_CXX_CIOCONTEXT_HPP

#include <stdexcept>
#include <memory>
#include "nimroda/nimroda_ip.h"

namespace NimrodA {

class Point;
class PointDB;
class CoordinateVector;

class CIOContext
{
public:
	explicit CIOContext(cio_context_t *ctx, bool wrap = false);
	CIOContext(CIO_PUSHPOPPROC push, CIO_PUSHPOPPROC pop, CIO_READPROC read, CIO_WRITEPROC write, void *user);
	CIOContext(cio_context_t *parent, bool list, const char *name);

	CIOContext(const CIOContext&) = delete;
	CIOContext(CIOContext&&) = delete;

	~CIOContext();

	void write(const void *buffer, size_t size, cio_type_t type, const char *name = nullptr);
	void read(void *buffer, size_t size, cio_type_t type, const char *name = nullptr);

	void write_point(const nimroda_point_t * const point);

	/**
	 * @brief Read a point into the specified point structure.
	 *
	 * @param[in] point The point. This is expected to have no fields initialised.
	 *
	 * @remark The point must be passed to nimroda_point_release() after use.
	 */
	void read_point(nimroda_point_t *point);

	/**
	 * @brief Read a point into an already-allocated point.
	 *
	 * @param[in] point The point. This is expected to have the @p num_dims and
	 * @p num_objectives fields set and the @p coord and @p objectives fields
	 * properly allocated.
	 *
	 * @throws std::runtime_error - if @p num_dims and @p num_objectives doesn't
	 * match those read.
	 */
	void read_point_noalloc(nimroda_point_t *point);
private:
	/**
	 * @brief Read the @p status, @p coord, and @p objectives fields
	 * using existing memory.
	 *
	 * @param[in] point The point. This is expected to have the @p num_dims and
	 * @p num_objectives fields set and the @p coord and @p objectives fields
	 * properly allocated.
	 */
	void read_point_noalloc2(nimroda_point_t *point);
public:

	/**
	 * @brief Read a point.
	 *
	 * @returns A point.
	 */
	ScopedPoint read_point();

	void write_point_reference(const std::shared_ptr<const Point>& pt, const char *name = nullptr);
	std::shared_ptr<Point> read_point_reference(PointDB& db, const char *name = nullptr);

	void write_coord_vector(const CoordinateVector& vec, const char *name = nullptr);
	CoordinateVector read_coord_vector(const char *name = nullptr);

	void write_string(const char *s, const char *name = nullptr);
	void write_string(const std::string& s, const char *name = nullptr);
	std::string read_string(const char *name = nullptr);

#define NIMACIO_DECLARE_PRIMITIVE_WRITER(fnname, type)			\
	void write_##fnname(type val, const char *name = nullptr)	\
	{ 															\
		if(cio_write_##fnname(*this, val, name)) 				\
			throw Exception(); 									\
	}

#define NIMACIO_DECLARE_PRIMITIVE_READER(fnname, type)		\
	type read_##fnname(const char *name = nullptr)			\
	{														\
		int status;											\
		type val = cio_read_##fnname(*this, &status, name);	\
		if(status)											\
			throw Exception();								\
		return val;											\
	}														\
	type read_##fnname(size_t index)						\
	{														\
		return read_##fnname(nullptr);						\
	}

#define NIMACIO_DECLARE_PRIMITIVE_IO(name, type)	\
	NIMACIO_DECLARE_PRIMITIVE_WRITER(name, type)	\
	NIMACIO_DECLARE_PRIMITIVE_READER(name, type)

	NIMACIO_DECLARE_PRIMITIVE_IO(int8, int8_t);
	NIMACIO_DECLARE_PRIMITIVE_IO(uint8, uint8_t);
	NIMACIO_DECLARE_PRIMITIVE_IO(int16, int16_t);
	NIMACIO_DECLARE_PRIMITIVE_IO(uint16, uint16_t);
	NIMACIO_DECLARE_PRIMITIVE_IO(int32, int32_t);
	NIMACIO_DECLARE_PRIMITIVE_IO(uint32, uint32_t);
	NIMACIO_DECLARE_PRIMITIVE_IO(int64, int64_t);
	NIMACIO_DECLARE_PRIMITIVE_IO(uint64, uint64_t);
	NIMACIO_DECLARE_PRIMITIVE_IO(float32, float);
	NIMACIO_DECLARE_PRIMITIVE_IO(float64, double);


	operator cio_context_t*() const noexcept;

	class Exception : public std::runtime_error
	{
	public:
		using std::runtime_error::runtime_error;
		Exception();
	};

private:
	cio_context_t *m_pContext;
	cio_context_t m_Context;
};
}

#endif /* NIMRODA_PRIVATE_CXX_CIOCONTEXT_HPP */