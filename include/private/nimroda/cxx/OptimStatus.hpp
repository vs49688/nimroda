/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef NIMRODA_PRIVATE_CXX_OPTIMSTATUS_HPP
#define NIMRODA_PRIVATE_CXX_OPTIMSTATUS_HPP

#include <vector>
#include "Point.hpp"

namespace NimrodA {

class Nimrod;
enum class OptimisationState;

class OptimStatus
{
public:
	~OptimStatus();

	OptimisationState getState() const noexcept;
	const std::vector<Point::Ref>& getResults() const noexcept;
	const char *getErrorString() const noexcept;
	int32_t getCode() const noexcept;

	void clearResults() noexcept;
	void addResultPoint(const Point::Ref& pt);
	void setErrorString(const char *s);
	void setErrorString(const std::string& s);
	void setCode(int32_t code);

private:
	OptimStatus(Nimrod& nimrod);

	Nimrod& m_Nimrod;
	nimroda_optim_status_t& m_RawStatus;

	std::vector<Point::Ref> m_Results;
	std::vector<nimroda_point_t*> m_RawVector;

	friend class Nimrod;
};

}

#endif /* NIMRODA_PRIVATE_CXX_OPTIMSTATUS_HPP */
