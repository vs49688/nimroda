/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef NIMRODA_PRIVATE_CXX_NIMRODA_HPP
#define NIMRODA_PRIVATE_CXX_NIMRODA_HPP

#include <cassert>
#include <list>
#include <vector>
#include <set>
#include <unordered_map>
#include <ostream>
#include <sstream>

#include "nimroda/nimroda_ip.h"
#include "CoordinateVector.hpp"
#include "Point.hpp"
#include "PointDB.hpp"
#include "Batch.hpp"
#include "OptimStatus.hpp"

namespace NimrodA {

enum class OptimisationState
{
	Stopped,
	WaitingForBatch,
	Refire,
	Finished
};

enum class DebugLevel
{
	None, Warn, Error,
	Info, MoreInfo,
	Debug, MoreDebug,
	Trace
};

class Nimrod
{
public:

	Nimrod(nimroda_instance_t *instance);

	nimroda_instance_t * const instance;

	const size_t dimensionality;
	const size_t numObjectives;

	const DoubleVector upperBound;
	const DoubleVector lowerBound;
	const DoubleVector range;
	PointDB pointDb;

	Point::ConstRef getStartingPoint();
	/**
	 * @brief Set the optimisation state.
	 *
	 * @param[in] state The optimisation state.
	 *
	 * @remark Each time this is called, the current batch is cleared.
	 * In cases where a batch is required, set it AFTER calling setState().
	 */
	void setState(OptimisationState state);

	OptimisationState getState() const noexcept;

	void setBatch(const PointVector& batch);

	const Batch& getBatch() const;

	/**
	 * @brief Check if the given point is valid for the objective function.
	 *
	 * @param[in] pt The point to validate.
	 *
	 * @returns If the point is valid for this objective function, true is returned.
	 * Otherwise, false.
	 */
	bool isValid(const Point& pt);

	void puts(const char *s);
	void printf(const char *fmt, ...);

	void vprintf(const char *fmt, va_list ap);

	DebugLevel getDebugLevel() const noexcept;

	bool debugEnabled(DebugLevel level) const noexcept;

	uint32_t randomUInt(uint32_t min, uint32_t max) noexcept;
	double randomDouble(double min, double max) noexcept;

	/**
	 * @brief Copy this point to a Nimrod/A-managed point.
	 *
	 * @param[in] dst The destination point. May not be nullptr.
	 * @param[in] src The source point.
	 *
	 * @returns @p dst
	 *
	 * @throws std::invalid_argument - if @p point is nullptr.
	 * @throws std::bad_alloc - if malloc() fails.
	 *
	 * @remark @p dst is passed to utils2CopyPoint()
	 */
	nimroda_point_t *copyPoint(nimroda_point_t *dst, const Point& src);

	/**
	 * @brief Get the value for the given custom configuration option.
	 *
	 * @param[in] The key.
	 *
	 * @returns If the key exists, the value. Otherwise, and empty string.
	 */
	const char *getConfig(const char *key) const noexcept;

	const ConfigMap& getConfig() const noexcept;

	OptimStatus& getOptimStatus() noexcept;

	size_t getEvalCount() const noexcept;

	typedef std::vector<std::pair<std::string, std::string>> StatsVector;

	void writeStats(const StatsVector& stats) noexcept;

	template <typename T>
	Point::Ref selectRandomFromContainer(T& container, bool remove);

	void save(CIOContext &ctx);
	void restore(CIOContext& ctx);

private:
	Point::Ref m_StartingPoint;
	std::unique_ptr<Batch> m_CurrentBatch;
	OptimStatus m_OptimStatus;

	friend class Point;
};

template <typename T>
Point::Ref Nimrod::selectRandomFromContainer(T& container, bool remove)
{
	if(container.size() == 0)
		throw std::out_of_range("container empty");

	assert(container.size() <= UINT32_MAX);

	uint32_t index = this->randomUInt(0, (uint32_t)container.size() - 1);

	auto it = container.begin();
	std::advance(it, index);

	Point::Ref ref = *it;

	if(remove)
		container.erase(it);

	return ref;
}

template <typename T>
std::string formatType(const T& val)
{
	std::stringstream ss;
	return ss << val, ss.str();
}

}

#endif /* NIMRODA_PRIVATE_CXX_NIMRODA_HPP */
