/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef NIMRODA_ALGORITHMS_HPP
#define NIMRODA_ALGORITHMS_HPP

/** @cond */
#include "nimroda_ip.h"
#include "mots2/NimrodAdapter.hpp"

extern "C" {
	nimroda_instance_t *always0Init(nimroda_instance_t *instance, const nimroda_custom_config_t *config);
	void always0DeInit(nimroda_instance_t *instance);
	void always0Optimise(nimroda_instance_t *instance);
}

/** @endcond */

#endif /* NIMRODA_ALGORITHMS_HPP */