/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef NIMRODA_PRIVATE_NIMRODA_IP_H
#define NIMRODA_PRIVATE_NIMRODA_IP_H

#include <stdarg.h>
#include "nimroda/nimroda.h"

typedef struct nimroda_optim_data
{
	/** @brief The current state of the optimisation. */
	nimroda_optim_state_t state;

	/**
	* @brief The results of the optimisation. The contents of this is only valid when
	* <code>state == ::STATE_FINISHED</code>.
	*/
	nimroda_optim_status_t status;

	/** @brief The current batch to be evaluated. The contents of this is only valid
	* when <code>state == ::STATE_WAITING_FOR_BATCH</code>*/
	nimroda_batch_t current_batch;

	/** @brief The number of evaluations done so far.
	* This is managed by Nimrod/A and should not be changed. */
	size_t num_evaluations;

	/** @brief Private algorithm-specific data. */
	void *__private;
} nimroda_optim_data_t;

/**
 * @brief The Nimrod/A instance structure.
 */
struct nimroda_instance
{
	/*--------------------Algorithm-Set Attributes--------------------*/
	PFNPUTS puts;
	PFNSTATS write_stats;

	/** @brief A pointer to user-stored data. */
	void *user;

	struct
	{
		/** @brief The maximum debug level. 7 is the maximum. */
		uint32_t debug;
		/** @brief The RNG seed. */
		uint32_t rng_seed;
	} config;

	/** @brief The algorithm information structure. */
	const nimroda_algorithm_t *algorithm;

	/** @brief The dimensionality of each point. */
	size_t num_dims;

	/** @brief The number of objectives of each point. */
	size_t num_objectives;

	/** The attributes of each variable. */
	nimroda_variable_attributes_t var_attribs;

	/** @brief The starting point. */
	nimroda_point_t starting_point;

	/**
	 * @brief A copy of the custom configuration options with duplicate keys removed.
	 * This is backed by memory in @p __cconfig, so don't free it.
	 */
	nimroda_custom_config_t *custom_config;

	/**
	* @brief Private RNG implementation.
	* Use nimroda_get_random_double() or nimroda_get_random_uint().
	*/
	void *__rng;

	/** @brief The backed memory for the custom configuration. */
	void *__cconfig;

	/**
	 * @brief How many time's we've "rolled" the RNG.
	 * This is used to fast-forward after nimroda_restore() has been called.
	 */
	size_t __rng_roll;

	/** @brief The C++ wrapper implementation. This is an instance of NimrodA::Nimrod. */
	void *__cpp;

	/** @brief Algorithm-defined values. */
	nimroda_optim_data_t optim;
};

#ifdef __cplusplus
extern "C" {
#endif

	NIMRODA_INTERNAL_API bool nimroda_validate_callbacks(const nimroda_callbacks_t * const callbacks);
	NIMRODA_INTERNAL_API bool nimroda_validate_config(const nimroda_config_t * const config);

	/**
	 * @brief Get the current state of the optimisation.
	 *
	 * Each state value has different meaning, and some functionality may only
	 * be available when an optimisation is in a certain state. See the function
	 * documentation for more information.
	 *
	 * @param[in] instance The Nimrod/A instance. May not be NULL.
	 *
	 * @returns The current status of the optimisation. This will be one of the
	 * values in ::nimroda_optimisation_status_t.
	 *
	 * @remark This function is thread-safe.
	 */
	NIMRODA_INTERNAL_API nimroda_optim_state_t nimroda_get_optim_state(nimroda_instance_t *instance);

	/**
	 * @brief Generate a random real number in the interval [@p min, @p max].
	 *
	 * @param[in] instance The Nimrod/A instance. May not be NULL.
	 * @param[in] min The minimum value.
	 * @param[in] max The minimum value.
	 *
	 * @returns A psuedo-random real number in the interval [@p min, @p max].
	 */
	NIMRODA_INTERNAL_API double nimroda_random_double(nimroda_instance_t *instance, double min, double max);

	/**
	 * @brief Generate a random size_t in the interval [@p min, @p max].
	 *
	 * @param[in] instance The Nimrod/A instance. May not be NULL.
	 * @param[in] min The minimum value.
	 * @param[in] max The minimum value.
	 *
	 * @returns A psuedo-random size_t in the interval [@p min, @p max].
	 */
	NIMRODA_INTERNAL_API uint32_t nimroda_random_uint(nimroda_instance_t *instance, uint32_t min, uint32_t max);

	NIMRODA_INTERNAL_API char *nimroda_sprintf(nimroda_instance_t *instance, const char *fmt, ...) NIMRODA_PRINTF(2, 3);

	NIMRODA_INTERNAL_API char *nimroda_vsprintf(nimroda_instance_t *instance, const char *fmt, va_list ap);

	NIMRODA_INTERNAL_API void nimroda_printf(nimroda_instance_t *instance, const char *fmt, ...) NIMRODA_PRINTF(2, 3);

	NIMRODA_INTERNAL_API void nimroda_vprintf(nimroda_instance_t *instance, const char *fmt, va_list ap);

	NIMRODA_INTERNAL_API nimroda_point_t *nimroda_point_alloc(nimroda_instance_t *instance);
	NIMRODA_INTERNAL_API nimroda_point_t *nimroda_point_alloc_manual(size_t num_dims, size_t num_objectives);
	NIMRODA_INTERNAL_API nimroda_point_t *nimroda_point_alloc_inplace(nimroda_instance_t *instance, nimroda_point_t *point);
	NIMRODA_INTERNAL_API nimroda_point_t *nimroda_point_alloc_inplace_manual(size_t num_dims, size_t num_objectives, nimroda_point_t *point);
	NIMRODA_INTERNAL_API nimroda_point_t *nimroda_point_copy(nimroda_instance_t *instance, const nimroda_point_t * const point);

	/**
	 * @brief Copy a Nimrod/A point to the specified instance.
	 * @param[in] instance The Nimrod/A instance that will own the point.
	 * @param[in] dst The destination point. Buffers that are still valid will
	 * be reused. If this is not intended, zero the memory before passing.
	 * @param[in] src The source point. Must be a valid point.
	 *
	 * @returns The new point, @dst. If an error occurred, NULL.
	 */
	NIMRODA_INTERNAL_API nimroda_point_t *nimroda_point_copy_inplace(nimroda_instance_t *instance, nimroda_point_t *dst, const nimroda_point_t * const src);

	NIMRODA_INTERNAL_API void nimroda_point_release(nimroda_point_t *point);
	NIMRODA_INTERNAL_API void nimroda_point_release_inplace(nimroda_point_t *point);

	NIMRODA_INTERNAL_API void nimroda_release_optim_data(nimroda_optim_data_t *optim);
	NIMRODA_INTERNAL_API void nimroda_batch_cleanup(nimroda_batch_t *batch);
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
#include <unordered_map>
#include <random>
#include <memory>

namespace NimrodA {
using ConfigMap = std::unordered_map<std::string, std::string>;
using RNGImpl = std::mt19937;
using ScopedPoint = std::unique_ptr<nimroda_point_t, decltype(nimroda_point_release)*>;
using ScopedNimrod = std::unique_ptr<nimroda_instance_t, decltype(nimroda_release)*>;

NIMRODA_INTERNAL_API void nimroda_map_custom_config(NimrodA::ConfigMap& map, const nimroda_custom_config_t *config);
NIMRODA_INTERNAL_API std::unique_ptr<nimroda_custom_config_t[]> nimroda_create_backed_custom_config(const NimrodA::ConfigMap& map);
}
#endif

#endif /* NIMRODA_PRIVATE_NIMRODA_IP_H */
