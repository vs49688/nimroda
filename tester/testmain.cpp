/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#if 0
#include <allocators>
#include "nimroda/cxx/CoordinateVector.hpp"
#include "nimroda/cxx/Nimrod.hpp"

using namespace NimrodA;


template class CoordinateVector<double, std::allocator<double>>;

extern "C" void debug_hack(nimroda_instance_t *instance)
{
	Nimrod *pNimrod = reinterpret_cast<Nimrod*>(instance->__cpp);

	typedef NimrodAllocator<double> NimDouble;
	NimDouble dalloc = pNimrod->allocator;

	DoubleVector<> vectorStandard(10, 1.0);
	DoubleVector<NimDouble> vectorNimrod(10, 1.0, pNimrod->allocator);
	
	DoubleVector<>::String sString = vectorStandard.toString();
	DoubleVector<NimDouble>::String nString = vectorNimrod.toString();
	__debugbreak();
}
#endif
#if 0
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include "nimroda/nimroda_ip.h"
#include "nimroda/mots2/TabuSearch.hpp"

#include "zdt4.hpp"
using namespace NimrodA;


static void mots2_stm_test(void)
{
	MOTS2::STMContainer stm(2);

	stm.add(MOTS2::Point::create(3, 0));
	stm.add(MOTS2::Point::create(3, 1));
	stm.add(MOTS2::Point::create(3, 2));
	// stm should only contain (1, 1) and (2, 2)

	MOTS2::Point::Ref tabuPoint = MOTS2::Point::create(3, 1);
	bool isTabu = stm.isTabu(tabuPoint);
	assert(isTabu);
	
}

static void mots2_stm_test_close_tabu()
{
	using Point = MOTS2::Point;

	MOTS2::STMContainer stm(1);
	stm.add(Point::create(1, 0.69999999999999996));

	Point::Ref pt = Point::create(1, 0.70000000000000007);

	/* Numbers this close together had issues when using the old unordered_map
	 * implementation of STMContainer. */
	bool isTabu = stm.isTabu(pt);
	assert(isTabu);
}


static void mots2_mtm_test()
{
	//DummyObjective obj(MOTS2::Point::create(3, 0.0), MOTS2::Point::create(3, 1.0));
	//MOTS2::MTMContainer mtm(obj);
	//mtm.add(MOTS2::Point::create(3, 0));
	//mtm.add(MOTS2::Point::create(3, 1));
	//mtm.add(MOTS2::Point::create(3, 2));
	//
	//for(const MOTS2::Point::ConstRef& p : mtm)
	//{
	//	const MOTS2::Point& pt = *p;
	//
	//	printf("(%lf, %lf, %lf)\n", pt[0], pt[1], pt[2]);
	//}
}

static void mots2_ak_test()
{
	auto pt = MOTS2::Point::create(3, 1.0);
	auto pt2 = MOTS2::Point::create(3, 1.0);

	//std::unordered_set<MOTS2::Point::ConstRef> kek;
	MOTS2::PointSet kek;

	/* Check for duplicates */
	kek.add(pt);
	kek.add(pt2);
	assert(kek.size() == 1);

	/* See if addition works */
	auto pt3 = MOTS2::Point::create(3, 2.0);
	kek.add(pt3);
	assert(kek.size() == 2);
}

static int dominates(const MOTS2::Point::ConstRef& pt1, const MOTS2::Point::ConstRef& pt2, MOTS2::ObjectiveFunction& obj)
{
	/* Fail condition - bug */
	if(pt1->length != pt2->length)
		throw std::runtime_error("Point::dominates(): Incompatible points.");

	size_t numGreater = 0, numLess = 0;

	MOTS2::Point::ConstRef obj1 = obj.evaluate(pt1);
	MOTS2::Point::ConstRef obj2 = obj.evaluate(pt2);

	/* This is a fail condition, if we get here, there's a bug in the code. */
	if(!obj1 || !obj2)
		throw std::runtime_error("Point::dominates(): Error retrieving evaluation results: Point not evaluated.");

	for(size_t i = 0; i < obj1->length; ++i)
	{
		double o1 = (*obj1)[i];
		double o2 = (*obj2)[i];

		if(o1 > o2)
			++numGreater;
		else if(o1 < o2)
			++numLess;

		//if((o1 - o2) > 0)
		//	++numGreater;
		//else if((o1 - o2) < 0)
		//	++numLess;
	}

	/* No component is greater and at least one component is smaller. */

	if(numGreater > 0 && numLess == 0)
		return -1; /* pt1 dominates pt2 */
	else if(numLess > 0 && numGreater == 0)
		return 1; /* pt2 dominates pt1 */
	else
		return 0;
}

static bool addIfNotDominated(MOTS2::MTMContainer& mtm, const MOTS2::Point::ConstRef& pt, MOTS2::ObjectiveFunction& obj)
{
	for(const MOTS2::Point::ConstRef& point : mtm)
	{
		int dom = dominates(point, pt, obj);
		printf("dom = %d\n", dom);
		if(dom == 1)
			return false;
	}

	return mtm.add(pt), true;
}

static void dominance_test()
{
	
	MOTS2::Point::ConstRef startPoint = MOTS2::Point::create(10, 0);

	std::vector<MOTS2::Point::Ref> tempSampled(6);
	tempSampled[0] = MOTS2::Point::create(10, 0);
	(*(tempSampled[0]))[7] = -1;

	tempSampled[1] = MOTS2::Point::create(10, 0);
	(*(tempSampled[1]))[1] = 1;

	tempSampled[2] = MOTS2::Point::create(10, 0);
	(*(tempSampled[2]))[6] = -1;

	tempSampled[3] = MOTS2::Point::create(10, 0);
	(*(tempSampled[3]))[8] = -1;

	tempSampled[4] = MOTS2::Point::create(10, 0);
	(*(tempSampled[4]))[5] = -1;

	tempSampled[5] = MOTS2::Point::create(10, 0);
	(*(tempSampled[5]))[0] = 0.100;

	
	

	ZDT4Adapter zdt4Adapter;
	MOTS2::MTMContainer mtm(zdt4Adapter);
	mtm.add(startPoint);

	int selfDominates = dominates(startPoint, startPoint, zdt4Adapter);

	//MOTS2::PointSet tempSampledDominated;
	//MOTS2::PointSet tempSampledDominat;
	//
	//int i = 0;
	//for(const MOTS2::Point::ConstRef& pt : tempSampled)
	//{
	//	if(i == 5)
	//	{
	//		printf("");
	//	}
	//	if(/*m_MTM.addIfNotDominated(pt)*/addIfNotDominated(mtm, pt, zdt4Adapter))
	//		tempSampledDominat.add(pt);
	//	else
	//		tempSampledDominated.add(pt);
	//
	//	++i;
	//}

}

MOTS2::Point::Ref createPointV(size_t n, va_list ap)
{
	std::vector<double> pt(n);

	for(size_t i = 0; i < n; ++i)
		pt[i] = va_arg(ap, double);

	return MOTS2::Point::create(pt);
}

MOTS2::Point::Ref createPoint(size_t n, ...)
{
	MOTS2::Point::Ref pt;

	va_list ap;

	va_start(ap, n);
	pt = createPointV(n, ap);
	va_end(ap);

	return pt;
}

static void mots2_set_adding_test(void)
{
	/* These are all the points generated by the initial Hooke&Jeeves move for ZDT4,
	 * assuming (0, 0, 0, 0, 0, 0, 0, 0, 0, 0) is the starting point. */
	MOTS2::Point::Ref points[20];
	size_t pt = 0;
	points[pt++] = createPoint(10,  0.1,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0);
	points[pt++] = createPoint(10, -0.1,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0);
	points[pt++] = createPoint(10,  0.0,  1.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0);
	points[pt++] = createPoint(10,  0.0, -1.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0);
	points[pt++] = createPoint(10,  0.0,  0.0,  1.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0);
	points[pt++] = createPoint(10,  0.0,  0.0, -1.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0);
	points[pt++] = createPoint(10,  0.0,  0.0,  0.0,  1.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0);
	points[pt++] = createPoint(10,  0.0,  0.0,  0.0, -1.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0);
	points[pt++] = createPoint(10,  0.0,  0.0,  0.0,  0.0,  1.0,  0.0,  0.0,  0.0,  0.0,  0.0);
	points[pt++] = createPoint(10,  0.0,  0.0,  0.0,  0.0, -1.0,  0.0,  0.0,  0.0,  0.0,  0.0);
	points[pt++] = createPoint(10,  0.0,  0.0,  0.0,  0.0,  0.0,  1.0,  0.0,  0.0,  0.0,  0.0);
	points[pt++] = createPoint(10,  0.0,  0.0,  0.0,  0.0,  0.0, -1.0,  0.0,  0.0,  0.0,  0.0);
	points[pt++] = createPoint(10,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  1.0,  0.0,  0.0,  0.0);
	points[pt++] = createPoint(10,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, -1.0,  0.0,  0.0,  0.0);
	points[pt++] = createPoint(10,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  1.0,  0.0,  0.0);
	points[pt++] = createPoint(10,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, -1.0,  0.0,  0.0);
	points[pt++] = createPoint(10,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  1.0,  0.0);
	points[pt++] = createPoint(10,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, -1.0,  0.0);
	points[pt++] = createPoint(10,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  1.0);
	points[pt++] = createPoint(10,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0, -1.0);

	// pt = 20

	MOTS2::PointSet set;

	for(size_t i = 0; i < pt; ++i)
	{
		set.add(points[i]);
	}

	assert(set.size() == pt);

}

int main2(int argc, char **argv);

int main3(int argc, char **argv)
{
	return main2(argc, argv);
	//{
	//	MOTS2::ConfigurationSettings settings;
	//	
	//	settings.nVar = 3;
	//	
	//	settings.setUniformStartingStep(0.5);
	//	
	//	settings.starting_point = MOTS2::Point::create(3, 0);
	//	settings.starting_point->operator[](0) = 0;
	//	settings.starting_point->operator[](1) = 1;
	//	settings.starting_point->operator[](2) = 2;
	//	
	//	settings.write("configuration-mots2.txt");
	//}
	//
	//{
	//	MOTS2::ConfigurationSettings settings("configuration-mots2.txt");
	//	settings.dump(stdout);
	//}
	

	//nimroda_state_t state;
	//
	//printf("sizeof(nimroda_state_t) = %u\n", (uint32_t)sizeof(nimroda_state_t));
	//printf("sizeof(nimroda_variable_attributes_t) = %u\n", (uint32_t)sizeof(nimroda_variable_attributes_t));
	//printf("sizeof(nimroda_optim_status_t) = %u\n", (uint32_t)sizeof(nimroda_optim_status_t));
	//printf("sizeof(state.config) = %u\n", (uint32_t)sizeof(state.config));

	mots2_stm_test();
	mots2_stm_test_close_tabu();
	mots2_mtm_test();

	mots2_ak_test();

	dominance_test();
	mots2_set_adding_test();
	return 0;
}

#endif

#include "nimroda/nimroda.h"
NIMRODA_API void debugHack(nimroda_instance_t *nimrod);

extern "C" void debug_hack(nimroda_instance_t *instance)
{
	//debugHack(instance);
}


//#include "nimroda/cxx/Nimrod.hpp"
//
//NIMRODA_API void debugHack(nimroda_instance_t *nimrod)
//{
//	Nimrod *nim = (Nimrod*)nimrod->__cpp;
//
//	PointSet::Set set(nim->allocator);
//
//	Point::_DoubleVector vec(10, nim->allocator);
//	vec[0] = 0.7309677873767;
//	vec[1] = -2.5946358432851;
//	vec[2] = 1.3741742535011;
//	vec[3] = 0.5043700511763;
//	vec[4] = 0.975452777972;
//	vec[5] = -1.6678160052335;
//	vec[6] = -1.1481081525928;
//	vec[7] = 4.8484154019981;
//	vec[8] = 3.7918251787248;
//	vec[9] = 4.4124917948211;
//	set.insert(nim->createPoint(vec));
//
//	vec[0] = 0.74;
//	set.insert(nim->createPoint(vec));
//
//	int x = 0;
//}