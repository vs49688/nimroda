cmake_minimum_required(VERSION 3.1)
project(nimrodotest)

add_executable(nimrodotest testmain.cpp zdt4.cpp)

target_link_libraries(nimrodotest nimroda)

set_target_properties(nimrodotest PROPERTIES
	C_STANDARD 99
	C_STANDARD_REQUIRED YES

    CXX_STANDARD 11
    CXX_STANDARD_REQUIRED YES
)
