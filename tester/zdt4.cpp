/*
 * Nimrod/A Native Optimisation Library for Nimrod/OK
 * https://github.com/vs49688/nimroda
 *
 * SPDX-License-Identifier: Apache-2.0
 * Copyright (c) 2019 Zane van Iperen
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <inttypes.h>

#define _USE_MATH_DEFINES
#include <math.h>

#ifndef M_PI
#	define M_PI (3.14159265358979323846)
#endif

#include "nimroda/nimroda.h"

#define ZDT4_VAR_COUNT (10)
#define ZDT4_OBJECTIVE_COUNT (2)


typedef struct
{
	FILE *evalOut;
	FILE *saveOut;
	size_t depth;
	cio_context_t saveRestoreCtx;
} private_t;



/* -----------------ZDT4 Functions----------------- */
static double zdt4_h(double f, double g) { return 1.0 - sqrt(f / g); }

static double zdt4_g(const double *x)
{
	double result = 91;

	for(int i = 1; i < 10; ++i)
		result += pow(x[i], 2) - 10 * cos(4 * M_PI * x[i]);

	return result;
}

double zdt4_f(const double *x) { return zdt4_g(x) * zdt4_h(x[0], zdt4_g(x)); }

/* -----------------Nimrod/A Callbacks-----------------*/
static void cb_puts(nimroda_instance_t *instance, const char *s) { fputs(s, stdout); }

static int cb_eval(nimroda_instance_t *instance, nimroda_batch_t *batch)
{
	for(size_t i = 0; i < batch->num_points; ++i)
	{
		double o1 = batch->points[i]->coord[0];
		double o2 = zdt4_f(batch->points[i]->coord);

		batch->points[i]->objectives[0] = o1;
		batch->points[i]->objectives[1] = o2;
		batch->points[i]->status = JOB_COMPLETE;

		private_t *ptr = (private_t*)nimroda_get_user_pointer(instance);
		fprintf(ptr->evalOut, "plot o %lf %lf\n", o1, o2);
		fflush(ptr->evalOut);
	}

	return 0;
}

static void init_vars(nimroda_variable_attributes_t *vars)
{
	memset(vars, 0, sizeof(nimroda_variable_attributes_t));

	for(int i = 0; i < ZDT4_VAR_COUNT; ++i)
	{
		if(i == 0)
		{
			vars->lower_bound[i] = 0.0;
			vars->upper_bound[i] = 1.0;
		}
		else
		{
			vars->lower_bound[i] = -5.0;
			vars->upper_bound[i] = 5.0;
		}

		assert(MAX_NAME_LENGTH > 3);
		sprintf(vars->name[i], "x%d", i + 1);

		vars->range[i] = vars->upper_bound[i] - vars->lower_bound[i];
	}
}

static void init_point(nimroda_point_t *pt, const double *coords)
{
	memset(pt, 0, sizeof(nimroda_point_t));

	pt->num_dims = ZDT4_VAR_COUNT;
	pt->coord = (double*)malloc(sizeof(double) * ZDT4_VAR_COUNT);
	pt->status = JOB_COMPLETE;
	pt->num_objectives = ZDT4_OBJECTIVE_COUNT;
	pt->objectives = (double*)malloc(sizeof(double) * ZDT4_OBJECTIVE_COUNT);

	for(int i = 0; i < ZDT4_VAR_COUNT; ++i)
		pt->coord[i] = coords[i];

	for(int i = 0; i < ZDT4_OBJECTIVE_COUNT; ++i)
		pt->objectives[i] = HUGEISH;
}

static nimroda_custom_config_t g_sZDT4Config[] =
{
	{"mots2.loop_limit", "100"},
	{"mots2.diversify", "25"},
	{"mots2.intensify", "15"},
	{"mots2.reduce", "50"},
	{"mots2.eval_limit", "0"},
	{"mots2.reduce", "10"},
	{"mots2.start_step", "10 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1"},
	{"mots2.ssrf", "0.5"},
	{"mots2.n_sample", "6"},
	{"mots2.n_regions", "4"},
	{"mots2.stm_size", "20"},
	{NULL, NULL}
};

/* If defined, call debug_hack() in testmain.cpp after nimroda_init(). */
//#define DEBUG_HACKS

#if defined(DEBUG_HACKS)
static char *s_debugArgs[] = {"/path/to/exe", "1000", "0", "0", "0", "0", "0", "0", "0", "0", "0", "0"};
extern "C" void debug_hack(nimroda_instance_t *instance);
#endif

static void fillTabs(FILE *f, size_t num)
{
	for(size_t i = 0; i < num; ++i)
		fprintf(f, "\t");
}

int cb_sr_push(cio_context_t *oldCtx, cio_context_t *newCtx)
{
	private_t *priv = (private_t*)oldCtx->user;

	fillTabs(priv->saveOut, priv->depth);
	fprintf(priv->saveOut, "'%s' = %c\n", newCtx->__name, newCtx->__listFlag ? '[' : '{');
	++priv->depth;
	return 0;
}

int cb_sr_pop(cio_context_t *oldCtx, cio_context_t *newCtx)
{
	private_t *priv = (private_t*)oldCtx->user;
	--priv->depth;
	fillTabs(priv->saveOut, priv->depth);
	fprintf(priv->saveOut, "%c\n", oldCtx->__listFlag ? ']' : '}');
	return 0;
}

int cb_sr_write(cio_context_t *ctx, const void *buffer, size_t size, cio_type_t type, const char *name)
{
	private_t *priv = (private_t*)ctx->user;

	fillTabs(priv->saveOut, priv->depth);
	if(name && !ctx->__listFlag)
		fprintf(priv->saveOut, "'%s' = ", name);

	switch(type)
	{
		case CIO_TYPE_NONE:
			fprintf(priv->saveOut, "raw:[");
			for(size_t i = 0; i < size; ++i)
			{
				fprintf(priv->saveOut, "0x%02x", *((int8_t*)buffer + i));

				if(i != size - 1)
					fprintf(priv->saveOut, ", ");
			}
			fprintf(priv->saveOut, "]");
			break;
		case CIO_TYPE_INT8:
			fprintf(priv->saveOut, "int8:'%d'", *((int8_t*)buffer));
			break;
		case CIO_TYPE_UINT8:
			fprintf(priv->saveOut, "uint8:'%u'", *((uint8_t*)buffer));
			break;
		case CIO_TYPE_INT16:
			fprintf(priv->saveOut, "int16:'%hd'", *((int16_t*)buffer));
			break;
		case CIO_TYPE_UINT16:
			fprintf(priv->saveOut, "uint16:'%hu'", *((uint16_t*)buffer));
			break;
		case CIO_TYPE_INT32:
			fprintf(priv->saveOut, "int32:'%d'", *((int32_t*)buffer));
			break;
		case CIO_TYPE_UINT32:
			fprintf(priv->saveOut, "uint32:'%u'", *((uint32_t*)buffer));
			break;
		case CIO_TYPE_INT64:
			fprintf(priv->saveOut, "int64:'%" PRId64 "'", *((int64_t*)buffer));
			break;
		case CIO_TYPE_UINT64:
			fprintf(priv->saveOut, "uint64:'%" PRIu64 "'", *((uint64_t*)buffer));
			break;
		case CIO_TYPE_FLOAT32:
			fprintf(priv->saveOut, "float32:'%f'", *((float*)buffer));
			break;
		case CIO_TYPE_FLOAT64:
			fprintf(priv->saveOut, "float64:'%lf'", *((double*)buffer));
			break;
		case CIO_TYPE_STRING:
			fprintf(priv->saveOut, "string:'%s'", (const char*)buffer);
			break;
	}

	fprintf(priv->saveOut, "\n");
	return 0;
}

void cb_stats(nimroda_instance_t *instance, const nimroda_kv_t *stats, size_t count)
{
	int x = 0;
}

int main(int argc, char **argv)
{
	if(argc != ZDT4_VAR_COUNT + 2)
	{
		printf("Usage: %s <iterations> <c0> <c1> <c2> <c3> <c4> <c5> <c6> <c7> <c8> <c9> <c10>\n", argv[0]);
		return 1;
	}

	uint32_t iterations;
	if(sscanf(argv[1], "%u", &iterations) != 1)
	{
		fprintf(stderr, "Error reading iteration count.");
		return 1;
	}

	char intBuffer[16];
	sprintf(intBuffer, "%u", iterations);
	g_sZDT4Config[0].value = intBuffer;

	double rawPoint[ZDT4_VAR_COUNT];
	for(int i = 0; i < ZDT4_VAR_COUNT; ++i)
	{
		if(sscanf(argv[i + 2], "%lf", &rawPoint[i]) != 1)
		{
			fprintf(stderr, "Error reading coordinate %d.", i);
			return 1;
		}
	}

	const nimroda_algorithm_t *algo = nimroda_lookup_algorithm("MOTS2");

	if(!algo)
	{
		fprintf(stderr, "Unable to select MOTS2 algorithm. Exiting...\n");
		return 1;
	}

	private_t priv;
	priv.depth = 0;
	if(!(priv.evalOut = fopen("eval_log.txt", "wb")))
	{
		fprintf(stderr, "Error opening eval_log.txt for writing.\n");
		return 1;
	}

	if(!(priv.saveOut = fopen("save_state.txt", "wb")))
	{
		fprintf(stderr, "Error opening eval_log.txt for writing.\n");
		fclose(priv.evalOut);
		return 1;
	}

	fprintf(priv.evalOut, "clear\n");

	/* Configure Save/Restore */
	priv.saveRestoreCtx.push = &cb_sr_push;
	priv.saveRestoreCtx.pop = &cb_sr_pop;
	priv.saveRestoreCtx.read = nullptr;
	priv.saveRestoreCtx.write = &cb_sr_write;
	priv.saveRestoreCtx.user = &priv;

	nimroda_variable_attributes_t vars;
	init_vars(&vars);

	nimroda_config_t config;
	memset(&config, 0, sizeof(config));
	config.algorithm = algo;
	config.vars = &vars;
	config.num_dimensions = ZDT4_VAR_COUNT;
	config.num_objectives = ZDT4_OBJECTIVE_COUNT;
	config.callbacks.puts = &cb_puts;
	config.callbacks.write_stats = &cb_stats;
	config.callbacks.user = &priv;
	config.debug = 10000;
	config.rng_seed = 0;
	config.custom_config = g_sZDT4Config;

	nimroda_point_t point;
	init_point(&point, rawPoint);

	config.starting_point = &point;

	nimroda_instance_t *instance = nimroda_init(&config);
	if(!instance)
	{
		fprintf(stderr, "Nimrod/A initialisation failed. Exiting...\n");
		return 1;
	}

#if defined(DEBUG_HACKS)
	debug_hack(instance);
	return -1;
#endif
	for(;;)
	{
		nimroda_optim_state_t state = nimroda_optimise(instance);

		if(state == STATE_WAITING_FOR_BATCH)
		{
			nimroda_batch_t *batch = nimroda_get_batch(instance);
			cb_eval(instance, batch);
		}
		else if(state == STATE_FINISHED)
		{
			break;
		}
	}

	nimroda_optim_status_t *status = nimroda_get_optim_status(instance);

	if(status->code != 0)
	{
		printf("Optimisation failed with message %s\n", status->message);
		goto optimisation_failed;
	}

	printf("Pareto Front:\n");
	for(size_t i = 0; i < status->result_count; ++i)
	{
		for(int j = 0; j < ZDT4_OBJECTIVE_COUNT; ++j)
			printf("%.8f ", status->results[i]->objectives[j]);

		printf("  => ");

		for(int j = 0; j < ZDT4_VAR_COUNT; ++j)
			printf("%.8lf ", status->results[i]->coord[j]);

		printf("\n");
	}
	printf("\n");

	nimroda_save(instance, &priv.saveRestoreCtx);

optimisation_failed:
	free(point.coord);
	free(point.objectives);

	nimroda_release(instance);

	fclose(priv.evalOut);
	fclose(priv.saveOut);
	return 0;
}