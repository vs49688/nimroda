# Nimrod/A

Nimrod/A native optimisation library for Nimrod/OK.

Written for for my 2016 honours project and subsequently published:

* [Multi-objective optimisation in scientific workflows](https://www.sciencedirect.com/science/article/pii/S1877050917308062)
  - [10.1016/j.procs.2017.05.213](https://doi.org/10.1016/j.procs.2017.05.213)

If you use this library it would be greatly appreciated if you cited the above article.

### Building for Nimrod/OK
When building for Nimrod/OK, try to minimise the amount of dynamic dependencies required. 

#### Linux
* Statically link to `libstdc++`, you'd be surprised how many systems have no C++11 support yet.
  - This is fine, as the public interface is pure C. There is no front-facing C++ code.
```
$ git clone https://path/to/repo/nimroda
$ cd nimroda && mkdir -p build && cd build
$ LDFLAGS="-static-libstdc++" cmake -DNIMRODA_BUILD_STATIC=Off ..
$ make nimroda && strip -s libnimroda.so
```

#### Windows (Mingw-w64)
* Statically link to `libgcc` and `libstdc++`.
* For 32-bit use `-DCMAKE_TOOLCHAIN_FILE=../mingw-w64-toolchain-x86.cmake`
* For 64-bit use `-DCMAKE_TOOLCHAIN_FILE=../mingw-w64-toolchain-x86_64.cmake`
```
$ git clone https://path/to/repo/nimroda
$ cd nimroda && mkdir -p build && cd build
$ LDFLAGS="-static-libgcc -static-libstdc++" cmake -DCMAKE_TOOLCHAIN_FILE=../mingw-w64-toolchain-x86.cmake -DNIMRODA_BUILD_STATIC=Off ..
$ make nimroda && strip -s libnimroda.dll && mv libnimroda.dll nimroda.dll
```

#### Windows (Visual Studio)
* VS 2015, use:
  - `-G "Visual Studio 14 2015"` for 32-bit
  - `-G "Visual Studio 14 2015 Win64"` for 64-bit
* Only Visual Studio 2015 RTM has been tested
```
$ git clone https://path/to/repo/nimroda
$ cd nimroda && mkdir -p build && cd build
$ cmake -DNIMRODA_BUILD_STATIC=Off
```

## Notes
* Will not compile with GCC 4.8, see [bug 54577](https://gcc.gnu.org/bugzilla/show_bug.cgi?id=54577)

## License
This project is licensed under the [Apache License, Version 2.0](https://opensource.org/licenses/Apache-2.0):

Copyright &copy; 2019 [Zane van Iperen](mailto:zane@zanevaniperen.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.