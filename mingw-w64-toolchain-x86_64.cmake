set(CMAKE_SYSTEM_NAME Windows)

if(EXISTS /usr/x86_64-w64-mingw32)
	set(CMAKE_C_COMPILER x86_64-w64-mingw32-gcc)
	set(CMAKE_CXX_COMPILER x86_64-w64-mingw32-g++)
	set(CMAKE_RC_COMPILER x86_64-w64-mingw32-windres)
	set(CMAKE_FIND_ROOT_PATH /usr/x86_64-w64-mingw32)
else()
	message(FATAL_ERROR "No 64-bit mingw-w64 toolchain found in /usr/x86_64-w64-mingw32")
endif()



set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
